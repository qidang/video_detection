import torch
from torchcore.data.datasets import MixDataset, COCOPersonPatchDataset, ModanetHumanPatchDataset
from torchcore.data.transforms import Resize, ResizeAndPadding, Compose, ToTensor, Normalize
from torchcore.dnn import networks
from torchcore.dnn.networks.classification_net import ClassificationNet

def get_dataset():
    #transforms = Resize((128, 128))
    #transforms = ResizeAndPadding((128,128))
    transforms = Compose([ResizeAndPadding(160), ToTensor(), Normalize()])
    anno_path = '/ssd/data/annotations/coco2014_instances_person.pkl'
    root = '/ssd/data/datasets/COCO'
    part = 'train2014' # or val2014
    dataset_a = COCOPersonPatchDataset(root, anno_path, part=part, transforms=transforms)

    anno_root = '/ssd/data/annotations/modanet_with_human.pkl'
    image_root = '/ssd/data/datasets/modanet/Images'
    dataset_b = ModanetHumanPatchDataset(anno_root, image_root, part='train', transforms=transforms)

    mix_dataset = MixDataset(dataset_a, dataset_b, shuffle=True, sample=True)
    dataloader = torch.utils.data.DataLoader(mix_dataset, batch_size=16, num_workers=2)
    return dataloader

def get_model():
    backbone = networks.feature.resnet50(pretrained=True)
    net = ClassificationNet(backbone, input_size=5, class_num=2)
    return net

def run(dataset, model):
    model.eval()
    i = 0
    for inputs, targets in dataset:
        out = model(inputs, targets)
        print(out)
        break
        i += 1
        if i > 10:
            break
