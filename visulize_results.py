import cv2
import pickle
import os
import argparse

from dev import get_video_info, parse_video_list
from benchmark_model import load_pkl_result
from torchcore.tools.visulize_tools import cv_put_text_with_box, cv_put_text
from torchcore.tools.color_gen import random_colors

def visulization_gt_and_dt(video_path, out_path,  garment_gt, garment_dt, human_result=None, tracker=None, valid_track_id=None, visulize_audience=False):
    fourcc = 'mp4v'
    video_info = get_video_info(video_path)
    cap = cv2.VideoCapture(video_path)
    out = cv2.VideoWriter(out_path, cv2.VideoWriter_fourcc(*fourcc), video_info['fps'], video_info['size'])

    # prepare model track boxes
    if tracker is not None:
        model_nodes, human_nodes = tracker.convert_tracks(one_model_per_shot=False, valid_track_ids=valid_track_id)
        model_boxes_by_frame = {}
        audience_boxes_by_frame = {}
        
        for frame_count in model_nodes:
            model_boxes_by_frame[frame_count] = [node.box for node in model_nodes[frame_count]]
        for frame_count in human_nodes:
            audience_boxes_by_frame[frame_count] = [node.box for node in human_nodes[frame_count]]

    color_num = 13
    colors = random_colors(color_num, GBR=True)

    frame_count = 0
    while True:
        ret, im = cap.read()
        if not ret:
            break
        frame_count += 1
        # visulize fashion model
        if tracker is not None:
            if frame_count in model_boxes_by_frame:
                for box in model_boxes_by_frame[frame_count]:
                    cv2.rectangle(im, tuple(box[:2]), tuple(box[2:]),(0,255,0), lineType=4)
                    cv_put_text_with_box(im, 'model', tuple(box[:2].astype(int)), cv2.FONT_HERSHEY_COMPLEX, 1, font_color=(0, 0, 0), box_color=(0,255,0),thickness=2)
            if visulize_audience:
                if frame_count in audience_boxes_by_frame:
                    for box in audience_boxes_by_frame[frame_count]:
                        cv2.rectangle(im, tuple(box[:2]), tuple(box[2:]),(0,0,255), lineType=4)
                        cv_put_text_with_box(im, 'audience', tuple(box[:2].astype(int)), cv2.FONT_HERSHEY_COMPLEX, 1, font_color=(0, 0, 0), box_color=(0,0,255),thickness=2)
        
        # visulize garments
        if frame_count in garment_gt:
            boxes = garment_gt[frame_count]['boxes']
            labels = garment_gt[frame_count]['labels']
            scores = garment_gt[frame_count]['scores']
            for box, label in zip(boxes, labels):
                color = colors[int(label)%color_num]
                cv2.rectangle(im, tuple(box[:2]), tuple(box[2:]),color, lineType=4)
        
        if frame_count in garment_dt:
            boxes = garment_dt[frame_count]['boxes']
            labels = garment_dt[frame_count]['labels']
            scores = garment_dt[frame_count]['scores']
            for box, label in zip(boxes, labels):
                color = colors[int(label)%color_num]
                cv2.rectangle(im, tuple(box[:2]), tuple(box[2:]),color, lineType=4)

        out.write(im)
        if frame_count > 2000:
            break
    out.release()
    print('The generated video is saved to {}'.format(out_path))
def visulization(video_path, out_path,  human_result=None, tracker=None, valid_track_id=None, garment_result=None, visulize_audience=False):
    fourcc = 'mp4v'
    video_info = get_video_info(video_path)
    cap = cv2.VideoCapture(video_path)
    out = cv2.VideoWriter(out_path, cv2.VideoWriter_fourcc(*fourcc), video_info['fps'], video_info['size'])

    # prepare model track boxes
    if tracker is not None:
        model_nodes, human_nodes = tracker.convert_tracks(one_model_per_shot=False, valid_track_ids=valid_track_id)
        model_boxes_by_frame = {}
        audience_boxes_by_frame = {}
        
        for frame_count in model_nodes:
            model_boxes_by_frame[frame_count] = [node.box for node in model_nodes[frame_count]]
        for frame_count in human_nodes:
            audience_boxes_by_frame[frame_count] = [node.box for node in human_nodes[frame_count]]

    color_num = 13
    colors = random_colors(color_num, GBR=True)

    frame_count = 0
    while True:
        ret, im = cap.read()
        if not ret:
            break
        frame_count += 1
        # visulize fashion model
        if tracker is not None:
            if frame_count in model_boxes_by_frame:
                for box in model_boxes_by_frame[frame_count]:
                    cv2.rectangle(im, tuple(box[:2]), tuple(box[2:]),(0,255,0), lineType=4)
                    cv_put_text_with_box(im, 'model', tuple(box[:2].astype(int)), cv2.FONT_HERSHEY_COMPLEX, 1, font_color=(0, 0, 0), box_color=(0,255,0),thickness=2)
            if visulize_audience:
                if frame_count in audience_boxes_by_frame:
                    for box in audience_boxes_by_frame[frame_count]:
                        cv2.rectangle(im, tuple(box[:2]), tuple(box[2:]),(0,0,255), lineType=4)
                        cv_put_text_with_box(im, 'audience', tuple(box[:2].astype(int)), cv2.FONT_HERSHEY_COMPLEX, 1, font_color=(0, 0, 0), box_color=(0,0,255),thickness=2)
        
        # visulize garments
        if garment_result is not None:
            if frame_count in garment_result:
                boxes = garment_result[frame_count]['boxes']
                labels = garment_result[frame_count]['labels']
                scores = garment_result[frame_count]['scores']
                for box, label in zip(boxes, labels):
                    color = colors[int(label)%color_num]
                    cv2.rectangle(im, tuple(box[:2]), tuple(box[2:]),color, lineType=4)
        
        out.write(im)
        if frame_count > 2000:
            break
    out.release()
    print('The generated video is saved to {}'.format(out_path))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--tracker', default='tracker.pkl',
                        type=str, help='tracker path')
    #parser.add_argument('--human_res', default=416, 
    #                    type=int, help='Human detector resolution')
    parser.add_argument('--garment_detector', default='centernet', choices=['centernet', 'yolo_v3', 'frcnn', 'ssd'],
                        type=str, help='Garment detector')
    parser.add_argument('--garment_res', default=256, 
                        type=int, help='Garment detector resolution')
    parser.add_argument('--video_list', default='video_names.txt', 
                        type=str, help='file path for video names')
    #parser.add_argument('--video_shots', default='video_shots.txt', 
    #                    type=str, help='file path for valid video shots')
    args = parser.parse_args()
    print(args)


    video_root_path = os.path.expanduser('~/data/fashion_videos/')
    video_names, video_paths, valid_shots = parse_video_list(args.video_list, video_root_path, video_shots=None)

    valid_track_ids = [17,111,152,184,234,269,284,301,313,329,338,402,482,528,535,548,553,566,612,655,705,763,782,786,820,831,879,948,962]

    #valid_track_ids = [17,79,141,169,213,235,262,277,301,348,360,403,489,518,545,554,573,636,668,752,771,767,807,855,894,932,1025,1057,1065,1067,1076,1130,1166,1212,1256,1283,1274,1323,1346,1381,1429,1527,1560,1565,1588,1591,1592,1596,1637,1720,1796,1815,1816,1831,1838,1848,1857,1939,1956,1999,2006,2012,2056,2074,2092,2107,2126,2130,2157,2177,2237,2267,2330,2385,2395,2408,2490,2510,2611,2633,2639,2660,2669,2722,2763,2851,2863,2876,2895,2905,2961,2994,3007,3109,3114,3136,3145,3181,3188,3210,3267,3301,3390,3425,3432,3440,3463,3490,3529,3579,3589,3615,3627,3656,3692,3695,3711,3767,3802,3899,3914, 3942, 3961,3991, 4021, 4059,4086,4158,4209,4221,4238,4267,4331,4355,4389, 4412, 4460,4457,4473,4481,4487,4537,4583,4666,4718,4730,4733,4769,4801,4839,4884,4983,5008,5055,5156,5204,5300,5349,5392,5453,5488,5556,5594,5615,5641,5657,5671,5711,5753,5820,5903]

    #for video_path, video_name in zip(video_paths, video_names):
    #    garment_path = os.path.join('garment_detection_result', '{}_{}_{}_refer_anno.pkl'.format(video_name, args.garment_detector, args.garment_res))
    #    out_path = os.path.join('out_videos', '{}_{}_{}_ref.mp4'.format(video_name, args.garment_detector, args.garment_res))
    #    #out_path = os.path.join('out_videos', '{}_all.mp4'.format(video_name, ))
    #    garment_result = load_pkl_result(garment_path)
    #    tracker = load_pkl_result(args.tracker)
    #    visulization(video_path, out_path, tracker=tracker, valid_track_id=valid_track_ids, garment_result=garment_result, visulize_audience=False)
    #    #visulization(video_path, out_path, tracker=None, valid_track_id=None, garment_result=garment_result, visulize_audience=False)
    #    #visulization(video_path, out_path, tracker=tracker, valid_track_id=None, garment_result=None, visulize_audience=True)

    for video_path, video_name in zip(video_paths, video_names):
        garment_gt_path = os.path.join('garment_detection_result', '{}_{}_{}_refer_anno.pkl'.format(video_name, args.garment_detector, args.garment_res))
        garment_dt_path = os.path.join('garment_detection_result', '{}_{}_{}.pkl'.format(video_name, args.garment_detector, args.garment_res))
        out_path = os.path.join('out_videos', '{}_{}_{}_ref.mp4'.format(video_name, args.garment_detector, args.garment_res))
        #out_path = os.path.join('out_videos', '{}_all.mp4'.format(video_name, ))
        garment_result = load_pkl_result(garment_gt)
        tracker = load_pkl_result(args.tracker)
        visulization(video_path, out_path, tracker=tracker, valid_track_id=valid_track_ids, garment_result=garment_result, visulize_audience=False)