import os
import pickle
import cv2
import argparse
import json
import numpy as np

from sklearn.cluster import SpectralClustering

from dev import merge_human_result,merge_result, convert_result_to_coco_gt
from dev import clip_boxes
from torchcore.tools.visulize_tools import cv_put_text_with_box, cv_put_text
from torchcore.tools.color_gen import random_colors

def get_iou(pred_box, gt_box):
    """
    pred_box : the coordinate for predict bounding box
    gt_box :   the coordinate for ground truth bounding box
    return :   the iou score
    the  left-down coordinate of  pred_box:(pred_box[0], pred_box[1])
    the  right-up coordinate of  pred_box:(pred_box[2], pred_box[3])
    """
    # 1.get the coordinate of inters
    ixmin = max(pred_box[0], gt_box[0])
    ixmax = min(pred_box[2], gt_box[2])
    iymin = max(pred_box[1], gt_box[1])
    iymax = min(pred_box[3], gt_box[3])

    iw = np.maximum(ixmax-ixmin+1., 0.)
    ih = np.maximum(iymax-iymin+1., 0.)

    # 2. calculate the area of inters
    inters = iw*ih

    # 3. calculate the area of union
    uni = ((pred_box[2]-pred_box[0]+1.) * (pred_box[3]-pred_box[1]+1.) +
           (gt_box[2] - gt_box[0] + 1.) * (gt_box[3] - gt_box[1] + 1.) -
           inters)

    # 4. calculate the overlaps between pred_box and gt_box
    iou = inters / uni

    return iou

def merge_result_all(detections):
    result_num = len(detections)
    for ind in range(result_num-1):
        assert set(list(detections[ind].keys())) == set(list(detections[ind+1].keys()))
    final_result = {}
        
    for i in detections[0]:
        boxes = []
        scores = []
        labels = []
        boxes_all = []
        scores_all = []
        labels_all = []
        for ind in range(result_num):
            frame_boxes = detections[ind][i]['boxes']
            if len(frame_boxes) == 0:
                continue
            boxes.append(frame_boxes)
            scores.append(detections[ind][i]['scores'])
            labels.append(detections[ind][i]['labels'])

        if len(boxes) > 0:
            boxes_all = np.concatenate(boxes, axis=0)
            #print(scores_all)
            #print(labels_all)
            scores_all = np.concatenate(scores, axis=0)
            labels_all = np.concatenate(labels, axis=0)
            frame_result = {'boxes':boxes_all, 'scores':scores_all, 'labels':labels_all}
            final_result[i] = frame_result
        if i not in final_result:
            final_result[i] = None
    return final_result

def merge_result_spectral_clustering_svd_old(detections, class_num,  one_class=False, debug_cluster=False ):
    result_num = len(detections)
    final_result = {}
    if one_class:
        class_num=None

    for ind in range(result_num-1):
        assert set(list(detections[ind].keys())) == set(list(detections[ind+1].keys()))
        
    for i in detections[0]:
        boxes = []
        scores = []
        labels = []
        boxes_all = []
        scores_all = []
        labels_all = []
        for ind in range(result_num):
            frame_boxes = detections[ind][i]['boxes']
            if len(frame_boxes) == 0:
                #final_result[i] = None
                continue
            boxes.append(frame_boxes)
            scores.append(detections[ind][i]['scores'])
            labels.append(detections[ind][i]['labels'])
        if len(boxes) > 0:
            boxes = np.concatenate(boxes, axis=0)
            scores = np.concatenate(scores)
            labels = np.concatenate(labels)
            if class_num is None or class_num==1:
                labels = np.ones_like(labels)
                class_num = 1
            for cls in range(1, class_num+1):
                class_ind = np.where(labels==cls)
                if len(class_ind[0]) == 0:
                    continue

                if len(class_ind[0]) == 1:
                    boxes_all.append(boxes[class_ind])

                boxes_class = boxes[class_ind]
                labels_class = labels[class_ind]
                scores_class = scores[class_ind]

                affmat = np.zeros((len(boxes_class), len(boxes_class)))
                for idx, r0 in enumerate(boxes_class) :
                    for idy, r1 in enumerate(boxes_class) :
                        affmat[idx,idy] = get_iou(r0,r1)
                u, s, vh = np.linalg.svd(affmat)
                #cluster_num = len(np.where(s>1.0)[0])
                cluster_num = len(np.where(s>s[0]*0.2)[0])

                affmat += 0.5
                affmat = np.minimum(affmat, 1.0)

                if cluster_num > 1:
                    clustering = SpectralClustering(n_clusters=cluster_num, affinity='precomputed')
                    res = clustering.fit_predict(affmat)

                    box_class = []
                    score_class = []
                    label_class = []
                    for clu_num in range(cluster_num):
                        inds = np.where(res==clu_num)
                        if len(inds[0])==0:
                            print('wrong')
                            continue
                        box_new = boxes_class[inds]
                        if non_overlap_boxes(box_new):
                            #print('abondon {} in frame {}'.format(box_new, i))
                            continue
                        score_new = scores_class[inds]
                        if not debug_cluster:
                            box_new = np.mean(box_new, axis=0)
                            score_new = np.mean(score_new)
                            box_class.append(box_new)
                            score_class.append(score_new)
                            label_class.append(cls)
                        else:
                            box_class.append(box_new)
                            score_class.append(score_new)
                            label_class.append(np.ones(len(box_new)))
                else:
                    box_class = [np.mean(boxes_class, axis=0)]
                    score_class = [np.mean(scores_class)]
                    label_class = [cls]

                if debug_cluster:
                    boxes_all.extend(box_class)
                    scores_all.extend(score_class)
                    labels_all.extend(label_class)
                else:
                    boxes_all.append(box_class)
                    scores_all.append(score_class)
                    labels_all.append(label_class)

            if not debug_cluster:
                boxes_all = np.concatenate(boxes_all, axis=0)
                scores_all = np.concatenate(scores_all, axis=0)
                labels_all = np.concatenate(labels_all, axis=0)
            
            frame_result = {'boxes':boxes_all, 'scores':scores_all, 'labels':labels_all}
            final_result[i] = frame_result
        if i not in final_result:
            final_result[i] = None
        if debug_cluster:
            if i > 301:
                break
        #i += 1
    return final_result
def merge_result_spectral_clustering_svd(detections, width, height,  debug_cluster=False, no_label=False ):
    result_num = len(detections)
    final_result = {}

    for ind in range(result_num-1):
        assert set(list(detections[ind].keys())) == set(list(detections[ind+1].keys()))
        
    for i in detections[0]:
        boxes = []
        scores = []
        labels = []

        for ind in range(result_num):
            frame_boxes = detections[ind][i]['boxes']
            if len(frame_boxes) == 0:
                #final_result[i] = None
                continue
            frame_boxes = clip_boxes(frame_boxes, width, height)
            boxes.append(frame_boxes)
            scores.append(detections[ind][i]['scores'])
            labels.append(detections[ind][i]['labels'])

        if len(boxes) > 0:
            boxes = np.concatenate(boxes, axis=0)
            scores = np.concatenate(scores)
            labels = np.concatenate(labels)
            if len(boxes) == 1:
                final_result[i]=None
                continue

            affmat = get_affi_mat(boxes)
            u, s, vh = np.linalg.svd(affmat)
            #cluster_num = len(np.where(s>1.0)[0])
            cluster_num = len(np.where(s>s[0]*0.2)[0])

            if cluster_num == len(boxes):
                final_result[i] = None
                continue
            elif cluster_num == 1:
                box_temp = np.mean(boxes, axis=0)
                if filter_out_box(boxes, box_temp):
                    final_result[i] = None
                    continue
                boxes_frame = [box_temp]
                scores_frame = [np.mean(scores)]
                labels_frame = [vote(labels)]
            else:
                clustering = SpectralClustering(n_clusters=cluster_num, affinity='precomputed')
                affmat += 0.5
                affmat = np.minimum(affmat, 1.0)
                res = clustering.fit_predict(affmat)

                boxes_frame = []
                scores_frame = []
                labels_frame = []
                for clu_num in range(cluster_num):
                    inds = np.where(res==clu_num)
                    if len(inds[0])<=1:
                        continue
                    box_cluster = boxes[inds]
                    score_cluster = scores[inds]
                    label_cluster = labels[inds]

                    if non_overlap_boxes(box_cluster):
                        #print('abondon {} in frame {}'.format(box_new, i))
                        continue

                    if not debug_cluster:
                        single_box = np.mean(box_cluster, axis=0)
                        if filter_out_box(box_cluster, single_box):
                            continue
                        single_score = np.mean(score_cluster)
                        single_label = vote(label_cluster)
                        boxes_frame.append(single_box)
                        scores_frame.append(single_score)
                        labels_frame.append(single_label)
                    else:
                        boxes_frame.append(box_cluster)
                        scores_frame.append(score_cluster)
                        labels_frame.append(label_cluster)

            boxes_frame = np.array(boxes_frame)
            scores_frame = np.array(scores_frame)
            labels_frame = np.array(labels_frame)
            if no_label:
                labels_frame = np.ones_like(labels_frame)
            frame_result = {'boxes':boxes_frame, 'scores':scores_frame, 'labels':labels_frame}
            final_result[i] = frame_result
        if i not in final_result:
            final_result[i] = None
        if debug_cluster:
            if i > 301:
                break
        #i += 1
    return final_result

def clip_result(result, width, height):
    for key, val in result.items():
        if val is None:
            continue
        else:
            result[key]['boxes'] = clip_boxes(result[key]['boxes'], width, height)
    return result

def get_max_iou(boxes, box):
    max_overlap = 0
    for abox in boxes:
        iou = get_iou(abox, box)
        max_overlap = max(max_overlap, iou)
    return max_overlap

def filter_out_box(boxes, box, threshold=0.5):
    max_iou = get_max_iou(boxes, box)
    if max_iou < threshold:
        return True
    else:
        return False

def vote(labels):
    labels = labels.astype(int)
    max_count_label = np.argmax(np.bincount(labels))
    return max_count_label

def merge_result_spectral_clustering(detections, class_num, cluster_num_max=5, one_class=False ):
    result_num = len(detections)
    final_result = {}
    if one_class:
        class_num=None

    for ind in range(result_num-1):
        assert set(list(detections[ind].keys())) == set(list(detections[ind+1].keys()))
        
    for i in detections[0]:
        boxes = []
        scores = []
        labels = []
        boxes_all = []
        scores_all = []
        labels_all = []
        for ind in range(result_num):
            frame_boxes = detections[ind][i]['boxes']
            if len(frame_boxes) == 0:
                #final_result[i] = None
                continue
            boxes.append(frame_boxes)
            scores.append(detections[ind][i]['scores'])
            labels.append(detections[ind][i]['labels'])
        if len(boxes) > 0:
            boxes = np.concatenate(boxes, axis=0)
            scores = np.concatenate(scores)
            labels = np.concatenate(labels)
            if class_num is None or class_num==1:
                labels = np.ones_like(labels)
                class_num = 1
            for cls in range(1, class_num+1):
                class_ind = np.where(labels==cls)
                if len(class_ind[0]) == 0:
                    continue
                boxes_class = boxes[class_ind]
                labels_class = labels[class_ind]
                scores_class = scores[class_ind]

                if len(boxes_class) <= cluster_num_max:
                    boxes_all.append(boxes_class)
                    labels_all.append(labels_class)
                    scores_all.append(scores_class)
                else:
                    affmat = np.zeros((len(boxes_class), len(boxes_class)))
                    for idx, r0 in enumerate(boxes_class) :
                        for idy, r1 in enumerate(boxes_class) :
                            affmat[idx,idy] = get_iou(r0,r1) + 0.05
                            affmat[idx,idy] = np.min( [ affmat[idx,idy], 1.0 ] )
                    cluster_num = min(cluster_num_max, len(boxes_class))


                    clustering = SpectralClustering(n_clusters=cluster_num, affinity='precomputed')
                    res = clustering.fit_predict(affmat)

                    box_class = []
                    score_class = []
                    label_class = []
                    for clu_num in range(cluster_num):
                        inds = np.where(res==clu_num)
                        if len(inds[0])==0:
                            print('wrong')
                            continue
                        box_new = boxes_class[inds]
                        score_new = scores_class[inds]
                        box_new = np.mean(box_new, axis=0)
                        score_new = np.mean(score_new)
                        box_class.append(box_new)
                        score_class.append(score_new)
                        label_class.append(cls)
                    boxes_all.append(box_class)
                    scores_all.append(score_class)
                    labels_all.append(label_class)

                #boxes_all.append(boxes_class)
                #scores_all.append(scores_class)
                #labels_all.append(labels_class)
            #print('boxes', boxes)
            #print('scores', scores)
            #print('labels', labels)
            boxes_all = np.concatenate(boxes_all, axis=0)
            #print(scores_all)
            #print(labels_all)
            scores_all = np.concatenate(scores_all, axis=0)
            labels_all = np.concatenate(labels_all, axis=0)
            frame_result = {'boxes':boxes_all, 'scores':scores_all, 'labels':labels_all}
            final_result[i] = frame_result
        if i not in final_result:
            final_result[i] = None
        #i += 1
    return final_result

def visulize(im, boxes, labels, scores, class_num, class_names):
    color = (255, 0, 255)
    colors = random_colors(class_num, GBR=True)
    #print(labels.shape)
    #print(len(labels.shape))
    for box, label, score in zip(boxes, labels, scores):
        color = colors[int(label)-1]
        class_name = class_names[int(label)-1]
        cv2.rectangle(im, tuple(box[:2]), tuple(box[2:]),color, lineType=4)
        pos = (int(box[0]), int(box[1]+20))
        #cv_put_text(im, 'label: '+), pos, cv2.FONT_HERSHEY_COMPLEX, 1, (255, 0, 255), 2 )
        #cv_put_text(im, 'label: '+), pos, cv2.FONT_HERSHEY_COMPLEX, 1, (255, 0, 255), 2 )
        cv_put_text_with_box(im, class_name, pos, box_color=color)
        #pos = (int(box[0]), int(box[1]+40))
        #cv_put_text(im, 'score: {:.3f}'.format(score), pos, cv2.FONT_HERSHEY_COMPLEX, 1, (255, 0, 255), 2 )

def visulize_debug(im, boxes_group, labels, scores, class_num, class_names):
    colors = random_colors(15, GBR=True)
    #for i, (boxes, label, score) in enumerate(zip(boxes_group, labels, scores)):
    
    for i, boxes in enumerate(boxes_group):
        color = colors[i%15]
        class_name = 'c{}'.format(i)
        for box in boxes:
            cv2.rectangle(im, tuple(box[:2]), tuple(box[2:]),color, lineType=4)
            pos = (int(box[0]), int(box[1]+20))
            #cv_put_text(im, 'label: '+), pos, cv2.FONT_HERSHEY_COMPLEX, 1, (255, 0, 255), 2 )
            #cv_put_text(im, 'label: '+), pos, cv2.FONT_HERSHEY_COMPLEX, 1, (255, 0, 255), 2 )
            cv_put_text_with_box(im, class_name, pos, box_color=color)
            #pos = (int(box[0]), int(box[1]+40))
            #cv_put_text(im, 'score: {:.3f}'.format(score), pos, cv2.FONT_HERSHEY_COMPLEX, 1, (255, 0, 255), 2 )

def non_overlap_boxes(boxes, thre=0.5):
    if len(boxes) == 1:
        return False
    aff_mat = get_affi_mat(boxes)
    box_num = len(boxes)
    average_overlap = np.sum(aff_mat - np.eye(box_num)) /2/ box_num
    if average_overlap < thre:
        return True
    else:
        return False

def get_affi_mat(boxes):
    affmat = np.zeros((len(boxes), len(boxes)))
    for idx, r0 in enumerate(boxes) :
        for idy, r1 in enumerate(boxes) :
            affmat[idx,idy] = get_iou(r0,r1) 
    return affmat


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--type', default='human', 
                        type=str, help='Human or garment')
    parser.add_argument('--width', default=1920, 
                        type=int, help='width of video')
    parser.add_argument('--height', default=1080,
                        type=int, help='height of video')
    parser.add_argument('--no_merging', action='store_true', 
                        help='Do we just want to have the original result')
    parser.add_argument('--debug', action='store_true', 
                        help='Do we want to debug')
    parser.add_argument('--load_pkl', action='store_true', 
                        help='Do we want to load merged result for test')
    parser.add_argument('--gen_no_label_gt', action='store_true', 
                        help='Do we want to generate no label ground truth')
    args = parser.parse_args()

    the_type = args.type
    video_root_path = os.path.expanduser('~/data/fashion_videos/')
    video_name = 'IDA'
    video_path = os.path.join(video_root_path, '{}.mp4'.format(video_name))
    if args.debug:
        print('debug mode')

    if the_type == 'human':
        class_num = 1
        result_root = 'human_detection_result'
        #out_path = os.path.join('out_videos', '{}_merge_human.mp4'.format(video_name))
        #out_pkl_path = os.path.join(result_root, '{}_merged_human.pkl'.format(video_name))
        if args.debug:
            out_path = os.path.join('out_videos', '{}_merge_human_all10_svd_debug.mp4'.format(video_name))
            out_pkl_path = os.path.join(result_root, '{}_merged_human_all10_svd_debug.pkl'.format(video_name))
        else:
            out_path = os.path.join('out_videos', '{}_merge_human_all10_svd.mp4'.format(video_name))
            out_pkl_path = os.path.join(result_root, '{}_merged_human_all10_svd.pkl'.format(video_name))
        #results_name = ['IDA_frcnn_416', 'IDA_yolo_v3_416', 'IDA_centernet_416', 'IDA_ssd_416']
        results_name = ['IDA_frcnn_416', 'IDA_yolo_v3_416', 'IDA_centernet_416', 'IDA_ssd_416', 
                        'IDA_centernet_800', 'IDA_yolo_v3_800', 'IDA_ssd_800', 'IDA_frcnn_800',
                        'IDA_centernet_1216', 'IDA_frcnn_1216']
        class_names = ['human']
    elif the_type == 'garment':
        class_num = 13
        result_root = 'garment_detection_result'
        out_path = os.path.join('out_videos', '{}_merge_garment_spe_all_svd.mp4'.format(video_name))
        out_pkl_path = os.path.join(result_root, '{}_merged_garment_spe_all_svd.pkl'.format(video_name))
        if args.gen_no_label_gt:
            out_json_path = os.path.join(result_root, '{}_merged_garment_spe_all_svd_no_label.json'.format(video_name))
        else:
            out_json_path = os.path.join(result_root, '{}_merged_garment_spe_all_svd.json'.format(video_name))
        #results_name = ['IDA_frcnn_256', 'IDA_yolo_v3_256', 'IDA_centernet_256', 'IDA_ssd_256']
        results_name = ['IDA_frcnn_128_refer_anno', 'IDA_yolo_v3_128_refer_anno', 'IDA_centernet_128_refer_anno', 'IDA_ssd_128_refer_anno', 
                        'IDA_centernet_256_refer_anno', 'IDA_yolo_v3_256_refer_anno', 'IDA_ssd_256_refer_anno', 'IDA_frcnn_256_refer_anno',
                        'IDA_centernet_512_refer_anno', 'IDA_frcnn_512_refer_anno', 'IDA_yolo_v3_512_refer_anno', 'IDA_ssd_512_refer_anno']
        #results_name = ['IDA_centernet_256', 'IDA_ssd_256']
        class_names = ['bag', 'belt', 'boots', 'footwear', 'outer', 'dress', 'sunglasses', 'pants',
                        'top', 'shots', 'skirt', 'headwear', 'scarf_tie']
        #results_name = ['IDA_centernet_256']
    else:
        raise ValueError('Invalid type!')
    #results_name = ['IDA_frcnn_416', 'IDA_centernet_416', 'IDA_ssd_416']
    #results_name = ['IDA_yolo_v3_416']

    results = []
    for name in results_name:
        path = os.path.join(result_root, '{}.pkl'.format(name))
        with open(path, 'rb') as f:
            result = pickle.load(f)
            results.append(result)

    #merged_result = merge_result(results, class_num)
    #merged_result = merge_result_spectral_clustering(results, class_num, one_class=True)
    if args.load_pkl:
        with open(out_pkl_path, 'rb')as f:
            merged_result = pickle.load(f)
            print('result is loaded from {}'.format(out_pkl_path))
    else:
        if args.no_merging:
            merged_result = merge_result_all(results)
        else:
            merged_result = merge_result_spectral_clustering_svd(results, args.width, args.height, debug_cluster=args.debug, no_label=args.gen_no_label_gt)
            # clip boxes
            merged_result = clip_result(merged_result, args.width, args.height)
            with open(out_pkl_path, 'wb') as f:
                pickle.dump(merged_result, f)
            print('merged result has been saved to {}'.format(out_pkl_path))

    #json_result = []
    #for frame in merged_result:
    #    image_id = frame
    #    result = merged_result[frame]
    #    if result is not None:
    #        scores = result['scores']
    #        labels = result['labels']
    #        boxes = result['boxes']
    #        for box, score, label in zip(boxes, scores, labels):
    #            single_result = {'image_id': image_id,
    #                             'category_id': int(label),
    #                             'bbox':box.tolist(),
    #                             'score':float(score)}
    #            json_result.append(single_result)
    
    json_result = convert_result_to_coco_gt(merged_result)
    
    with open(out_json_path, 'w') as f:
        json.dump(json_result, f)
        print('json file is saved to {}'.format(out_json_path))
    ##exit()
                
    ## visualize the result
    #cap = cv2.VideoCapture(video_path)
    #width  = cap.get(cv2.CAP_PROP_FRAME_WIDTH)   # float
    #height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)  # float
    #fps = cap.get(cv2.CAP_PROP_FPS)
    #size = (int(width), int(height))
    #fourcc = 'mp4v'
    #out = cv2.VideoWriter(out_path, cv2.VideoWriter_fourcc(*fourcc), fps, size)
    
    #frame_num = 1
    #while(True):
    #    ret, frame = cap.read()
    #    if not ret:
    #        break

    #    #im = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    #    im = frame
    #    cv_put_text_with_box(im, 'frame{}'.format(frame_num), (0,0), font_scale=2, box_color=(255,255,255))
    #    
    #    #result = merged_result[frame_num]
    #    if frame_num not in merged_result:
    #        result = None
    #    else:
    #        result = merged_result[frame_num]
    #    if result is not None:
    #        scores = result['scores']
    #        labels = result['labels']
    #        boxes = result['boxes']
    #        #if len(labels.shape)==0:
    #        #    labels = np.array([labels])
    #        #    merged_result[frame_num]['labels'] = labels

    #        
    #        #print('scores', scores)
    #        #print('labels', labels)
    #        if args.debug:
    #            visulize_debug(im, boxes, labels, scores, class_num, class_names)
    #        else:
    #            visulize(im, boxes, labels, scores, class_num, class_names)
    #    out.write(im)
    #    frame_num += 1
    #    if frame_num > 300:
    #        break
    #out.release()
    #print('video is saved to {}'.format(out_path))
    