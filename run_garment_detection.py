import argparse
import os
import pickle
from dev import get_dataset
from dev import get_models, get_garment_model
from dev import parse_video_list
from dev import run_detect, run_detect_on_shot
from dev import load_classification_model
from dev import get_transnet, extract_shots, get_garment_result
from dev import convert_result_to_coco_style
from dev import benchmark_coco
from dev import get_video_info
from benchmark_model import load_pkl_result

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    #parser.add_argument('--dataset_res', default=416, 
    #                    type=int, help='Human dataset resolution')
    parser.add_argument('--tracker', default='tracker.pkl',
                        type=str, help='tracker path')
    #parser.add_argument('--human_res', default=416, 
    #                    type=int, help='Human detector resolution')
    parser.add_argument('--garment_detector', default='centernet', choices=['centernet', 'yolo_v3', 'frcnn', 'ssd'],
                        type=str, help='Garment detector')
    parser.add_argument('--garment_res', default=256, 
                        type=int, help='Garment detector resolution')
    parser.add_argument('--device', default='cuda', 
                        type=str, help='Garment detector resolution')
    parser.add_argument('--load_pkl', dest='load_pkl', action='store_true', 
                        help='Do we want to load the detection result from pickle file for benchmark')
    parser.add_argument('--benchmark', dest='benchmark', action='store_true',
                        help='Do we want to benchmark the result')
    parser.add_argument('--no_label', dest='no_label', action='store_true',
                        help='Do we want to benchmark the result without label?')
    parser.add_argument('--video_list', default='video_names.txt', 
                        type=str, help='file path for video names')
    #parser.add_argument('--video_shots', default='video_shots.txt', 
    #                    type=str, help='file path for valid video shots')
    args = parser.parse_args()
    print(args)

    garment_detector = args.garment_detector
    garment_resolution = args.garment_res
    #dataset_res = args.dataset_res
    device = args.device
    load_pkl = args.load_pkl
    benchmark = args.benchmark

    tracker = load_pkl_result(args.tracker)
    #valid_track_ids = [17,111,152,184,234,269,284,301,313,329,338,402,482,528,535,548,553,566,612,655,705,763,782,786,820,831,879,948,962]
    valid_track_ids = [17,111,152,184,234,269,284,301,313,329,338,402,482,528,535,548,553,566,612,655,705,763,782,786,820,831,879,948,962,1021,1043,1062,1072,1087,1119,1172,1219,1263,1265,1296,1307,1339,1415,1528,1554,1575,1582,1599,1611,1660,1719,1814,1846,1848,1872,1879,1883,1935,1966,1998,2035,2075,2087,2102,2106,2110,2112,2142,2161,2199,2233,2271,2315,2367,2373,2387,2423,2468,2575,2600,2604,2623,2640,2678,2733,2806,2826,2841,2876,2929,2951,2994,3055,3088,3111,3122,3150,3160,3186,3209,3269,3347,3396,3410,3434,3463,3486,3533,3580,3649,3665,3676,3695,3746,3809,3842,3856,3884,3892,3937,3962,4027,4079,4113,4129,4145,4167,4209,4242,4289,4325,4360,4378,4379,4395,4412,4431,4504,4609,4630,4651,4673,4676,4687,4693,4716,4757,4776,4830,4875,4895,4939,4965,4993,5053,5166,5209,5218,5242,5270,5303,5343,5404,5438,5457,5466,5492,5517,5563,5662,]

    #valid_track_ids = [17,79,141,169,213,235,262,277,301,348,360,403,489,518,545,554,573,636,668,752,771,767,807,855,894,932,1025,1057,1065,1067,1076,1130,1166,1212,1256,1283,1274,1323,1346,1381,1429,1527,1560,1565,1588,1591,1592,1596,1637,1720,1796,1815,1816,1831,1838,1848,1857,1939,1956,1999,2006,2012,2056,2074,2092,2107,2126,2130,2157,2177,2237,2267,2330,2385,2395,2408,2490,2510,2611,2633,2639,2660,2669,2722,2763,2851,2863,2876,2895,2905,2961,2994,3007,3109,3114,3136,3145,3181,3188,3210,3267,3301,3390,3425,3432,3440,3463,3490,3529,3579,3589,3615,3627,3656,3692,3695,3711,3767,3802,3899,3914, 3942, 3961,3991, 4021, 4059,4086,4158,4209,4221,4238,4267,4331,4355,4389, 4412, 4460,4457,4473,4481,4487,4537,4583,4666,4718,4730,4733,4769,4801,4839,4884,4983,5008,5055,5156,5204,5300,5349,5392,5453,5488,5556,5594,5615,5641,5657,5671,5711,5753,5820,5903]
    #valid_track_ids = [41,143,209,245,331,389,413,447,513,593,740,823,828,832,866,952,1026,1088,1207,1240,1241,1310,1329,1412,1504,1622,1641,1726,1729,1712,1766,1775,1838,1906,2022,2096,2139,2157,2230,2350,2518,2594,2641,2739,2828,2951,2995,3020,3043,3056,3140,3182,3238,3278,3357,3382,3390,3412,3448,3520,3542,3573,3626,3720,3805,3815,3831,3900,3954,4138,4172,4187,4212,4227,4289,4364,4512,4559,4573,4583,4614,4628,4735,4772,4824,4957,5003,5031,5063,5097,5100,5170,5213,5292,5443,5504,5515,5531,5558,5618,5668,5736,5823,5902,5942,5960,6012,6089,6210,6252,6284,6332,6335,6436,6473,6541,6657,6728,6746,6778,6843,6897,6972,7022,7067,7110,7111,7164,7154,7159,7210,7301,7457,7535,7565,7570,7585,7641,7723,7764,7840,7953,8007,8021,8047,8094,8170,8348,8401,8424,8468,8517,8580,8651,8764,8834,8816,8849,8859,8921,8961,9027,9176,9281,9412,9414,9413,9415,9681]

    #video_paths = [os.path.expanduser('~/data/fashion_clips/videos/example_video2.mp4')]
    video_root_path = os.path.expanduser('~/data/fashion_videos/')
    video_names, video_paths, valid_shots = parse_video_list(args.video_list, video_root_path, video_shots=None)
    print(video_names)
    print(video_paths)
    print(valid_shots)
    #transnet_path = './transnet/model/transnet_model-F16_L3_S2_D256'

    result_root = 'garment_detection_result'

    #shot_num = 1
    #for score in shots:
    #    if score>0.1:
    #        shot_num += 1
    #print('There are {} shots in the video'.format(shot_num))

    #print('dataset done')
    if benchmark:
        garment_model = get_garment_model(garment=garment_detector, garment_res=garment_resolution, garment_score_thre=0.001)
        save_result = False
    else:
        if not load_pkl:
            garment_model = get_garment_model(garment=garment_detector, garment_res=garment_resolution)
        save_result = True
    print('model done')
    #classification_model = load_classification_model(device=device)
    for video_path, video_name in zip(video_paths, video_names):
        if benchmark:
            if args.no_label:
                out_json_path = os.path.join(result_root, '{}_{}_{}_benchmark_no_label.json'.format(video_name, garment_detector, garment_resolution))
            else:
                out_json_path = os.path.join(result_root, '{}_{}_{}_benchmark.json'.format(video_name, garment_detector, garment_resolution))
        else:
            out_json_path = os.path.join(result_root, '{}_{}_{}.json'.format(video_name, garment_detector, garment_resolution))

        if load_pkl:
            out_path = os.path.join(result_root, '{}_{}_{}.pkl'.format(video_name, garment_detector, garment_resolution ))
            result = load_pkl_result(out_path)
        else:
            print('start garment detection')
            #dataset, video_info = get_dataset('yolo_v3', dataset_res, video_path)
            video_info = get_video_info(video_path)
            video_info['out_path'] = 'out_videos/{}_garment_{}_{}_all.mp4'.format(video_name, garment_detector, garment_resolution)
            video_info['video_name'] = video_name
            result = get_garment_result(video_path, tracker, garment_model, garment_detector, valid_track_ids, garment_resolution, video_info, save_result=save_result, no_label=args.no_label )
        if benchmark:
            convert_result_to_coco_style(result, out_json_path)
            dt_path = out_json_path
            if args.no_label:
                gt_path = os.path.join(result_root, '{}_merged_garment_spe_all_svd_no_label.json'.format(video_name))
            else:
                gt_path = os.path.join(result_root, '{}_merged_garment_spe_all_svd.json'.format(video_name))
            print(gt_path)
            benchmark_coco(gt_path, dt_path)
            

        #run_detect_on_shot(dataset, human_model, garment_model, garment_detector, video_info, shots, classification_model=classification_model, garment_res=garment_resolution, valid_shots=valid_shots[video_name] )
        #run_detect_on_shot(dataset, human_model, garment_model, garment_detector, video_info, shots, classification_model=classification_model, garment_res=garment_resolution, valid_shots=None )