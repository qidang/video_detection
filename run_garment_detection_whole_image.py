import argparse
import os
import pickle
from dev import get_dataset
from dev import get_garment_model
from dev import parse_video_list
from dev import run_detect, run_detect_on_shot
from dev import load_classification_model
from dev import get_transnet, extract_shots, save_garment_result

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--garment_detector', default='centernet', choices=['centernet', 'yolo_v3', 'frcnn', 'ssd'],
                        type=str, help='Garment detector')
    parser.add_argument('--garment_res', default=800, 
                        type=int, help='Garment detector resolution')
    parser.add_argument('--benchmark', action='store_true',
                        help='Do we want to save result for benchmark')
    parser.add_argument('--device', default='cuda', 
                        type=str, help='Garment detector resolution')
    parser.add_argument('--video_list', default='video_names.txt', 
                        type=str, help='file path for video names')
    args = parser.parse_args()
    print(args)

    garment_detector = args.garment_detector
    garment_resolution = args.garment_res
    dataset_res = args.garment_res
    device = args.device
    benchmark = args.benchmark

    #video_paths = [os.path.expanduser('~/data/fashion_clips/videos/example_video2.mp4')]
    video_root_path = os.path.expanduser('~/data/fashion_videos/')
    video_names, video_paths, valid_shots = parse_video_list(args.video_list, video_root_path, video_shots=None)
    print(video_names)
    print(video_paths)

    root = 'garment_detection_result'

    #shot_num = 1
    #for score in shots:
    #    if score>0.1:
    #        shot_num += 1
    #print('There are {} shots in the video'.format(shot_num))

    #print('dataset done')
    if benchmark:
        garment_model = get_garment_model(garment=garment_detector, garment_res=garment_resolution, garment_score_thre=0.001, use_whole_image_detector=True)
    else:
        garment_model = get_garment_model(garment=garment_detector, garment_res=garment_resolution, garment_score_thre=None, use_whole_image_detector=True)
    print('model done')
    #classification_model = load_classification_model(device=device)
    for video_path, video_name in zip(video_paths, video_names):
        dataset, video_info = get_dataset(garment_detector, dataset_res, video_path)
        video_info['out_path'] = 'out_videos/{}_human_{}_{}_garment_{}_{}_all.mp4'.format(video_name, garment_detector, garment_resolution, garment_detector, garment_resolution)
        video_info['video_name'] = video_name
        if benchmark:
            out_path = os.path.join(root, '{}_{}_{}_whole_benchmark.pkl'.format(video_name, garment_detector, garment_resolution ))
        else:
            out_path = os.path.join(root, '{}_{}_{}_whole.pkl'.format(video_name, garment_detector, garment_resolution ))

        save_garment_result(dataset, garment_model, garment_detector, garment_resolution, video_info, out_path )
        #run_detect_on_shot(dataset, human_model, garment_model, garment_detector, video_info, shots, classification_model=classification_model, garment_res=garment_resolution, valid_shots=valid_shots[video_name] )
        #run_detect_on_shot(dataset, human_model, garment_model, garment_detector, video_info, shots, classification_model=classification_model, garment_res=garment_resolution, valid_shots=None )