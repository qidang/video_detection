import argparse
import os
import pickle
import numpy as np

from dev import get_dataset
from dev import get_models, get_garment_model
from dev import parse_video_list
from dev import run_detect, run_detect_on_shot
from dev import load_classification_model
from dev import get_transnet, extract_shots, get_garment_result
from dev import convert_result_to_coco_style
from dev import benchmark_coco
from dev import get_video_info
from benchmark_model import load_pkl_result

def set_no_label(result):
    for key in result:
        if result[key] is not None:
            result[key]['labels'] = np.ones_like(result[key]['labels'])
    return result

def cal_inter_boxa(boxA, boxB):
    # determine the (x, y)-coordinates of the intersection rectangle
    xA = max(boxA[0], boxB[0])
    yA = max(boxA[1], boxB[1])
    xB = min(boxA[2], boxB[2])
    yB = min(boxA[3], boxB[3])
    # compute the area of intersection rectangle
    interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)
    # compute the area of both the prediction and ground-truth
    # rectangles
    boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
    inter = interArea / boxAArea

    return inter

def get_biggest_inter(box, boxes):
    max_inter = 0
    for abox in boxes:
        inter = cal_inter_boxa(box, abox)
        max_inter = max(max_inter, inter)
    return max_inter

def keep_box(box, boxes, thre=0.8):
    if get_biggest_inter(box, boxes) < thre:
        return False
    else:
        return True

def filter_garments_from_tracker(tracker, result, valid_track_ids, inter_thre=0.8):
    reuslt_new = {}
    model_nodes, human_nodes = tracker.convert_tracks(one_model_per_shot=False, valid_track_ids=valid_track_ids)
    for frame, node_list in model_nodes.items():
        boxes = [node.box for node in node_list]
        boxes = np.array(boxes)

        boxes_frame = []
        labels_frame = []
        scores_frame = []
        for i, box in enumerate(result[frame]['boxes']):
            if keep_box(box, boxes, inter_thre):
                boxes_frame.append(box)
                labels_frame.append(result[frame]['labels'][i])
                scores_frame.append(result[frame]['scores'][i])

        reuslt_new[frame] = {'boxes':np.array(boxes_frame),
                             'labels':np.array(labels_frame),
                             'scores':np.array(scores_frame)}
    return reuslt_new
            

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    #parser.add_argument('--dataset_res', default=416, 
    #                    type=int, help='Human dataset resolution')
    parser.add_argument('--tracker', default='tracker.pkl',
                        type=str, help='tracker path')
    parser.add_argument('--garment_detector', default='centernet', choices=['centernet', 'yolo_v3', 'frcnn', 'ssd'],
                        type=str, help='Garment detector')
    parser.add_argument('--garment_res', default=800, 
                        type=int, help='Garment detector resolution')
    parser.add_argument('--no_label', dest='no_label', action='store_true',
                        help='Do we want to benchmark the result without label?')
    parser.add_argument('--video_list', default='video_names.txt', 
                        type=str, help='file path for video names')
    #parser.add_argument('--video_shots', default='video_shots.txt', 
    #                    type=str, help='file path for valid video shots')
    args = parser.parse_args()
    print(args)

    garment_detector = args.garment_detector
    garment_resolution = args.garment_res
    #dataset_res = args.dataset_res

    tracker = load_pkl_result(args.tracker)
    #valid_track_ids = [17,111,152,184,234,269,284,301,313,329,338,402,482,528,535,548,553,566,612,655,705,763,782,786,820,831,879,948,962]
    valid_track_ids = [17,111,152,184,234,269,284,301,313,329,338,402,482,528,535,548,553,566,612,655,705,763,782,786,820,831,879,948,962,1021,1043,1062,1072,1087,1119,1172,1219,1263,1265,1296,1307,1339,1415,1528,1554,1575,1582,1599,1611,1660,1719,1814,1846,1848,1872,1879,1883,1935,1966,1998,2035,2075,2087,2102,2106,2110,2112,2142,2161,2199,2233,2271,2315,2367,2373,2387,2423,2468,2575,2600,2604,2623,2640,2678,2733,2806,2826,2841,2876,2929,2951,2994,3055,3088,3111,3122,3150,3160,3186,3209,3269,3347,3396,3410,3434,3463,3486,3533,3580,3649,3665,3676,3695,3746,3809,3842,3856,3884,3892,3937,3962,4027,4079,4113,4129,4145,4167,4209,4242,4289,4325,4360,4378,4379,4395,4412,4431,4504,4609,4630,4651,4673,4676,4687,4693,4716,4757,4776,4830,4875,4895,4939,4965,4993,5053,5166,5209,5218,5242,5270,5303,5343,5404,5438,5457,5466,5492,5517,5563,5662]


    #video_paths = [os.path.expanduser('~/data/fashion_clips/videos/example_video2.mp4')]
    video_root_path = os.path.expanduser('~/data/fashion_videos/')
    video_names, video_paths, valid_shots = parse_video_list(args.video_list, video_root_path, video_shots=None)
    print(video_names)
    print(video_paths)
    print(valid_shots)
    #transnet_path = './transnet/model/transnet_model-F16_L3_S2_D256'

    result_root = 'garment_detection_result'

    for video_path, video_name in zip(video_paths, video_names):
        
        if args.no_label:
            out_json_path = os.path.join(result_root, '{}_{}_{}_whole_benchmark_no_label.json'.format(video_name, garment_detector, garment_resolution))
        else:
            out_json_path = os.path.join(result_root, '{}_{}_{}_whole_benchmark.json'.format(video_name, garment_detector, garment_resolution))
        
        dt_pkl_path = os.path.join(result_root, '{}_{}_{}_whole_benchmark.pkl'.format(video_name, garment_detector, garment_resolution ))
        result = load_pkl_result(dt_pkl_path)
        if args.no_label:
            result = set_no_label(result)
        result = filter_garments_from_tracker(tracker, result, valid_track_ids)
        convert_result_to_coco_style(result, out_json_path)
        dt_path = out_json_path
        if args.no_label:
            gt_path = os.path.join(result_root, '{}_merged_garment_spe_all_svd_no_label.json'.format(video_name))
        else:
            gt_path = os.path.join(result_root, '{}_merged_garment_spe_all_svd.json'.format(video_name))
        print(gt_path)
        benchmark_coco(gt_path, dt_path)
            

        #run_detect_on_shot(dataset, human_model, garment_model, garment_detector, video_info, shots, classification_model=classification_model, garment_res=garment_resolution, valid_shots=valid_shots[video_name] )
        #run_detect_on_shot(dataset, human_model, garment_model, garment_detector, video_info, shots, classification_model=classification_model, garment_res=garment_resolution, valid_shots=None )