import numpy as np
from collections import OrderedDict
import matplotlib.pyplot as plt

def parse_log(log_path):
    results = []
    with open(log_path) as f:
        log_lines = f.readlines()
    for line in log_lines:
        if line.startswith('Namespace'):
            new_result = {}
            line = line[10:-2]
            key_vals = line.split(',')
            for key_val in key_vals:
                key_val = key_val.strip()
                key_val = key_val.split('=')
                key, val = key_val
                new_result[key] = val.strip('\'')
        if line.startswith(' Average Precision  (AP) @[ IoU=0.50      | area=   all | maxDets=100 ] = '):
            new_result['ap05'] = line[-6:-1]
            results.append(new_result)
    return results

def gen_out_human_detector(results, human_detector, human_resolution ):
    target_result = []
    for result in results:
        if result['human_detector'] == human_detector:
            if result['human_resolution'] == human_resolution:
                target_result.append(result)
    return target_result

def convert_result_type(results):
    for result in results:
        result['human_resolution'] = int(result['human_resolution'])
        result['garment_resolution'] = int(result['garment_resolution'])
        result['no_label'] = True if result['no_label']=='True' else False


def get_ordered_result(results, keys):
    for key in reversed(keys):
        results = sorted(results, key=key)
    return results

def print_result(results):
    for result in results:
        print('{}{}+{}{},{}'.format(result['human_detector'], result['human_resolution'], result['garment_detector'], result['garment_resolution'], result['ap05']))

def merge_result(results):
    #i = 0
    paired_results = []
    while len(results) > 0:
        if len(results) == 1:
            print('something goes wrong')
            print(result)
            result = results[0]
            if result['no_label']:
                result['loc_ap'] = result['ap05']
                result['cat_ap'] = None
            else:
                result['cat_ap'] = result['ap05']
                result['loc_ap'] = None
            paired_results.append(result)
            break
        cur_result = results[0]
        new_result, index = find_pair(cur_result, results[1:])
        if index is not None:
            paired_results.append(new_result)
            results.pop(index)
            results.pop(0)
        else:
            result = results[0]
            if result['no_label']:
                result['loc_ap'] = result['ap05']
                result['cat_ap'] = None
            else:
                result['cat_ap'] = result['ap05']
                result['loc_ap'] = None
            paired_results.append(result)
            results.pop(0)
    return paired_results


def find_pair(result1, results_rest):
    new_result = {}
    index = None
    for i, result in enumerate(results_rest):
        if result['garment_detector'] != result1['garment_detector']:
            continue
        if result['human_detector'] != result1['human_detector']:
            continue
        if result['garment_resolution'] != result1['garment_resolution']:
            continue
        if result['human_resolution'] == result1['human_resolution']:
            index = i+1
            for k, v in result1.items():
                new_result[k] = v
            if result1['no_label']:
                new_result['loc_ap'] = result1['ap05']
            else:
                new_result['cat_ap'] = result1['ap05']
            if result['no_label']:
                new_result['loc_ap'] = result['ap05']
            else:
                new_result['cat_ap'] = result['ap05']
            if 'cat_ap' not in new_result:
                new_result['cat_ap'] = None
                print('something is very wrong')
            if 'loc_ap' not in new_result:
                new_result['loc_ap'] = None
                print('something is also very wrong')
            break
    return new_result, index
            

def print_table_head(detector_name):
    head = '\\begin{table}[h!]\n    \\begin{small}\n    \\centering\n    \\begin{center}\n    \\begin{tabular}{ c|c|c|c|c }\n' + \
    '        \\multicolumn{5}{c}{\\textbf{System benchmark $AP_{0.5}$ and Timing}} \\\\\n        \\hline\n        Human Detector & \\multicolumn{4}{c}{\\textbf{'+detector_name+'}} \\\\\n        \\hline\n\
        Garment Detector & Garment Resolution & Localization & Categorization & Timing (seconds) \\\\\n\
        \\hline'
    print(head)

def print_table_bottom():
    bottom = '        \\hline\n\
    \\end{tabular}\n\
    \\caption{}\n\
    \\label{tab:system_benchmark}\n\
    \end{center}\n\
    \end{small}\n\
\end{table}'
    print(bottom)

def print_merged_result(results, human_detector, human_resolution):
    detecter_names = {'centernet':'CenterNet', 'yolo_v3':'YOLOv3', 'frcnn': 'Faster RCNN', 'ssd': 'SSD'}
    resolution_dict = {128:'128:64, patch', 256:'256:128, patch', 512:'512:256, patch', 800:'800, whole'}
    detector_name = '{} {}'.format(detecter_names[human_detector], human_resolution)
    #print_table_head(detector_name)
    for result in results:
        detecter_name = detecter_names[result['garment_detector']]
        resolution = resolution_dict[result['garment_resolution']]
        loc_score = result['loc_ap']
        cat_score = result['cat_ap']
        template = '{},{}'
        whole_str = template.format(loc_score, cat_score)
        #template = '        {0:13} & {1:15} & {2:8} & {3:8} &   \\\\'
        #whole_str = template.format(detecter_name, resolution, loc_score, cat_score)
        print(whole_str)
    #print_table_bottom()


def print_human_detector_result(results):
    detecter_names = {'centernet':'CenterNet', 'yolo_v3':'YOLOv3', 'frcnn': 'Faster RCNN', 'ssd': 'SSD'}
    resolution_dict = {128:'128:64, patch', 256:'256:128, patch', 512:'512:256, patch', 800:'800, whole'}
    pre_no_label = False

    whole_str = ''
    for result in results:
        if result['no_label'] and not pre_no_label:
            detecter_name = detecter_names[result['garment_detector']]
            resolution = resolution_dict[result['garment_resolution']]
            flag = '{}_{}'.format(result['garment_detector'], result['garment_resolution'])
            loc_score = result['ap05']
            whole_str = '{} & {} & {} & '.format(detecter_name, resolution, loc_score)
            pre_no_label = True
        elif pre_no_label and not result['no_label']:
            print('go to 2')
            new_flag = '{}_{}'.format(result['garment_detector'], result['garment_resolution'])
            assert flag == new_flag
            whole_str += result['ap05']+'  \\\\'
            pre_no_label = False
            print(whole_str)
        elif (pre_no_label and result['no_label']):
            print('condition:',pre_no_label and result['no_label'])
            print('pre no label; ', pre_no_label)
            print('no label: ', result['no_label'])
            print('go to 3')
            print(whole_str + '   \\\\')
            detecter_name = detecter_names[result['garment_detector']]
            resolution = resolution_dict[result['garment_resolution']]
            flag = '{}_{}'.format(result['garment_detector'], result['garment_resolution'])
            loc_score = result['ap05']
            whole_str = '{} & {} & {} & '.format(detecter_name, resolution, loc_score)
            pre_no_label = True
        else:
            print('go to 4')
            detecter_name = detecter_names[result['garment_detector']]
            resolution = resolution_dict[result['garment_resolution']]
            flag = '{}_{}'.format(result['garment_detector'], result['garment_resolution'])
            loc_score = result['ap05']
            whole_str = '{} & {} &      &{}  \\\\'.format(detecter_name, resolution, loc_score)
            pre_loc = False
            print(whole_str)
        
def draw_figure(results, no_label):
    detector_names = ['ssd','yolov3', 'frcnn', 'centernet']
    detector_res = [128, 256, 512, 800]
    garment_detecoters = [['ssd', 128], ['ssd', 256], ['ssd', 512], ['ssd', 800],
                          ['yolo_v3', 128], ['yolo_v3', 256], ['yolo_v3', 512], ['yolo_v3', 800],
                          ['centernet', 128], ['centernet', 256], ['centernet', 512], ['centernet', 800],
                          ['frcnn', 128], ['frcnn', 256], ['frcnn', 512], ['frcnn', 800],
                         ]
    human_detectors = [['ssd', 416], ['ssd', 800], 
                       ['yolo_v3', 416], ['yolo_v3', 800],
                       ['centernet', 416], ['centernet', 800], ['centernet', 1216],
                       ['frcnn', 416], ['frcnn', 800], ['frcnn', 1216],
                      ]
    if no_label:
        figure_type = 'Localization'
    else:
        figure_type = 'Categorization'
    convert_dict = {'ssd':'SSD', 'yolo_v3':'YOLOv3', 'centernet':'CenterNet', 'frcnn':'Faster RCNN'}
    garment_dict = OrderedDict( [[x[0]+' '+str(x[1]), i] for i, x in enumerate(garment_detecoters)])
    human_dict = OrderedDict( [[x[0]+' '+str(x[1]), i] for i, x in enumerate(human_detectors)])
    garment_name_dict = OrderedDict( [[convert_dict[x[0]]+' '+str(x[1]), i] for i, x in enumerate(garment_detecoters)])
    human_name_dict = OrderedDict( [[convert_dict[x[0]]+' '+str(x[1]), i] for i, x in enumerate(human_detectors)])
    #human_dict = {x[0]+' '+str(x[1]): i for i, x in enumerate(human_detectors)}
    result_shape = (len(garment_detecoters), len(human_detectors))
    result_mat = np.zeros(result_shape)
    for result in results:
        if result['no_label'] != no_label:
            continue
        
        garment_name = result['garment_detector']+' '+str(result['garment_resolution'])
        human_name = result['human_detector']+' '+str(result['human_resolution'])
        garment_ind = garment_dict[garment_name]
        human_ind = human_dict[human_name]
        ap = result['ap05']
        result_mat[garment_ind][human_ind] = ap
    ave_result = np.average(result_mat, axis=1)
    ind = np.argsort(ave_result)
    result_mat = result_mat[ind,:]
    print(ind)
    #print(result_mat)
    fig, ax = plt.subplots()  # Create a figure containing a single axes.
    x_ticks = list(garment_name_dict.keys())
    x_ticks = [x_ticks[i] for i in ind]
    y_labels = list(human_name_dict.keys())
    x_data = list(range(1, len(x_ticks)+1))
    for i in range(result_mat.shape[1]):
        y_data = result_mat[:,i]
        #print(x_data)
        #print(x_ticks)
        #print(y_data)
        ax.set_xlabel('Garment Detector')
        ax.set_ylabel('AP@0.5')
        ax.set_xticks(x_data)
        ax.set_xticklabels(x_ticks, rotation=90)
        #ax.grid()
        ax.plot(x_data, y_data, label=y_labels[i])  # Plot some data on the axes.
    ax.legend(loc='upper left', bbox_to_anchor=(1.01, 1),
        ncol=1, fancybox=True, shadow=False, title='Human Detector')
    plt.locator_params(axis='x', nbins=20)
    plt.grid()
    plt.title('Application Benchmark - {}'.format(figure_type))
    plt.savefig('application_benchmark_{}.jpg'.format(figure_type), dpi=300, bbox_inches='tight')

    #plt.show()
        


if __name__ == '__main__':
    #log_path = 'test_log.log'
    #key_order = ['frcnn', 'ssd','yolo_v3','centernet']
    #key_order_dict = {key:val for val, key in enumerate(key_order)}
    #key0 = lambda x: x['garment_detection_whole']
    #key1 = lambda x: key_order_dict[x['human_detector']]
    #key2 = lambda x: int(x['human_resolution'])
    #keys = [key0, key1, key2]
    #results = parse_log(log_path)
    #results = get_ordered_result(results, keys)
    #print_result(results)

    human_detectors = ['frcnn', 'yolo_v3','ssd','centernet']
    human_resolutions = [416, 800, 1216]
    key_order = ['centernet', 'yolo_v3','ssd','frcnn']
    key_order_dict = {key:val for val, key in enumerate(key_order)}
    key0 = lambda x: x['garment_detection_whole']
    key1 = lambda x: key_order_dict[x['garment_detector']]
    key2 = lambda x: int(x['garment_resolution'])
    #key3 = lambda x: not x['no_label']
    keys = [key0, key1, key2 ]

    log_path = 'logs/current.log'

    results = parse_log(log_path)
    convert_result_type(results)

    draw_figure(results, no_label=True)
    #for result in results:
    #    print(result)
    exit()

    for human_detector in human_detectors:
        for human_resolution in human_resolutions:
            print('Human Detector: {}'.format(human_detector))
            print('Human resolution: {}'.format(human_resolution))
            target_result = gen_out_human_detector(results, human_detector, human_resolution)
            #for result in target_result:
            #    print(result)
            merged_result = merge_result(target_result)
            merged_result = get_ordered_result(merged_result, keys)
            print(merged_result)
            #print_merged_result(merged_result, human_detector, human_resolution)
            #print_human_detector_result(target_result)
            print()
            break