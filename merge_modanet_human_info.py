import pickle
import numpy as np
import os
import shutil

def gen_human_map(human_info):
    human_map = {}
    for info in human_info:
        im_id = info['image_id']
        bbox = info['human_box']
        human_map[im_id] = bbox
    return human_map

def gen_human_map_boxes(human_info):
    human_map = {}
    for info in human_info:
        im_id = int(info['image_id'])
        bbox = info['human_box']
        human_map[im_id] = bbox
    return human_map

def merge(annos, human_map, filter_method='biggest'):
    ids = []
    num=0
    for anno in annos:
        im_id = anno['id']
        if im_id not in human_map:
            num+=1
            #print(im_id)
            continue
        else:
            ids.append(im_id)

        human_box = human_map[im_id]['boxes']
        human_box = filter_human_box(human_box, filter_method)
        anno['human_box_det'] = human_box
        anno['human_box'] = revise_box(human_box, anno)
    print('length of the common image id is {}'.format(len(ids)))
    print('lost ids num is {}'.format(num))

def filter_human_box(human_box, filter_method):
    if len(human_box) == 1:
        return human_box[0]
    elif len(human_box) == 0:
        return []
    else:
        if filter_method == 'biggest':
            return biggest_box(human_box)
        else:
            raise ValueError('wrong filter method')

def revise_box(human_box, anno):
    if human_box == []:
        x1, y1, w, h = anno['objects'][0]['bbox']
        x2 = x1+w
        y2 = y1+h
    else:
        x1, y1, x2, y2 = human_box[:4]

    x1 = max(0, x1)
    y1 = max(0, y1)
    x2 = min(x2, anno['width'])
    y2 = min(x2, anno['height'])

    for obj in anno['objects']:
        '''obj bbox is xywh mode'''
        x1 = min(x1, obj['bbox'][0])
        y1 = min(y1, obj['bbox'][1])
        x2 = max(x2, obj['bbox'][2]+obj['bbox'][0])
        y2 = max(y2, obj['bbox'][3]+obj['bbox'][1])
    return (x1, y1, x2, y2)

def merge_info(human_info, anno):
    print('anno key:', anno.keys())
    print('human key:', human_info.keys())
    merge(anno['train'], human_info['train2014'])
    merge(anno['val'], human_info['val2014'])

def overlap_bigger_than_thre(human_box, obj_box, thre=0.8):
    x_min = max(human_box[0], obj_box[0])
    y_min = max(human_box[1], obj_box[1])
    x_max = min(human_box[2], obj_box[2])
    y_max = min(human_box[3], obj_box[3])
    if x_max<=x_min or y_max<=y_min:
        return False
    overlap = (x_max-x_min)*(y_max-y_min)
    obj_area = (obj_box[2] - obj_box[0]) * (obj_box[3] - obj_box[1])
    return overlap / obj_area >= thre

def get_wrong_anno(human_info, anno):
    threshold = 0.7
    print('threshold is {}'.format(threshold))
    human_map = gen_human_map_boxes(human_info)
    wrong_ids_train = get_wrong_anno_part(anno['train'], human_map,threshold)
    wrong_ids_val = get_wrong_anno_part(anno['val'], human_map, threshold)
    print('Length of wrong ids trian is {}'.format(len(wrong_ids_train)))
    print('Length of wrong ids val is {}'.format(len(wrong_ids_val)))
    return wrong_ids_train+wrong_ids_val

def get_wrong_anno_part(annos, human_map, threshold=0.8):
    wrong_ids = []
    file_names = []
    for anno in annos:
        im_id = anno['id']
        file_name = anno['file_name']
        human_boxes = human_map[im_id]
        if is_wrong_anno(human_boxes, anno, threshold):
            wrong_ids.append(im_id)
            file_names.append(file_name)
    return file_names


def biggest_box(boxes):
    max_area=0
    max_ind = 0

    for i, box in enumerate(boxes):
        area = (box[2]-box[0])*(box[3]-box[1])
        if area > max_area:
            max_area = area
            max_ind=i
    return boxes[max_ind]

def is_wrong_anno(human_boxes, anno, threshold, criteria='score'):
    '''criteria can only be score or area'''
    thre = threshold
    if len(human_boxes) == 0:
        return True
    # filter the boxes that have score more than 0.8
    inds = np.where(human_boxes[:,4]>=0.8)
    human_boxes = human_boxes[inds]

    #if len(human_boxes) != 1:
    #    return True
    #hbox = human_boxes[0][:4]
    hbox = biggest_box(human_boxes)[:4]
    for obj in anno['objects']:
        obox = obj['bbox']
        obox[2] += obox[0]
        obox[3] += obox[1]
        if obj['category_id'] == 3 or obj['category_id'] == 4:
            continue
        if not overlap_bigger_than_thre(hbox, obox, thre=thre):
            return True
    return False
            
    
def copy_ims(root_folder, target_folder, im_names):
    for im_name in im_names:
        root = os.path.join(root_folder, im_name)
        target = os.path.join(target_folder, im_name)
        shutil.copyfile(root, target)

if __name__=='__main__':
    #train_human_pkl_path = 'human_info_modanet_train.pkl'
    #val_human_pkl_path = 'human_info_modanet_val.pkl'
    detectors = ['centernet', 'yolo_v3', 'ssd', 'frcnn']
    detectors = ['centernet', 'frcnn']
    resolutions = [1216]
    for detector in detectors:
        for resolution in resolutions:
            human_det_path = os.path.join('human_detection_result', 'modanet_human_{}_{}.pkl'.format(detector, resolution))
            out_path = os.path.expanduser('~/data/annotations/modanet_with_human_{}_{}.pkl'.format(detector, resolution))
            anno_path = os.path.expanduser('~/data/annotations/modanet2018_instances_revised.pkl')

            with open(human_det_path, 'rb') as f:
                human_info= pickle.load(f)

            with open(anno_path, 'rb') as f:
                anno = pickle.load(f)

            print('the result doesnot contain constrain for shoes')
            #im_names = get_wrong_anno(human_info, anno)
            merge_info(human_info, anno)
            with open(out_path, 'wb') as f:
                pickle.dump(anno, f)
                print('the result has been saved to {}'.format(out_path))

