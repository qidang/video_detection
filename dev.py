import os
import torch
import torchvision
import cv2
import numpy as np
import sys
import pickle
from easydict import EasyDict as edict
import json
from pycocotools import coco
from pycocotools.cocoeval import COCOeval
#sys.path.append('torchcore')
sys.path.append('pytorch_yolov3')
sys.path.append('ssd_pytorch')

from centernet.src.lib.models.model import create_model, load_model
from opts.centernet_opt import CenterNetOpts
from detectors.centernet_detector import CenterNetDetector

from video_dataset import VideoDataset
from transforms.centernet_transform import CenterNetTrasform
from transforms.frcnn_transform import FrcnnTransform
from transforms.patch_transform import PatchTransform
from transforms.yolov3_transform import YoloV3Transform
from transforms.batch_dataset import BatchDataset, collate_fn
from transforms.ssd_transform import SSDTransform
from transforms.ssd_transform import detection_collate
from ssd_pytorch.data import configs as ssd_config
from rcnn_config import config
from detectors.frcnn_detector import FrcnnDetecter
from detectors.yolov3_detector import YoloV3Detector
from detectors.ssd_detector import SSDDetector
from tracker.tracker import Tracker
from torchcore.data.transforms import ToTensor, Normalize, Compose, ResizeAndPadding, RandomMirror
from torchcore.dnn.networks.classification_net import ClassificationNet
from torchcore.dnn.networks.feature import resnet50
from torchcore.data.datasets import PatchDataset
from torchcore.tools.visulize_tools import cv_put_text_with_box, cv_put_text
from torchcore.tools.color_gen import random_colors
from torchcore.data.datasets import COCOPersonCV2Dataset
from torchcore.data.datasets import ModanetCV2Dataset

import ffmpeg
import numpy as np

from transnet.transnet import TransNetParams, TransNet
from transnet.transnet_utils import draw_video_with_predictions, scenes_from_predictions

from torchvision.ops import nms

def get_transnet(model_path):
    import tensorflow as tf
    gpus = tf.config.experimental.list_physical_devices('GPU')

    tf.config.experimental.set_visible_devices(gpus[0], 'GPU')
    tf.config.experimental.set_virtual_device_configuration(gpus[0], [tf.config.experimental.VirtualDeviceConfiguration(memory_limit=1024)])
    # initialize the network
    params = TransNetParams()
    #params.CHECKPOINT_PATH = "./model/transnet_model-F16_L3_S2_D256"
    params.CHECKPOINT_PATH = model_path

    net = TransNet(params)
    return net, params


def extract_shots(net, params, video_path):
    # export video into numpy array using ffmpeg
    video_stream, err = (
        ffmpeg
        #.input('/ssd/data/datasets/fashion_videos/IVY.mp4')
        .input(video_path)
        .output('pipe:', format='rawvideo', pix_fmt='rgb24', s='{}x{}'.format(params.INPUT_WIDTH, params.INPUT_HEIGHT))
        .run(capture_stdout=True)
    )
    video = np.frombuffer(video_stream, np.uint8).reshape([-1, params.INPUT_HEIGHT, params.INPUT_WIDTH, 3])

    # predict transitions using the neural network
    predictions = net.predict_video(video)
    return predictions

def parse_video_list(video_list, video_root, video_shots=None):
    with open(video_list, 'r') as f:
        video_lines = f.readlines()
        video_lines = [name.strip('\n') for name in video_lines]
        video_names = [name.split('.')[0] for name in video_lines]
        video_paths = [os.path.join(video_root, name) for name in video_lines]

    valid_shots = {}
    if video_shots is not None:
        with open(video_shots, 'r') as f:
            shot_lines = f.readlines()
            for line in shot_lines:
                name, shots = line.split()
                name = name.split('.')[0]
                shots = shots.strip('\n').split(',')
                valid_shots[name] = [int(shot) for shot in shots]

    return video_names, video_paths, valid_shots

def get_dataset(netname, resolution, video_path):
    #video_path = os.path.expanduser('~/data/fashion_clips/videos/example_video2.mp4')
    collate_fn = None
    if netname == 'centernet':
        transforms = CenterNetTrasform(size=resolution)
    elif netname == 'frcnn':
        transforms = FrcnnTransform(size=resolution)
    elif netname == 'yolo_v3':
        transforms = YoloV3Transform(size=resolution)
    elif netname == 'ssd':
        transforms = SSDTransform(size=resolution)
        collate_fn = detection_collate
    dataset = VideoDataset(video_path, transforms=transforms)
    info = dataset.get_info()
    dataset = torch.utils.data.DataLoader(dataset, batch_size=1, collate_fn=collate_fn)

    return dataset, info

def get_coco_dataset(netname, resolution, image_root, anno_path, part):
    #video_path = os.path.expanduser('~/data/fashion_clips/videos/example_video2.mp4')
    collate_fn = None
    if netname == 'centernet':
        transforms = CenterNetTrasform(size=resolution)
    elif netname == 'frcnn':
        transforms = FrcnnTransform(size=resolution)
    elif netname == 'yolo_v3':
        transforms = YoloV3Transform(size=resolution)
    elif netname == 'ssd':
        transforms = SSDTransform(size=resolution)
        collate_fn = detection_collate
    
    dataset = COCOPersonCV2Dataset(image_root, anno_path, part=part, transforms=transforms, test=True)
    dataset = torch.utils.data.DataLoader(dataset, batch_size=1, collate_fn=collate_fn)

    return dataset

def get_modanet_dataset(netname, resolution, image_root, anno_path, part):
    #video_path = os.path.expanduser('~/data/fashion_clips/videos/example_video2.mp4')
    collate_fn = None
    if netname == 'centernet':
        transforms = CenterNetTrasform(size=resolution)
    elif netname == 'frcnn':
        transforms = FrcnnTransform(size=resolution)
    elif netname == 'yolo_v3':
        transforms = YoloV3Transform(size=resolution)
    elif netname == 'ssd':
        transforms = SSDTransform(size=resolution)
        collate_fn = detection_collate
    
    dataset = ModanetCV2Dataset(image_root, anno_path, part=part, transforms=transforms, test=True)
    dataset = torch.utils.data.DataLoader(dataset, batch_size=1, collate_fn=collate_fn)

    return dataset

def get_video_info(video_path):
    cap = cv2.VideoCapture(video_path)
    length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    width  = cap.get(cv2.CAP_PROP_FRAME_WIDTH)   # float
    height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)  # float
    fps = cap.get(cv2.CAP_PROP_FPS)
    size = (int(width), int(height))
    codec = int(cap.get(cv2.CAP_PROP_FOURCC))
    fourcc = chr(codec&0xFF) + chr((codec>>8)&0xFF) + chr((codec>>16)&0xFF) + chr((codec>>24)&0xFF)
    cap.release()
    video_name = os.path.splitext(os.path.basename(video_path))[0]

    info = {}
    info['video_name'] = video_name
    info['width'] = width
    info['height'] = height
    info['fps'] = fps
    info['size'] = size
    info['fourcc'] = fourcc
    info['frame_number'] = length
    return info

def get_opt(netname, part, resolution, device='cuda', score_thre=None):
    assert part in ['human', 'garment', 'garment_whole']
    if netname == 'centernet':
        num_classes = 0
        if part == 'human':
            num_classes = 1
            model_path = 'weights/centernet_coco_person_{}.pth'.format(resolution)
            if score_thre is None:
                human_score_thres = {416:0.44, 800:0, 1216:0}
                score_thre = human_score_thres[resolution]
                score_thre = 0.44
            else:
                score_thre = score_thre
        elif part == 'garment':
            num_classes = 13
            model_path = 'weights/centernet_modanet_{}.pth'.format(resolution)
            if score_thre is None:
                score_thre = 0.51
        elif part == 'garment_whole':
            num_classes = 13
            model_path = 'weights/centernet_modanet_{}.pth'.format(resolution)
            if score_thre is None:
                score_thre = 0.24
        opt =  CenterNetOpts().parse()
        opt = CenterNetOpts().set_heads(opt, num_classes)
        opt.model_path = model_path
        opt.score_thre = score_thre
    elif netname == 'frcnn':
        opt = config( 'frcnn_modanet', 'test' )
        if part == 'human':
            opt.num_classes = 2
            opt.model_path = 'weights/frcnn_coco_person_{}.pkl'.format(resolution)
            if score_thre is None:
                human_score_thres = {416:0.89, 800:0, 1216:0}
                score_thre = human_score_thres[resolution]
                score_thre = 0.89
        elif part == 'garment':
            opt.num_classes = 14
            opt.model_path = 'weights/frcnn_modanet_{}.pkl'.format(resolution)
            if score_thre is None:
                score_thre = 0.54
        elif part == 'garment_whole':
            opt.num_classes = 14
            opt.model_path = 'weights/frcnn_modanet_{}.pkl'.format(resolution)
            if score_thre is None:
                score_thre = 0.39
        opt.dnn['roi_head']['score_thresh'] = score_thre
        opt.device = torch.device(device)
    elif netname == 'yolo_v3':
        opt = edict()
        if part =='human':
            opt.cfg = 'pytorch_yolov3/config/yolov3-person1.cfg'
            opt.model_path = 'weights/yolov3_coco_person_{}.pth'.format(resolution)
            if score_thre is None:
                human_score_thres = {416:0.71, 800:0, 1216:0}
                score_thre = human_score_thres[resolution]
                score_thre = 0.71
            else:
                score_thre = score_thre
        elif part == 'garment':
            opt.cfg = 'pytorch_yolov3/config/yolov3-modanet.cfg'
            opt.model_path = 'weights/yolov3_modanet_{}.pth'.format(resolution)
            if score_thre is None:
                score_thre = 0.87
        elif part == 'garment_whole':
            opt.cfg = 'pytorch_yolov3/config/yolov3-modanet.cfg'
            opt.model_path = 'weights/yolov3_modanet_{}.pth'.format(resolution)
            if score_thre is None:
                score_thre = 0.59
        opt.device = torch.device(device)
        opt.score_thre = score_thre
    elif netname == 'ssd':
        if part =='human':
            opt = ssd_config['coco_person_{}'.format(resolution)]
            opt['model_path']= 'weights/ssd_coco_person_{}.pth'.format(resolution)
            if score_thre is None:
                human_score_thres = {416:0.31, 800:0, 1216:0}
                score_thre = human_score_thres[resolution]
                score_thre = 0.31
            else:
                score_thre = score_thre
        elif part == 'garment':
            opt = ssd_config['modanet_{}'.format(resolution)]
            opt['model_path']= 'weights/ssd_modanet_{}.pth'.format(resolution)
            if score_thre is None:
                score_thre = 0.49
        elif part == 'garment_whole':
            opt = ssd_config['modanet_whole_{}'.format(resolution)]
            opt['model_path']= 'weights/ssd_modanet_{}.pth'.format(resolution)
            if score_thre is None:
                score_thre = 0.49
        opt['thre'] = score_thre
    return opt

def get_detector(netname, opt):
    if netname == 'centernet':
        model = CenterNetDetector(opt)
    elif netname == 'frcnn':
        model = FrcnnDetecter(opt)
        #model = torch.jit.script(model)
    elif netname == 'yolo_v3':
        model = YoloV3Detector(opt)
    elif netname == 'ssd':
        model = SSDDetector(opt)
    model.eval()
    return model

def get_models(human='centernet', human_res=416, garment='centernet', garment_res=256, device='cuda',human_score_thre=None,garment_score_thre=None):
    opt_human = get_opt(human, 'human', human_res, score_thre=human_score_thre)
    opt_garment = get_opt(garment, 'garment', garment_res, score_thre=garment_score_thre)
    human_model = get_detector(human, opt_human) 
    garment_model = get_detector(garment, opt_garment) 

    human_model.to(torch.device(device))
    garment_model.to(torch.device(device))
    return human_model, garment_model

def get_human_model(model_name, res, device, score_thre=None):
    opt = get_opt(model_name, 'human', res, score_thre=score_thre)
    human_model = get_detector(model_name, opt)

    human_model.to(torch.device(device))
    return human_model


def get_garment_model(garment='centernet', garment_res=256, device='cuda',garment_score_thre=None, use_whole_image_detector=False):
    if use_whole_image_detector:
        opt_garment = get_opt(garment, 'garment_whole', garment_res, score_thre=garment_score_thre)
    else:
        opt_garment = get_opt(garment, 'garment', garment_res, score_thre=garment_score_thre)
    garment_model = get_detector(garment, opt_garment) 

    garment_model.to(torch.device(device))
    return garment_model

def get_frcnn_cfg():
    #params = {}
    #params['config'] = 'frcnn_modanet'
    cfg = config( 'frcnn_modanet', 'test' )
    cfg.num_classes = 2
    cfg.model_path = 'weights/frcnn_coco_person_416.pkl'
    cfg.device = torch.device('cpu')
    return cfg

def get_frcnn():
    params = {}
    params['config'] = 'frcnn_modanet'
    cfg = config( params['config'], 'test' )
    cfg.num_classes = 2
    cfg.model_path = 'weights/frcnn_coco_person_416.pkl'
    cfg.device = torch.device('cpu')
    human_model = FrcnnDetecter(cfg)
    cfg.num_classes = 14
    cfg.model_path = 'weights/frcnn_modanet_128.pkl'
    garment_model = FrcnnDetecter(cfg)
    return human_model


def get_centernet():
    num_classes = 1
    opt = CenterNetOpts().parse()
    opt = CenterNetOpts().set_heads(opt, num_classes)
    opt.model_path = 'weights/centernet_coco_person_defalut.pth'
    opt.score_thre = 0.5
    human_model = CenterNetDetector(opt)
    human_model.eval()

    num_classes = 13
    opt1 = CenterNetOpts().parse()
    opt1 = CenterNetOpts().set_heads(opt1, num_classes)
    opt1.model_path = 'weights/centernet_modanet_128.pth'
    opt1.score_thre = 0.3
    garment_model = CenterNetDetector(opt1)
    garment_model.eval()
    return human_model, garment_model

def crop_boxes(im, boxes):
    crops = []
    for box in boxes:
        x1, y1, x2, y2 = box.astype(int)
        crop = im[y1:y2, x1:x2]
        crops.append(crop)
        #cv2.imshow('test', crop)
        #cv2.waitKey(1000)
    return crops

def recover_boxes(box, padding, crop):
    if len(box) == 0:
        return box
    #boxes_new = []
    #for box, padding, crop in zip(boxes, paddings, crops):
    #h_scale, w_scale = scale
    #print(box)
    #box[:,0] *= w_scale
    #box[:,2] *= w_scale
    #box[:,1] *= h_scale
    #box[:,3] *= h_scale
    #left, right, top, down = padding
    top, down, left, right = padding
    if left != 0 or top != 0:
        box[:,0] -= left
        box[:,2] -= left
        box[:,1] -= top
        box[:,3] -= top
    
    x1, y1, x2, y2 = crop
    box[:,0] += x1
    box[:,2] += x1
    box[:,1] += y1
    box[:,3] += y1
    return box

def set_device(inputs, device):
    inputs['data'] = inputs['data'].to(device)
    #if 'height' in inputs:
    #    inputs['height'] = inputs['height'].to(device)
    #if 'width' in inputs:
    #    inputs['width'] = inputs['width'].to(device)

    return inputs

def load_classification_model(device, detector='centernet', resolution='416'):
    model_path = os.path.expanduser('~/Vision/data/embedding/classification/checkpoints/checkpoints_{}_{}_10.pkl'.format(detector, resolution))
    checkpoint = torch.load(model_path)

    backbone = resnet50()
    model = ClassificationNet(backbone, input_size=5, class_num=2)
    model.load_state_dict(checkpoint['model_state_dict'])
    device = torch.device(device)
    model.to(device)
    model.eval()
    return model

def visulize_garment_track_in_shot(tracker, images_in_shot, shot_id):
    colors = random_colors(10, GBR=True)
    i = -1
    for track in reversed(tracker.tracks):
        i += 1
        color = colors[i%10]
        if track.shot_id == shot_id:
            node = track.head
            track_id = track.track_id
            while node is not None:
                frame_num = node.frame_num
                box = node.box
                im = images_in_shot[frame_num]
                cv2.rectangle(im, tuple(box[:2]), tuple(box[2:]),color, lineType=4)
                #model_score = track.model_score
                #cv2.rectangle(im, tuple(box[:2]), tuple(box[2:]),(255,0,0))
                #pos = (int(box[0]), int(box[1]+20))
                #cv_put_text(im, 'ID: '+str(track_id), pos, cv2.FONT_HERSHEY_COMPLEX, 1, (255, 0, 255), 2 )
                #pos = (int(box[0]), int(box[1]+40))
                #cv_put_text(im, 'score: {:.3f}'.format(model_score), pos, cv2.FONT_HERSHEY_COMPLEX, 1, (255, 0, 255), 2 )
                #pos = (int(box[0]), int(box[1]+60))
                #cv_put_text(im, 'cla_score: {:.3f}'.format(track.cla_score), pos, cv2.FONT_HERSHEY_COMPLEX, 1, (255, 0, 255), 2 )
                #pos = (int(box[0]), int(box[1]+80))
                #cv_put_text(im, 'center_score: {:.3f}'.format(track.center_score), pos, cv2.FONT_HERSHEY_COMPLEX, 1, (255, 0, 255), 2 )
                #pos = (int(box[0]), int(box[1]+100))
                #cv_put_text(im, 'len_score: {:.3f}'.format(track.len_score), pos, cv2.FONT_HERSHEY_COMPLEX, 1, (255, 0, 255), 2 )

                #if model_score < 0.1:
                #    cv2.rectangle(im, tuple(box[:2]), tuple(box[2:]),(0,0,255), lineType=4)
                #    cv_put_text_with_box(im, 'audience', tuple(box[:2].astype(int)), cv2.FONT_HERSHEY_COMPLEX, 1, font_color=(0, 0, 0), box_color=(0,0,255),thickness=2)
                #else:
                #    if node.interpolate:
                #        cv2.rectangle(im, tuple(box[:2]), tuple(box[2:]),(55,0,0), lineType=4)
                #    else:
                #        cv2.rectangle(im, tuple(box[:2]), tuple(box[2:]),(0,255,0), lineType=4)

                #    #cv2.rectangle(im, tuple(box[:2]), tuple(box[2:]),(0,255,0), lineType=4)
                #    cv_put_text_with_box(im, 'model', tuple(box[:2].astype(int)), cv2.FONT_HERSHEY_COMPLEX, 1, font_color=(0, 0, 0), box_color=(0,255,0),thickness=2)

                node = node.child
        else:
            break
def visulize_track_in_shot(tracker, images_in_shot, shot_id):
    for track in reversed(tracker.tracks):
        if track.shot_id == shot_id:
            node = track.head
            track_id = track.track_id
            while node is not None:
                frame_num = node.frame_num
                box = node.box
                im = images_in_shot[frame_num]
                model_score = track.model_score
                #cv2.rectangle(im, tuple(box[:2]), tuple(box[2:]),(255,0,0))
                pos = (int(box[0]), int(box[1]+20))
                cv_put_text(im, 'ID: '+str(track_id), pos, cv2.FONT_HERSHEY_COMPLEX, 1, (255, 0, 255), 2 )
                pos = (int(box[0]), int(box[1]+40))
                cv_put_text(im, 'score: {:.3f}'.format(model_score), pos, cv2.FONT_HERSHEY_COMPLEX, 1, (255, 0, 255), 2 )
                pos = (int(box[0]), int(box[1]+60))
                cv_put_text(im, 'cla_score: {:.3f}'.format(track.cla_score), pos, cv2.FONT_HERSHEY_COMPLEX, 1, (255, 0, 255), 2 )
                pos = (int(box[0]), int(box[1]+80))
                cv_put_text(im, 'center_score: {:.3f}'.format(track.center_score), pos, cv2.FONT_HERSHEY_COMPLEX, 1, (255, 0, 255), 2 )
                pos = (int(box[0]), int(box[1]+100))
                cv_put_text(im, 'len_score: {:.3f}'.format(track.len_score), pos, cv2.FONT_HERSHEY_COMPLEX, 1, (255, 0, 255), 2 )

                if model_score < 0.1:
                    cv2.rectangle(im, tuple(box[:2]), tuple(box[2:]),(0,0,255), lineType=4)
                    cv_put_text_with_box(im, 'audience', tuple(box[:2].astype(int)), cv2.FONT_HERSHEY_COMPLEX, 1, font_color=(0, 0, 0), box_color=(0,0,255),thickness=2)
                else:
                    if node.interpolate:
                        cv2.rectangle(im, tuple(box[:2]), tuple(box[2:]),(55,0,0), lineType=4)
                    else:
                        cv2.rectangle(im, tuple(box[:2]), tuple(box[2:]),(0,255,0), lineType=4)

                    #cv2.rectangle(im, tuple(box[:2]), tuple(box[2:]),(0,255,0), lineType=4)
                    cv_put_text_with_box(im, 'model', tuple(box[:2].astype(int)), cv2.FONT_HERSHEY_COMPLEX, 1, font_color=(0, 0, 0), box_color=(0,255,0),thickness=2)

                node = node.child
        else:
            break

def smooth_shots(shots, score_thre=0.1, frame_thre=10):
    shots = np.array(shots)
    for i in range(len(shots)):
        if shots[i] > score_thre:
            ind_min = max(0, i-frame_thre)
            ind_max = min(len(shots), i+frame_thre)
            if shots[i] != shots[ind_min:ind_max].max():
                shots[i] = 0
    #shots[-1] = 1
    return shots
            


def run_detect(dataset, human_model, garment_model, garment_model_name, video_info, shot_prediction, classification_model=None, garment_res=256, device='cuda' ):
    #data_it = iter(dataloader)
    #inputs = next(data_it)
    fourcc = 'mp4v'
    print(video_info)
    out = cv2.VideoWriter(video_info['out_path'], cv2.VideoWriter_fourcc(*fourcc), video_info['fps'], video_info['size'])
    #out = cv2.VideoWriter(video_info['out_path'],cv2.VideoWriter_fourcc(*fourcc), frame_rate=video_info['fps'], video_info['size'])
    device = torch.device(device)
    size = (garment_res // 2, garment_res)
    tracker = Tracker(IoU_thre=0.5, min_valid_length=3, pic_width=video_info['width'], pic_height=video_info['height'])
    patch_transform = PatchTransform((size))
    if garment_model_name == 'centernet':
        human_transform = CenterNetTrasform()
    elif garment_model_name == 'frcnn':
        human_transform = FrcnnTransform()
    elif garment_model_name == 'yolo_v3':
        human_transform = YoloV3Transform()
    elif garment_model_name == 'ssd':
        human_transform = SSDTransform()
    transforms_test = Compose([ResizeAndPadding(160), ToTensor(), Normalize()])
    track_nums = {}
    track_num_pre = 0
    shot_id = 1
    images_in_shot = {}
    #for inputs in dataset:
    for inputs, shot in zip(dataset, shot_prediction):
        if shot > 0.1:
            tracker.stash()
            visulize_track_in_shot(tracker, images_in_shot, shot_id)

            for temp_frame_num, temp_image in images_in_shot.items():
                out.write(temp_image)

            track_num = len(tracker.tracks) - track_num_pre
            track_num_pre = len(tracker.tracks)
            track_nums[shot_id] = track_num
            images_in_shot = {}
            # only produce first 5 video clips
            if shot_id == 5:
                break
            shot_id += 1

        # human detection
        inputs = set_device(inputs, device)
        human_batch = human_model(inputs)
        if torch.is_tensor(inputs['ori_im']):
            im = inputs['ori_im'].detach().cpu().numpy()
        else:
            im = inputs['ori_im']
        for ind, human_out in enumerate(human_batch):
            # human_out is the human boxes in each frame
            boxes = human_out['boxes']
            scores = human_out['scores']
            labels = human_out['labels']
            im = im[ind]
            images_in_shot[tracker._cur_frame+1] = im

            # if there is no human detection
            if len(boxes) == 0:
                #cv2.putText(im, 'ID: '+str(shot_id), (0, 0), cv2.FONT_HERSHEY_COMPLEX, 1, (255, 255, 0), 2, bottomLeftOrigin=False)
                tracker.track_next(boxes)
                cv_put_text_with_box(im, 'Shot'+str(shot_id), (0,0), cv2.FONT_HERSHEY_COMPLEX, 2, font_color=(0, 0, 0), box_color=(255,255,255),thickness=2)
                #out.write(im)
                continue

            if classification_model is not None:
                patch_dataset = PatchDataset(im, boxes, transforms=transforms_test, single_mode=True)
                dataloader = torch.utils.data.DataLoader(patch_dataset, batch_size=128, num_workers=0, shuffle=False, drop_last=False)
                for inputs_patch, _ in dataloader:
                    inputs_patch = set_device(inputs_patch, device)
                    output = classification_model(inputs_patch)
                    labels = output['pred'].detach().cpu().numpy()

                classified_boxes = boxes[np.where(labels==1)]
                omitted_boxes = boxes[np.where(labels==0)]

                #visuliza the result of classification model
                for cla_box in omitted_boxes:
                    cv2.rectangle(im, tuple(cla_box[:2]), tuple(cla_box[2:]),(0,0,255), lineType=4)
                    cv_put_text_with_box(im, 'audience', tuple(cla_box[:2].astype(int)), cv2.FONT_HERSHEY_COMPLEX, 1, font_color=(0, 0, 0), box_color=(0,0,255),thickness=2)
                for cla_box in classified_boxes:
                    cv2.rectangle(im, tuple(cla_box[:2]), tuple(cla_box[2:]),(0,255,0), lineType=4)
                    cv_put_text_with_box(im, 'model', tuple(cla_box[:2].astype(int)), cv2.FONT_HERSHEY_COMPLEX, 1, font_color=(0, 0, 0), box_color=(0,255,0),thickness=2)


            # perform tracking here
            tracker.track_next(boxes)
            valid_boxes, box_ids = tracker.get_current_boxes()
            # if there is no valid tracking boxes
            if len(valid_boxes) == 0:
                cv_put_text_with_box(im, 'Shot'+str(shot_id), (0,0), cv2.FONT_HERSHEY_COMPLEX, 2, font_color=(0, 0, 0), box_color=(255,255,255),thickness=2)
                #out.write(im)
                continue

            crops, extended_boxes, paddings, scales, keep_inds, deprecated_boxes = patch_transform(im, valid_boxes )
            human_dataset = BatchDataset(crops, extended_boxes, scales, paddings, transforms=human_transform)
            human_loader = torch.utils.data.DataLoader(human_dataset, batch_size=10, collate_fn=collate_fn, drop_last=False)
            for human_inputs in human_loader:
                human_inputs = set_device(human_inputs, device)
                garment_batch = garment_model(human_inputs)
                for garment_out, padding, crop_box in zip(garment_batch, human_inputs['paddings'], human_inputs['crop_box']):
                    garment_boxes, garment_scores, garment_labels = garment_out['boxes'], garment_out['scores'], garment_out['labels']

                    #garment_boxes, garment_scores, garment_labels = garment_model.post_process(garment_out, human_inputs)
                    #print(garment_boxes)
                    garment_boxes = recover_boxes(garment_boxes, padding, crop_box)
                    for box in garment_boxes:
                        cv2.rectangle(im, tuple(box[:2]), tuple(box[2:]),(255,255,0))
                        #cv2.rectangle(crops[i], tuple(box[:2]), tuple(box[2:]),(0,0,255))
                    #cv2.imshow('test', crops[i])

            box_ids = np.array(box_ids)[keep_inds]

            #for box, boxe_id in zip(extended_boxes, box_ids):
            #    cv2.rectangle(im, tuple(box[:2]), tuple(box[2:]),(255,0,0))
            #    cv_put_text(im, 'ID: '+str(boxe_id), tuple(box[:2].astype(int)), cv2.FONT_HERSHEY_COMPLEX, 1, (255, 0, 255), 2 )

            #for box in deprecated_boxes:
            #    #cv2.rectangle(im, tuple(box[:2]), tuple(box[2:]),(0,0,255))
            #    pos = (int(box[0]), int(box[1]+20))
            #    cv_put_text(im, 'deprecated', pos, cv2.FONT_HERSHEY_COMPLEX, 1, (255, 0, 255), 2 )

            #cv2.putText(im, 'ID: '+str(shot_id), (0, 0), cv2.FONT_HERSHEY_COMPLEX, 1, (255, 255, 0), 2, bottomLeftOrigin=False)
            cv_put_text_with_box(im, 'Shot'+str(shot_id), (0,0), cv2.FONT_HERSHEY_COMPLEX, 2, font_color=(0, 0, 0), box_color=(255,255,255),thickness=2)

            #out.write(im)
            #cv2.imshow('test', im)
            #cv2.waitKey(200)
    out.release()
    print('track numbers for the setting is: {}'.format(track_nums))

def run_detect_on_shot(dataset, human_model, garment_model, garment_model_name, video_info, shot_prediction, classification_model=None, garment_res=256, device='cuda', valid_shots=None):
    #data_it = iter(dataloader)
    #inputs = next(data_it)
    fourcc = 'mp4v'
    print(video_info)
    #out = cv2.VideoWriter(video_info['out_path'], -1, video_info['fps'], video_info['size'])
    out = cv2.VideoWriter(video_info['out_path'], cv2.VideoWriter_fourcc(*fourcc), video_info['fps'], video_info['size'])
    #out = cv2.VideoWriter(video_info['out_path'],cv2.VideoWriter_fourcc(*fourcc), frame_rate=video_info['fps'], video_info['size'])
    device = torch.device(device)
    size = (garment_res // 2, garment_res)
    tracker = Tracker(IoU_thre=0.5, min_valid_length=3, pic_width=video_info['width'], pic_height=video_info['height'])
    patch_transform = PatchTransform((size), filter_ratio=None, extend_ratio=0.2)
    if garment_model_name == 'centernet':
        human_transform = CenterNetTrasform()
    elif garment_model_name == 'frcnn':
        human_transform = FrcnnTransform()
    elif garment_model_name == 'yolo_v3':
        human_transform = YoloV3Transform()
    elif garment_model_name == 'ssd':
        human_transform = SSDTransform()
    transforms_test = Compose([ResizeAndPadding(160), ToTensor(), Normalize()])
    track_nums = {}
    track_num_pre = 0
    shot_id = 0
    images_in_shot = {}
    frame_count = 0
    shot_start_frame = 1
    shot_end_frame = 0
    new_shot = False
    missing_all = {}
    total = {}
    detection_nums = {}

    shots = smooth_shots(shot_prediction, score_thre=0.1, frame_thre=10)

    if valid_shots is not None:
        last_shot = np.array(valid_shots).max()

    # do human detection and tracking
    for inputs in dataset:
        
        inputs = set_device(inputs, device)
        #print(inputs['image_sizes'])
        human_batch = human_model(inputs)
        if torch.is_tensor(inputs['ori_im']):
            im = inputs['ori_im'].detach().cpu().numpy()
        else:
            im = inputs['ori_im']
        for ind, human_out in enumerate(human_batch):
            # human_out is the human boxes in each frame
            boxes = human_out['boxes']
            scores = human_out['scores']
            labels = human_out['labels']
            im = im[ind]
            frame_count += 1
            images_in_shot[tracker._cur_frame+1] = im
            tracker.track_next(boxes)

            if frame_count < len(shots):
                shot = shots[frame_count-1]
            else:
                shot = shots[-1]

            # if the shot is finished, start the detection for the shot
            if shot > 0.1 or frame_count==len(shots):
                shot_id += 1
                new_shot = True
                tracker.cur_shot_len = frame_count - shot_start_frame + 1
                tracker.stash()
                shot_end_frame = frame_count

            if valid_shots is not None:
                if shot_id not in valid_shots:
                    if shot_id+1 not in valid_shots:
                        images_in_shot = {}
                    shot_start_frame = shot_end_frame + 1
                    continue

            if new_shot:
                new_shot = False
                # do detection for the previous shot
                shot_boxes = tracker.get_shot_boxes(shot_id)
                print('shot {}, start frame: {}, end frame: {}'.format(shot_id, shot_start_frame, shot_end_frame))

                for frame in range(shot_start_frame, shot_end_frame+1):
                    if frame not in shot_boxes:
                        #cv_put_text_with_box(im, 'Shot'+str(shot_id), (0,0), cv2.FONT_HERSHEY_COMPLEX, 2, font_color=(0, 0, 0), box_color=(255,255,255),thickness=2)
                        continue
                    boxes = shot_boxes[frame]['boxes']
                    nodes = shot_boxes[frame]['nodes']
                    im = images_in_shot[frame]
                    if classification_model is not None:
                        patch_dataset = PatchDataset(im, boxes, transforms=transforms_test, single_mode=True)
                        dataloader = torch.utils.data.DataLoader(patch_dataset, batch_size=128, num_workers=0, shuffle=False, drop_last=False)
                        for inputs_patch, _ in dataloader:
                            inputs_patch = set_device(inputs_patch, device)
                            output = classification_model(inputs_patch)
                            labels = output['pred'].detach().cpu().numpy()

                        #classified_boxes = boxes[np.where(labels==1)]
                        #omitted_boxes = boxes[np.where(labels==0)]
                        for i, node in enumerate(nodes):
                            #print('nodes has classification score', node)
                            if labels[i] == 1:
                                node.classification_score = 1
                            else:
                                node.classification_score = 0
                
                tracker.cal_track_model_score(shot_id)

                detection_num, detection_total = tracker.get_shot_max_people_detection(shot_id, model_score_threshold=0.1)
                detection_nums[shot_id] = detection_num
                total[shot_id] = tracker.cur_shot_len

                # get model boxes and do garments detection on them
                shot_model_boxes = tracker.get_shot_model_boxes(shot_id, model_score_threshold=0.1)
                for frame in range(shot_start_frame, shot_end_frame+1):
                    if frame not in shot_model_boxes:
                        #cv_put_text_with_box(im, 'Shot'+str(shot_id), (0,0), cv2.FONT_HERSHEY_COMPLEX, 2, font_color=(0, 0, 0), box_color=(255,255,255),thickness=2)
                        continue
                    boxes = shot_model_boxes[frame]['boxes']
                    nodes = shot_model_boxes[frame]['nodes']
                    im = images_in_shot[frame]
                    crops, extended_boxes, paddings, scales, keep_inds, deprecated_boxes = patch_transform(im, boxes )
                    human_dataset = BatchDataset(crops, extended_boxes, scales, paddings, transforms=human_transform)
                    human_loader = torch.utils.data.DataLoader(human_dataset, batch_size=10, collate_fn=collate_fn, drop_last=False)
                    for human_inputs in human_loader:
                        human_inputs = set_device(human_inputs, device)
                        garment_batch = garment_model(human_inputs)
                        for garment_out, padding, crop_box in zip(garment_batch, human_inputs['paddings'], human_inputs['crop_box']):
                            garment_boxes, garment_scores, garment_labels = garment_out['boxes'], garment_out['scores'], garment_out['labels']

                            #garment_boxes, garment_scores, garment_labels = garment_model.post_process(garment_out, human_inputs)
                            #print(garment_boxes)
                            garment_boxes = recover_boxes(garment_boxes, padding, crop_box)
                            for box in garment_boxes:
                                cv2.rectangle(im, tuple(box[:2]), tuple(box[2:]),(255,255,0))
                                #cv2.rectangle(crops[i], tuple(box[:2]), tuple(box[2:]),(0,0,255))
                            #cv2.imshow('test', crops[i])

                visulize_track_in_shot(tracker, images_in_shot, shot_id)

                #for temp_frame_num, temp_image in images_in_shot.items():
                for frame in range(shot_start_frame, shot_end_frame+1):
                    temp_image = images_in_shot[frame]
                    cv_put_text_with_box(temp_image, 'Shot'+str(shot_id), (0,0), cv2.FONT_HERSHEY_COMPLEX, 2, font_color=(0, 0, 0), box_color=(255,255,255),thickness=2)
                    out.write(temp_image)

                track_num = len(tracker.tracks) - track_num_pre
                track_num_pre = len(tracker.tracks)
                track_nums[shot_id] = track_num
                images_in_shot = {}
                ## only produce first 5 video clips
                #if shot_id == 20:
                #    break
                #shot_id += 1
                shot_start_frame = shot_end_frame + 1
        if valid_shots is not None:
            if shot_id > last_shot:
                break
        #if shot_id == 20:
        #    break

    out.release()
    print('track numbers for the setting is: {}'.format(track_nums))
    print('total len for shots: {}'.format(total))
    print('human detections for shots: {}'.format(detection_nums))
    total_frame_num = 0
    total_detection = 0
    for shot_id, num in total.items():
        total_frame_num += num
        total_detection += detection_nums[shot_id]
        print('shot {}: detection rate is {}'.format(shot_id, detection_nums[shot_id]/num))
    detection_rate = total_detection / total_frame_num

    print('total frame number is {}, detection number is {}, detection rate is {}'.format(total_frame_num, total_detection, detection_rate))

def human_evaluation(dataset, human_model, garment_model, garment_model_name, video_info, shot_prediction, classification_model=None, garment_res=256, device='cuda', valid_shots=None ):
    #data_it = iter(dataloader)
    #inputs = next(data_it)
    fourcc = 'mp4v'
    print(video_info)
    #out = cv2.VideoWriter(video_info['out_path'], cv2.VideoWriter_fourcc(*fourcc), video_info['fps'], video_info['size'])
    #out = cv2.VideoWriter(video_info['out_path'],cv2.VideoWriter_fourcc(*fourcc), frame_rate=video_info['fps'], video_info['size'])
    device = torch.device(device)
    size = (garment_res // 2, garment_res)
    tracker = Tracker(IoU_thre=0.5, min_valid_length=3, pic_width=video_info['width'], pic_height=video_info['height'])
    patch_transform = PatchTransform((size), filter_ratio=None, extend_ratio=0.2)
    if garment_model_name == 'centernet':
        human_transform = CenterNetTrasform()
    elif garment_model_name == 'frcnn':
        human_transform = FrcnnTransform()
    elif garment_model_name == 'yolo_v3':
        human_transform = YoloV3Transform()
    elif garment_model_name == 'ssd':
        human_transform = SSDTransform()
    transforms_test = Compose([ResizeAndPadding(160), ToTensor(), Normalize()])
    track_nums = {}
    track_num_pre = 0
    shot_id = 0
    images_in_shot = {}
    frame_count = 0
    shot_start_frame = 1
    shot_end_frame = 0
    new_shot = False
    missing_all = {}
    total = {}
    detection_nums = {}
    detection_total_nums = {}

    shots = smooth_shots(shot_prediction, score_thre=0.1, frame_thre=10)

    if valid_shots is not None:
        last_shot = np.array(valid_shots).max()

    # do human detection and tracking
    for inputs in dataset:
        
        inputs = set_device(inputs, device)
        #print(inputs['image_sizes'])
        human_batch = human_model(inputs)
        if torch.is_tensor(inputs['ori_im']):
            im = inputs['ori_im'].detach().cpu().numpy()
        else:
            im = inputs['ori_im']
        for ind, human_out in enumerate(human_batch):
            # human_out is the human boxes in each frame
            boxes = human_out['boxes']
            scores = human_out['scores']
            labels = human_out['labels']
            if len(boxes) > 0:
                try:
                    assert (boxes[:,3] - boxes[:,1] >= 1).all()
                    assert (boxes[:,2] - boxes[:,0] >= 1).all()
                except AssertionError:
                    print(boxes[:,3] - boxes[:,1])
                    print(boxes[:,2] - boxes[:,0])
            
            im = im[ind]
            frame_count += 1
            images_in_shot[tracker._cur_frame+1] = im
            tracker.track_next(boxes)

            if frame_count < len(shots):
                shot = shots[frame_count-1]
            else:
                shot = shots[-1]

            # if the shot is finished, start the detection for the shot
            if shot > 0.1 or frame_count==len(shots):
                shot_id += 1
                new_shot = True
                tracker.cur_shot_len = frame_count - shot_start_frame + 1
                tracker.stash()
                shot_end_frame = frame_count

            if valid_shots is not None:
                if shot_id not in valid_shots:
                    if shot_id+1 not in valid_shots:
                        images_in_shot = {}
                    shot_start_frame = shot_end_frame + 1
                    continue

            if new_shot:
                new_shot = False
                # do detection for the previous shot
                shot_boxes = tracker.get_shot_boxes(shot_id)
                print('shot {}, start frame: {}, end frame: {}'.format(shot_id, shot_start_frame, shot_end_frame))

                for frame in range(shot_start_frame, shot_end_frame+1):
                    if frame not in shot_boxes:
                        #cv_put_text_with_box(im, 'Shot'+str(shot_id), (0,0), cv2.FONT_HERSHEY_COMPLEX, 2, font_color=(0, 0, 0), box_color=(255,255,255),thickness=2)
                        continue
                    boxes = shot_boxes[frame]['boxes']
                    nodes = shot_boxes[frame]['nodes']
                    im = images_in_shot[frame]
                    if classification_model is not None:
                        patch_dataset = PatchDataset(im, boxes, transforms=transforms_test, single_mode=True)
                        dataloader = torch.utils.data.DataLoader(patch_dataset, batch_size=64, num_workers=0, shuffle=False, drop_last=False)
                        labels = []
                        for inputs_patch, _ in dataloader:
                            inputs_patch = set_device(inputs_patch, device)
                            output = classification_model(inputs_patch)
                            labels.append(output['pred'].detach().cpu().numpy())
                        labels = np.hstack(labels)

                        #classified_boxes = boxes[np.where(labels==1)]
                        #omitted_boxes = boxes[np.where(labels==0)]
                        for i, node in enumerate(nodes):
                            #print('nodes has classification score', node)
                            if labels[i] == 1:
                                node.classification_score = 1
                            else:
                                node.classification_score = 0
                
                tracker.cal_track_model_score(shot_id)

                detection_num, detection_total = tracker.get_shot_max_people_detection(shot_id, model_score_threshold=0.1)
                detection_nums[shot_id] = detection_num
                detection_total_nums[shot_id] = detection_total
                total[shot_id] = tracker.cur_shot_len

                # get model boxes and do garments detection on them
                #shot_model_boxes = tracker.get_shot_model_boxes(shot_id, model_score_threshold=0.1)
                #for frame in range(shot_start_frame, shot_end_frame+1):
                #    if frame not in shot_model_boxes:
                #        #cv_put_text_with_box(im, 'Shot'+str(shot_id), (0,0), cv2.FONT_HERSHEY_COMPLEX, 2, font_color=(0, 0, 0), box_color=(255,255,255),thickness=2)
                #        continue
                #    boxes = shot_model_boxes[frame]['boxes']
                #    nodes = shot_model_boxes[frame]['nodes']
                #    im = images_in_shot[frame]
                #    crops, extended_boxes, paddings, scales, keep_inds, deprecated_boxes = patch_transform(im, boxes )
                #    human_dataset = BatchDataset(crops, extended_boxes, scales, paddings, transforms=human_transform)
                #    human_loader = torch.utils.data.DataLoader(human_dataset, batch_size=10, collate_fn=collate_fn, drop_last=False)
                #    for human_inputs in human_loader:
                #        human_inputs = set_device(human_inputs, device)
                #        garment_batch = garment_model(human_inputs)
                #        for garment_out, padding, crop_box in zip(garment_batch, human_inputs['paddings'], human_inputs['crop_box']):
                #            garment_boxes, garment_scores, garment_labels = garment_out['boxes'], garment_out['scores'], garment_out['labels']

                #            #garment_boxes, garment_scores, garment_labels = garment_model.post_process(garment_out, human_inputs)
                #            #print(garment_boxes)
                #            garment_boxes = recover_boxes(garment_boxes, padding, crop_box)
                #            for box in garment_boxes:
                #                cv2.rectangle(im, tuple(box[:2]), tuple(box[2:]),(255,255,0))
                #                #cv2.rectangle(crops[i], tuple(box[:2]), tuple(box[2:]),(0,0,255))
                #            #cv2.imshow('test', crops[i])

                #visulize_track_in_shot(tracker, images_in_shot, shot_id)

                ##for temp_frame_num, temp_image in images_in_shot.items():
                #for frame in range(shot_start_frame, shot_end_frame+1):
                #    temp_image = images_in_shot[frame]
                #    cv_put_text_with_box(temp_image, 'Shot'+str(shot_id), (0,0), cv2.FONT_HERSHEY_COMPLEX, 2, font_color=(0, 0, 0), box_color=(255,255,255),thickness=2)
                #    out.write(temp_image)

                track_num = len(tracker.tracks) - track_num_pre
                track_num_pre = len(tracker.tracks)
                track_nums[shot_id] = track_num
                images_in_shot = {}
                ## only produce first 5 video clips
                #if shot_id == 20:
                #    break
                #shot_id += 1
                shot_start_frame = shot_end_frame + 1
        if valid_shots is not None:
            if shot_id > last_shot:
                break
        #if shot_id == 20:
        #    break

    #out.release()
    print('track numbers for the setting is: {}'.format(track_nums))
    print('total len for shots: {}'.format(total))
    print('human detections for shots: {}'.format(detection_nums))
    total_frame_num = 0
    total_detection = 0
    human_detection_num = 0
    for shot_id, num in total.items():
        total_frame_num += num
        total_detection += detection_nums[shot_id]
        human_detection_num += detection_total_nums[shot_id]
        #print('shot {}: detection rate is {}'.format(shot_id, detection_nums[shot_id]/num))
    detection_rate = total_detection / total_frame_num
    model_rate = total_detection / human_detection_num
    detection_score = detection_rate * model_rate

    print('total frame number is {}, model detection number is {}, total human detection number is {}, detection rate is {}, detection score is {}'.format(total_frame_num, total_detection, human_detection_num, detection_rate, detection_score))
    
    #return boxes
    



def evaluation_garments_on_shot(dataset, human_model, garment_model, garment_model_name, video_info, shot_prediction, classification_model=None, garment_res=256, device='cuda', valid_shots=None):
    fourcc = 'mp4v'
    print(video_info)
    out = cv2.VideoWriter(video_info['out_path'], cv2.VideoWriter_fourcc(*fourcc), video_info['fps'], video_info['size'])
    device = torch.device(device)
    size = (garment_res // 2, garment_res)
    tracker = Tracker(IoU_thre=0.5, min_valid_length=3, pic_width=video_info['width'], pic_height=video_info['height'])
    garment_tracker = Tracker(IoU_thre=0.5, min_valid_length=3, pic_width=video_info['width'], pic_height=video_info['height'])
    patch_transform = PatchTransform((size), filter_ratio=None, extend_ratio=0.2)
    if garment_model_name == 'centernet':
        human_transform = CenterNetTrasform()
    elif garment_model_name == 'frcnn':
        human_transform = FrcnnTransform()
    elif garment_model_name == 'yolo_v3':
        human_transform = YoloV3Transform()
    elif garment_model_name == 'ssd':
        human_transform = SSDTransform()
    transforms_test = Compose([ResizeAndPadding(160), ToTensor(), Normalize()])
    track_nums = {}
    track_num_pre = 0
    shot_id = 0
    images_in_shot = {}
    frame_count = 0
    shot_start_frame = 1
    shot_end_frame = 0
    new_shot = False
    missing_all = {}
    total = {}
    detection_nums = {}
    detection_total_nums = {}

    shots = smooth_shots(shot_prediction, score_thre=0.1, frame_thre=10)

    if valid_shots is not None:
        last_shot = np.array(valid_shots).max()

    # do human detection and tracking
    for inputs in dataset:
        
        inputs = set_device(inputs, device)
        #print(inputs['image_sizes'])
        human_batch = human_model(inputs)
        if torch.is_tensor(inputs['ori_im']):
            im = inputs['ori_im'].detach().cpu().numpy()
        else:
            im = inputs['ori_im']
        for ind, human_out in enumerate(human_batch):
            # human_out is the human boxes in each frame
            boxes = human_out['boxes']
            scores = human_out['scores']
            labels = human_out['labels']
            im = im[ind]
            frame_count += 1
            images_in_shot[tracker._cur_frame+1] = im
            tracker.track_next(boxes)

            if frame_count < len(shots):
                shot = shots[frame_count-1]
            else:
                shot = shots[-1]

            # if the shot is finished, start the detection for the shot
            if shot > 0.1 or frame_count==len(shots):
                shot_id += 1
                new_shot = True
                tracker.cur_shot_len = frame_count - shot_start_frame + 1
                tracker.stash()
                shot_end_frame = frame_count

            if valid_shots is not None:
                if shot_id not in valid_shots:
                    if shot_id+1 not in valid_shots:
                        images_in_shot = {}
                    shot_start_frame = shot_end_frame + 1
                    continue

            if new_shot:
                new_shot = False
                # do detection for the previous shot
                shot_boxes = tracker.get_shot_boxes(shot_id)
                print('shot {}, start frame: {}, end frame: {}'.format(shot_id, shot_start_frame, shot_end_frame))

                for frame in range(shot_start_frame, shot_end_frame+1):
                    if frame not in shot_boxes:
                        #cv_put_text_with_box(im, 'Shot'+str(shot_id), (0,0), cv2.FONT_HERSHEY_COMPLEX, 2, font_color=(0, 0, 0), box_color=(255,255,255),thickness=2)
                        continue
                    boxes = shot_boxes[frame]['boxes']
                    nodes = shot_boxes[frame]['nodes']
                    im = images_in_shot[frame]
                    if classification_model is not None:
                        patch_dataset = PatchDataset(im, boxes, transforms=transforms_test, single_mode=True)
                        dataloader = torch.utils.data.DataLoader(patch_dataset, batch_size=128, num_workers=0, shuffle=False, drop_last=False)
                        labels = []
                        for inputs_patch, _ in dataloader:
                            inputs_patch = set_device(inputs_patch, device)
                            output = classification_model(inputs_patch)
                            #labels = output['pred'].detach().cpu().numpy()
                            labels.append(output['pred'].detach().cpu().numpy())
                        labels = np.hstack(labels)

                        #classified_boxes = boxes[np.where(labels==1)]
                        #omitted_boxes = boxes[np.where(labels==0)]
                        for i, node in enumerate(nodes):
                            #print('nodes has classification score', node)
                            if labels[i] == 1:
                                node.classification_score = 1
                            else:
                                node.classification_score = 0
                
                tracker.cal_track_model_score(shot_id)

                #detection_num = tracker.get_shot_max_detection(shot_id, model_score_threshold=0.1)
                #detection_nums[shot_id] = detection_num
                #total[shot_id] = tracker.cur_shot_len
                detection_num, detection_total = tracker.get_garment_detection_num(shot_id, model_score_threshold=0.1)
                detection_nums[shot_id] = detection_num
                detection_total_nums[shot_id] = detection_total
                #total[shot_id] = tracker.cur_shot_len

                # get model boxes and do garments detection on them
                shot_model_boxes = tracker.get_shot_model_boxes(shot_id, model_score_threshold=0.1)
                for frame in range(shot_start_frame, shot_end_frame+1):
                    if frame not in shot_model_boxes:
                        #cv_put_text_with_box(im, 'Shot'+str(shot_id), (0,0), cv2.FONT_HERSHEY_COMPLEX, 2, font_color=(0, 0, 0), box_color=(255,255,255),thickness=2)
                        continue
                    boxes = shot_model_boxes[frame]['boxes']
                    nodes = shot_model_boxes[frame]['nodes']
                    im = images_in_shot[frame]
                    crops, extended_boxes, paddings, scales, keep_inds, deprecated_boxes = patch_transform(im, boxes )
                    human_dataset = BatchDataset(crops, extended_boxes, scales, paddings, transforms=human_transform)
                    human_loader = torch.utils.data.DataLoader(human_dataset, batch_size=10, collate_fn=collate_fn, drop_last=False)
                    node_count = 0
                    for human_inputs in human_loader:
                        human_inputs = set_device(human_inputs, device)
                        garment_batch = garment_model(human_inputs)
                        for garment_out, padding, crop_box in zip(garment_batch, human_inputs['paddings'], human_inputs['crop_box']):
                            garment_boxes, garment_scores, garment_labels = garment_out['boxes'], garment_out['scores'], garment_out['labels']

                            #garment_boxes, garment_scores, garment_labels = garment_model.post_process(garment_out, human_inputs)
                            #print(garment_boxes)
                            garment_boxes = recover_boxes(garment_boxes, padding, crop_box)
                            nodes[node_count].garment_boxes = garment_boxes
                            nodes[node_count].garment_scores = garment_scores
                            nodes[node_count].garment_labels = garment_labels
                            node_count += 1
                    assert node_count == len(nodes)
                            #for box in garment_boxes:
                            #    cv2.rectangle(im, tuple(box[:2]), tuple(box[2:]),(255,255,0))
                                #cv2.rectangle(crops[i], tuple(box[:2]), tuple(box[2:]),(0,0,255))
                            #cv2.imshow('test', crops[i])

                # create tracker for each model tracklet and track the garments
                shot_tracks = tracker.get_shot_model_tracks(shot_id, model_score_threshold=0.1)
                if shot_tracks[0] is not None:
                    for track in shot_tracks:
                        shot_id = track.shot_id
                        garment_tracker.set_shot_num(shot_id)
                        garment_tracker.track_garments_in_track(track)

                visulize_track_in_shot(tracker, images_in_shot, shot_id)
                visulize_garment_track_in_shot(garment_tracker, images_in_shot, shot_id)

                #for temp_frame_num, temp_image in images_in_shot.items():
                for frame in range(shot_start_frame, shot_end_frame+1):
                    temp_image = images_in_shot[frame]
                    cv_put_text_with_box(temp_image, 'Shot'+str(shot_id), (0,0), cv2.FONT_HERSHEY_COMPLEX, 2, font_color=(0, 0, 0), box_color=(255,255,255),thickness=2)
                    out.write(temp_image)

                track_num = len(tracker.tracks) - track_num_pre
                track_num_pre = len(tracker.tracks)
                track_nums[shot_id] = track_num
                images_in_shot = {}
                ## only produce first 5 video clips
                #if shot_id == 20:
                #    break
                #shot_id += 1
                shot_start_frame = shot_end_frame + 1
        if valid_shots is not None:
            if shot_id > last_shot:
                break
        #if shot_id == 20:
        #    break

    out.release()
    print('track numbers for the setting is: {}'.format(track_nums))
    print('total len for shots: {}'.format(total))
    print('human detections for shots: {}'.format(detection_nums))
    total_garments_in_tracks = 0
    total_detection = 0
    for shot_id, num in detection_total_nums.items():
        total_garments_in_tracks += num
        total_detection += detection_nums[shot_id]
    detection_rate = total_detection / total_garments_in_tracks

    print('detected garments number is {}, all the garments in tracks is {}, detection rate is {}'.format( total_detection, total_garments_in_tracks, detection_rate))

def run_whole_image_detection(dataset, model, video_info, device='cuda' ):
    #data_it = iter(dataloader)
    #inputs = next(data_it)
    width = video_info['width']
    height = video_info['height']
    #out = cv2.VideoWriter(video_info['out_path'], cv2.VideoWriter_fourcc(*fourcc), video_info['fps'], video_info['size'])
    #out = cv2.VideoWriter(video_info['out_path'],cv2.VideoWriter_fourcc(*fourcc), frame_rate=video_info['fps'], video_info['size'])
    device = torch.device(device)
    frame_count = 0
    result = {}

    # do human detection and tracking
    for inputs in dataset:
        
        inputs = set_device(inputs, device)
        batch_out = model(inputs)
        for ind, single_out in enumerate(batch_out):
            if len(single_out['boxes'])>0:
                single_out['boxes'] = clip_boxes(single_out['boxes'], width, height)
            #boxes = human_out['boxes']
            #scores = human_out['scores']
            #labels = human_out['labels']

            frame_count += 1
            result[frame_count]= single_out
    
    return result

def save_garment_result(dataset, garment_model, detector_name, resolution, video_info, out_path, device='cuda' ):
    #data_it = iter(dataloader)
    #inputs = next(data_it)
    fourcc = 'mp4v'
    print(video_info)
    video_name = video_info['video_name']
    width = video_info['width']
    height = video_info['height']
    #out = cv2.VideoWriter(video_info['out_path'], cv2.VideoWriter_fourcc(*fourcc), video_info['fps'], video_info['size'])
    #out = cv2.VideoWriter(video_info['out_path'],cv2.VideoWriter_fourcc(*fourcc), frame_rate=video_info['fps'], video_info['size'])
    device = torch.device(device)
    frame_count = 0
    result = {}

    # do human detection and tracking
    for inputs in dataset:
        
        inputs = set_device(inputs, device)
        #print(inputs['image_sizes'])
        garment_batch = garment_model(inputs)
        for ind, garment_out in enumerate(garment_batch):
            # human_out is the human boxes in each frame
            garment_out['boxes'] = clip_boxes(garment_out['boxes'], width, height)
            #boxes = human_out['boxes']
            #scores = human_out['scores']
            #labels = human_out['labels']

            frame_count += 1
            result[frame_count]= garment_out
    
    with open(out_path,'wb') as f:
        pickle.dump(result, f)
    print('the garment detection result is saved to {}'.format(out_path))
    return result

def clip_boxes(boxes, width, height):
    boxes = np.array(boxes)
    boxes[:, 0] = np.clip(boxes[:,0], 0, width-1)
    boxes[:, 1] = np.clip(boxes[:,1], 0, height-1)
    boxes[:, 2] = np.clip(boxes[:,2], 0, width-1)
    boxes[:, 3] = np.clip(boxes[:,3], 0, height-1)
    return boxes



def save_human_result(dataset, human_model, detector_name, resolution, video_info, device='cuda' ):
    #data_it = iter(dataloader)
    #inputs = next(data_it)
    fourcc = 'mp4v'
    root = 'human_detection_result'
    print(video_info)
    video_name = video_info['video_name']
    #out = cv2.VideoWriter(video_info['out_path'], cv2.VideoWriter_fourcc(*fourcc), video_info['fps'], video_info['size'])
    #out = cv2.VideoWriter(video_info['out_path'],cv2.VideoWriter_fourcc(*fourcc), frame_rate=video_info['fps'], video_info['size'])
    device = torch.device(device)
    frame_count = 0
    result = {}

    # do human detection and tracking
    for inputs in dataset:
        
        inputs = set_device(inputs, device)
        #print(inputs['image_sizes'])
        human_batch = human_model(inputs)
        if torch.is_tensor(inputs['ori_im']):
            im = inputs['ori_im'].detach().cpu().numpy()
        else:
            im = inputs['ori_im']
        for ind, human_out in enumerate(human_batch):
            # human_out is the human boxes in each frame
            boxes = human_out['boxes']
            scores = human_out['scores']
            labels = human_out['labels']

            frame_count += 1
            result[frame_count]= human_out
    
    out_path = os.path.join(root, '{}_{}_{}.pkl'.format(video_name, detector_name, resolution ))
    with open(out_path,'wb') as f:
        pickle.dump(result, f)
    print('the human detection result is saved to {}'.format(out_path))

def merge_human_result(detections, weights=None):
    result_num = len(detections)
    i = 1
    final_result = {}
    while True:
        boxes = []
        scores = []
        labels = []
        if i not in detections[0]:
            break
        for ind in range(result_num):
            frame_boxes = detections[ind][i]['boxes']
            if len(frame_boxes) == 0:
                final_result[i] = None
                continue
            boxes.append(frame_boxes)
            scores.append(detections[ind][i]['scores'])
            labels.append(detections[ind][i]['labels'])
        #print('boxes', boxes)
        #print('scores', scores)
        #print('labels', labels)
        if len(boxes) > 0:
            boxes = np.concatenate(boxes, axis=0)
            scores = np.concatenate(scores)
            labels = np.concatenate(labels)
            boxes = torch.from_numpy(boxes)
            scores = torch.from_numpy(scores)
            keep = nms(boxes, scores, iou_threshold=0.5)
            keep = keep.numpy()
            boxes = boxes.numpy()[keep]
            scores = scores.numpy()[keep]
            labels = labels[keep]
            #print('boxes', boxes)
            #print('scores', scores)
            #print('labels', labels)
            frame_result = {'boxes':boxes, 'scores':scores, 'labels':labels}
            final_result[i] = frame_result
        if i not in final_result:
            final_result[i] = None
        i += 1
    return final_result
    
def merge_result(detections, class_num, weights=None):
    result_num = len(detections)
    final_result = {}
    #result_keys = []
    #for ind in range(result_num):
    #    result_keys.append(set(list(detections[ind].keys())))
    #for ind in range(result_num-1):
    #    print(result_keys[ind].difference(result_keys[ind+1]))
    #    print(result_keys[ind+1].difference(result_keys[ind]))
    #print(result_keys[1])
    for ind in range(result_num-1):
        assert set(list(detections[ind].keys())) == set(list(detections[ind+1].keys()))
        
    for i in detections[0]:
        boxes = []
        scores = []
        labels = []
        boxes_all = []
        scores_all = []
        labels_all = []
        for ind in range(result_num):
            frame_boxes = detections[ind][i]['boxes']
            if len(frame_boxes) == 0:
                final_result[i] = None
                continue
            boxes.append(frame_boxes)
            scores.append(detections[ind][i]['scores'])
            labels.append(detections[ind][i]['labels'])
        #print('boxes', boxes)
        #print('scores', scores)
        #print('labels', labels)
        if len(boxes) > 0:
            boxes = np.concatenate(boxes, axis=0)
            scores = np.concatenate(scores)
            labels = np.concatenate(labels)
            for cls in range(1, class_num+1):
                class_ind = np.where(labels==cls)
                if len(class_ind) == 0:
                    continue
                boxes_class = boxes[class_ind]
                labels_class = labels[class_ind]
                scores_class = scores[class_ind]
                boxes_class = torch.from_numpy(boxes_class)
                scores_class = torch.from_numpy(scores_class)
                keep = nms(boxes_class, scores_class, iou_threshold=0.5)
                keep = keep.numpy()
                boxes_class = boxes_class.numpy()[keep]
                scores_class = scores_class.numpy()[keep]
                labels_class = labels_class[keep]
                boxes_all.append(boxes_class)
                scores_all.append(scores_class)
                labels_all.append(labels_class)
            #print('boxes', boxes)
            #print('scores', scores)
            #print('labels', labels)
            boxes_all = np.concatenate(boxes_all, axis=0)
            scores_all = np.concatenate(scores_all, axis=0)
            labels_all = np.concatenate(labels_all, axis=0)
            frame_result = {'boxes':boxes_all, 'scores':scores_all, 'labels':labels_all}
            final_result[i] = frame_result
        if i not in final_result:
            final_result[i] = None
        i += 1
    return final_result

def get_garment_result(video_path, tracker, garment_model, garment_model_name, valid_track_ids, resolution, video_info, save_result=False, device='cuda', no_label=False ):
    #data_it = iter(dataloader)
    #inputs = next(data_it)
    #fourcc = 'mp4v'
    root = 'garment_detection_result'
    print(video_info)
    video_name = video_info['video_name']
    cap = cv2.VideoCapture(video_path)
    width = video_info['width']
    height = video_info['height']
    #out = cv2.VideoWriter(video_info['out_path'], cv2.VideoWriter_fourcc(*fourcc), video_info['fps'], video_info['size'])
    #out = cv2.VideoWriter(video_info['out_path'],cv2.VideoWriter_fourcc(*fourcc), frame_rate=video_info['fps'], video_info['size'])

    size = (resolution // 2, resolution)
    #tracker = Tracker(IoU_thre=0.5, min_valid_length=3, pic_width=video_info['width'], pic_height=video_info['height'])
    #garment_tracker = Tracker(IoU_thre=0.5, min_valid_length=3, pic_width=video_info['width'], pic_height=video_info['height'])
    patch_transform = PatchTransform((size), filter_ratio=None, extend_ratio=0.2)

    if garment_model_name == 'centernet':
        human_transform = CenterNetTrasform()
    elif garment_model_name == 'frcnn':
        human_transform = FrcnnTransform()
    elif garment_model_name == 'yolo_v3':
        human_transform = YoloV3Transform()
    elif garment_model_name == 'ssd':
        human_transform = SSDTransform()

    device = torch.device(device)
    frame_count = 0
    result = {}

    model_nodes, human_nodes = tracker.convert_tracks(one_model_per_shot=False, valid_track_ids=valid_track_ids)

        
    while True:
        ret, im = cap.read()
        if not ret:
            break

        frame_count += 1
        if frame_count not in model_nodes:
            #result[frame_count] = {'boxes':[], 'labels':[], 'scores':[]}
            continue
        boxes = [node.box for node in model_nodes[frame_count]]
        boxes = np.array(boxes)

        crops, extended_boxes, paddings, scales, keep_inds, deprecated_boxes = patch_transform(im, boxes )
        human_dataset = BatchDataset(crops, extended_boxes, scales, paddings, transforms=human_transform)
        human_loader = torch.utils.data.DataLoader(human_dataset, batch_size=10, collate_fn=collate_fn, drop_last=False)
        node_count = 0
        frame_result = {'boxes':[], 'labels':[], 'scores':[]}
        for human_inputs in human_loader:
            human_inputs = set_device(human_inputs, device)
            garment_batch = garment_model(human_inputs)
            for garment_out, padding, crop_box in zip(garment_batch, human_inputs['paddings'], human_inputs['crop_box']):
                garment_boxes, garment_scores, garment_labels = garment_out['boxes'], garment_out['scores'], garment_out['labels']

                # if there is no garment detected in the human box
                if len(garment_labels) == 0:
                    continue
                #garment_boxes, garment_scores, garment_labels = garment_model.post_process(garment_out, human_inputs)
                #print(garment_boxes)
                garment_boxes = recover_boxes(garment_boxes, padding, crop_box)
                garment_boxes = clip_boxes(garment_boxes, width, height)
                frame_result['boxes'].append(garment_boxes)
                frame_result['labels'].append(garment_labels)
                frame_result['scores'].append(garment_scores)
        for k, v in frame_result.items():
            if len(v) == 0:
                continue
            frame_result[k] = np.concatenate(v, 0)
        if no_label:
            frame_result['labels'] = np.ones_like(frame_result['labels'])
        result[frame_count] = frame_result
        if frame_count > 2400:
            break
        #if frame_count == 16:
        #    boxes = frame_result['boxes']
        #    for box in boxes:
        #        cv2.rectangle(im, tuple(box[:2]), tuple(box[2:]),(0,255,0), lineType=4)
        #        cv2.imwrite('test.jpg', im)
        #        
        #    break
        #print(result)
        #break
    cap.release()
    if save_result:
        out_path = os.path.join(root, '{}_{}_{}_refer_anno.pkl'.format(video_name, garment_model_name, resolution ))
        with open(out_path,'wb') as f:
            pickle.dump(result, f)
            print('the garment detection result is saved to {}'.format(out_path))
    return result

def convert_result_to_coco_style(results, out_json_path):
    json_result = []
    for frame in results:
        image_id = frame
        result = results[frame]
        if result is not None:
            scores = result['scores']
            labels = result['labels']
            boxes = result['boxes']
            for box, score, label in zip(boxes, scores, labels):
                box = to_xywh(box)
                single_result = {'image_id': image_id,
                                 'category_id': int(label),
                                 'bbox':box.tolist(),
                                 'score':float(score)}
                json_result.append(single_result)
    
    with open(out_json_path, 'w') as f:
        json.dump(json_result, f)

def benchmark_coco(gt_path, dt_path):
    with open(dt_path) as f:
        anns = json.load(f)
    annsImgIds = [ann['image_id'] for ann in anns]
    mycoco = coco.COCO(gt_path)
    gt_ids = mycoco.getImgIds()
    print('det num:',len(set(annsImgIds)))
    print('gt num:',len(set(gt_ids)))
    #print(set(annsImgIds).difference(set(gt_ids)))
    #print(set(gt_ids).difference(set(annsImgIds)))
    coco_dets = mycoco.loadRes(dt_path)
    coco_eval = COCOeval(mycoco, coco_dets, "bbox")
    coco_eval.evaluate()
    coco_eval.accumulate()
    coco_eval.summarize()

def to_xywh(box):
    box[2] = box[2]-box[0]
    box[3] = box[3]-box[1]
    return box

def boxes_to_xywh(boxes):
    boxes[:,2] = boxes[:,2]-boxes[:,0]
    boxes[:,3] = boxes[:,3]-boxes[:,1]
    return boxes

def convert_result_to_coco_gt(results):
    annoatations = []
    images = []
    obj_count = 1
    for frame in results:
        image_id = frame
        result = results[frame]
        image = {'id':frame}
        images.append(image)
        if result is not None:
            scores = result['scores']
            labels = result['labels']
            boxes = result['boxes']
            boxes = boxes_to_xywh(boxes)
            areas = boxes[:,2]*boxes[:,3]
            for box, score, label, area in zip(boxes, scores, labels, areas):
                single_result = {'image_id': image_id,
                                 'category_id': int(label),
                                 'bbox':box.tolist(),
                                 'area':float(area),
                                 'iscrowd':0,
                                 'id': obj_count,
                                 'score':float(score)}
                obj_count += 1
                annoatations.append(single_result)
    gt = {
        "info": {
            "description": "Customised Dataset",
            "url": "None",
            "version": "1.0",
            "year": 2020,
            "contributor": "someone",
            "date_created": "2020/xx/xx"
        },
        "licenses": [],
        "images": images,
        "categories": [
            {"supercategory": "garments","id": 1,"name": "bag"},
            {"supercategory": "garments","id": 2,"name": "belt"},
            {"supercategory": "garments","id": 3,"name": "boots"},
            {"supercategory": "garments","id": 4,"name": "footwear"},
            {"supercategory": "garments","id": 5,"name": "outer"},
            {"supercategory": "garments","id": 6,"name": "dress"},
            {"supercategory": "garments","id": 7,"name": "sunglasses"},
            {"supercategory": "garments","id": 8,"name": "pants"},
            {"supercategory": "garments","id": 9,"name": "top"},
            {"supercategory": "garments","id": 10,"name": "shorts"},
            {"supercategory": "garments","id": 11,"name": "skirt"},
            {"supercategory": "garments","id": 12,"name": "headwear"},
            {"supercategory": "garments","id": 13,"name": "scarf & tie"},
        ],
        "annotations": annoatations
    }
    return gt

def remove_unrelated_coco_result(gt_path, result):
    mycoco = coco.COCO(gt_path)
    gt_ids = mycoco.getImgIds()
    gt_ids = set(gt_ids)

    # remove the detections out of benchmark
    result_key = set(result.keys())
    for key in result_key:
        if key not in gt_ids:
            del result[key]

    # add fake detections to result
    for gt_id in gt_ids:
        if gt_id not in result:
            result[gt_id] = {'boxes':np.array([[0,0,0,0]]),
                             'labels':np.array([1]),
                             'scores':np.array([0.001])}
    return result
