python gen_groundtruth.py --detector yolo_v3 --resolution 800
python gen_groundtruth.py --detector centernet --resolution 800
python gen_groundtruth.py --detector frcnn --resolution 800
python gen_groundtruth.py --detector ssd --resolution 800
python gen_groundtruth.py --detector centernet --resolution 1216
python gen_groundtruth.py --detector frcnn --resolution 1216
