import numpy as np
import cv2
import torch
import math
from torchvision.transforms import ToTensor
from ssd_pytorch.data import base_transform

class SSDTransform():
    def __init__(self, size=None):
        self.size = size
        self.mean = (104, 117, 123)

    def __call__(self, inputs, targets=None):
        h, w, _ = inputs['data'].shape
        inputs['ori_image'] = inputs['data']
        inputs['height'] = h
        inputs['width'] = w
        if self.size is None:
            self.size = max(h, w)
        img = base_transform(inputs['data'], self.size, self.mean)
        inputs['data'] = torch.from_numpy(img).permute(2, 0, 1)

        if targets is None:
            return inputs
        else:
            return inputs, targets

def detection_collate(batch):
    """Custom collate fn for dealing with batches of images that have a different
    number of associated object annotations (bounding boxes).

    Arguments:
        batch: (tuple) A tuple of tensor images and lists of annotations

    Return:
        A tuple containing:
            1) (tensor) batch of images stacked on their 0 dim
            2) (list of tensors) annotations for a given image are stacked on
                                 0 dim
    """
    imgs = []
    inputs = {}
    ori_images = []
    ws = []
    hs = []
    im_ids = []
    scales = []
    crop_box = []

    for sample in batch:
        imgs.append(sample['data'])
        ws.append(sample['width'])
        hs.append(sample['height'])
        ori_images.append(sample['ori_image'])
        if 'image_id' in sample:
            im_ids.append(sample['image_id'])
        if 'scale' in sample:
            scales.append(sample['scale'])
        if 'crop_box' in sample:
            crop_box.append(sample['crop_box'])
    inputs['data'] = torch.stack(imgs, 0)
    inputs['width'] = np.array(ws)
    inputs['height'] = np.array(hs)
    inputs['ori_im'] = ori_images
    inputs['image_id'] = im_ids
    inputs['scale'] = scales
    inputs['crop_box'] = crop_box
    return inputs