import torch
import math
import numpy as np

class BatchDataset(torch.utils.data.Dataset):
    def __init__(self, crops, boxes, scales, paddings=None, transforms=None):
        self.crops = crops
        self.boxes = boxes
        self.scales = scales
        self.paddings = paddings
        self.sample_num = len(crops)
        self.transforms = transforms
        #self.length = math.ceil(sample_num / batch_size)
        #inds = list(range(sample_num))
        #self.batch_ind = [inds[i:min(i+batch_size, sample_num)] for i in range(0, sample_num, batch_size)]

    def __getitem__(self, ind):
        img = self.crops[ind]

        inputs = {}
        inputs['data'] = img
        h, w, _ = img.shape
        inputs['crop_box'] = self.boxes[ind]
        inputs['scale'] = self.scales[ind]
        inputs['paddings'] = self.paddings[ind]

        if self.transforms is not None:
            inputs = self.transforms(inputs)
        inputs['width'] = w*inputs['scale']
        inputs['height'] = h*inputs['scale']
        return inputs

    def __len__(self):
        return self.sample_num

def collate_fn(batch):
    imgs = []
    crop_boxes = []
    scales = []
    paddings = []
    image_sizes = []
    widths = []
    heights = []
    for sample in batch:
        imgs.append(sample['data'])
        crop_boxes.append(sample['crop_box'])
        scales.append(sample['scale'])
        paddings.append(sample['paddings'])
        _, height, width = sample['data'].shape
        image_sizes.append((height, width))
        if 'width' in sample:
            widths.append(sample['width'])
        if 'height' in sample:
            heights.append(sample['height'])
    inputs = {}
    inputs['data'] = torch.stack(imgs)
    inputs['scale'] = torch.from_numpy(np.array(scales))
    inputs['paddings'] = paddings
    inputs['crop_box'] = crop_boxes
    inputs['image_sizes'] = image_sizes
    inputs['width'] = widths
    inputs['height'] = heights
    return inputs
