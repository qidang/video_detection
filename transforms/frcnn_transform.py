import numpy as np
import cv2
import torch

class FrcnnTransform():
    def __init__(self, size=None):
        self.size = size
        mean=[0.406, 0.456, 0.485]
        std=[0.225, 0.224, 0.229]
        self.mean = np.array(mean,dtype=np.float32).reshape(1,1,3)
        self.std = np.array(std,dtype=np.float32).reshape(1,1,3)

    def __call__(self, inputs, targets=None):
        image = inputs['data']
        height, width, _ = image.shape
        if self.size is not None:
            scale = max(height, width) / self.size
            out_height, out_width = int(height / scale), int(width / scale)
            image = cv2.resize(image, (out_width, out_height))
            inputs['scale'] = 1/scale
        else:
            inputs['scale'] = 1/inputs['scale']

        inp = (image.astype(np.float32) / 255.)
        inp = (inp - self.mean) / self.std
        inp = inp.transpose(2, 0, 1)
        inputs['data'] = torch.from_numpy(inp)
        height, width, _ = image.shape
        inputs['image_sizes'] = np.array([height, width])
        if targets is None:
            return inputs
        else:
            return inputs, targets