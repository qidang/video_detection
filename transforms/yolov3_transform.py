import numpy as np
import cv2
import torch
import math
from torchvision.transforms import ToTensor

class YoloV3Transform():
    def __init__(self, size=None):
        self.size = size
        self.to_tenser = ToTensor()
        #mean=[0.406, 0.456, 0.485]
        #std=[0.225, 0.224, 0.229]
        #self.mean = np.array(mean,dtype=np.float32).reshape(1,1,3)
        #self.std = np.array(std,dtype=np.float32).reshape(1,1,3)

    def __call__(self, inputs, targets=None):
        image = cv2.cvtColor(inputs['data'], cv2.COLOR_BGR2RGB) 
        height, width, _ = image.shape
        if self.size is not None:
            scale = max(height, width) / self.size
            out_height, out_width = int(height / scale), int(width / scale)
            image = cv2.resize(image, (out_width, out_height))
            image, padding = pad(image, min_stride=32)
            inputs['scale'] = scale

        #inp = (image.astype(np.float32) / 255.)
        #inp = (inp - self.mean) / self.std
        #inp = inp.transpose(2, 0, 1)
        inputs['data'] = self.to_tenser(image)

        if targets is None:
            return inputs
        else:
            return inputs, targets

def pad(image, min_stride=32):
    height, width, _ = image.shape
    pad_y = min_stride * math.ceil(height/min_stride) - height
    pad_x = min_stride * math.ceil(width/min_stride) - width
    image = np.pad(image, ((0, pad_y),(0, pad_x), (0, 0)),mode='constant')
    padding = (0, 0, pad_x, pad_y)
    return image, padding
    


