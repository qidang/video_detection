import numpy as np
import cv2
import torch
import math

class CenterNetTrasform():
    def __init__(self, size=None, min_stride=32):
        if size is not None:
            self.size = math.ceil(size / min_stride) * min_stride
        else:
            self.size = size
        if self.size != size:
            print('The input min size has padded as {}'.format(self.size))
        self.min_stride = 32
        mean = [0.408, 0.447, 0.470] # may need to be changed
        std = [0.289, 0.274, 0.278] # may need to be changed

        self.mean = np.array(mean,dtype=np.float32).reshape(1,1,3)
        self.std = np.array(std,dtype=np.float32).reshape(1,1,3)

    def __call__(self, inputs, targets=None):
        image = inputs['data']
        height, width, _ = image.shape
        if self.size is not None:
            scale = max(height, width) / self.size
            out_height, out_width = int(height / scale), int(width / scale)
            image = cv2.resize(image, (out_width, out_height))
            image, padding = pad(image, min_stride=self.min_stride)
            inputs['scale'] = scale

        inp = (image.astype(np.float32) / 255.)
        inp = (inp - self.mean) / self.std
        inp = inp.transpose(2, 0, 1)
        inputs['data'] = torch.from_numpy(inp)
        if targets is None:
            return inputs
        else:
            return inputs, targets

def pad(image, min_stride=32):
    height, width, _ = image.shape
    pad_y = min_stride * math.ceil(height/min_stride) - height
    pad_x = min_stride * math.ceil(width/min_stride) - width
    image = np.pad(image, ((0, pad_y),(0, pad_x), (0, 0)),mode='constant')
    padding = (0, 0, pad_x, pad_y)
    return image, padding