import numpy as np

class Normalization():
    def __init__(self, mean, std):
        self.mean = mean
        self.std = std

    def __call__(self, inputs):
        im = inputs['data']
        im = im / 255.
        im = (im - self.mean) / self.std
        inputs['data'] = im
        return inputs