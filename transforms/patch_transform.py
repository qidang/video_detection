import numpy as np
import cv2

class PatchTransform():
    # size = (width, height)
    # do not perform ratio filter when filter_ratio is None
    def __init__(self, size, filter_ratio=None, extend_ratio=0.2):
        self.filter_ratio = filter_ratio
        self.extend_ratio = extend_ratio
        self.size = size
        self.resize_ratio = size[1] / size[0]

    def __call__(self, image, boxes):
        h, w, _ = image.shape
        boxes, keep_inds, deprecated_boxes = self.box_filter(boxes, ratio=self.filter_ratio)
        # output of box_extend is within the image
        boxes = self.box_extend(boxes, w, h, ratio=self.extend_ratio)
        boxes, padding = self.box_resize(boxes, w, h, ratio=self.resize_ratio)
        crops = self.crop_and_padding(image, boxes, padding)
        crops, scales = self.resize_crops(crops, self.size)
        #inputs = {}
        #inputs['data'] = crops
        return crops, boxes, padding, scales, keep_inds, deprecated_boxes

    def box_filter(self, boxes, ratio):
        if ratio is None:
            return boxes, np.array(range(len(boxes)), dtype=int), np.array([], dtype=int)
        inds = []
        deprecate_inds = []
        for i, box in enumerate(boxes):
            x1, y1, x2, y2 = box
            if (y2-y1) / (x2-x1) > ratio:
                inds.append(i)
            else:
                deprecate_inds.append(i)
        inds = np.array(inds, dtype=int)
        return boxes[inds], inds, boxes[deprecate_inds]

    def box_extend(self, boxes, width, height, ratio=0.2):
        for i, box in enumerate(boxes):
            x1, y1, x2, y2 = box
            box_w = x2-x1
            box_h = y2-y1
            cx = (x1+x2) / 2
            cy = (y1+y2) / 2
            box_w_new = box_w * (1+ratio)
            box_h_new = box_h * (1+ratio)

            x1 = max(0, cx-box_w_new/2)
            y1 = max(0, cy-box_h_new/2)
            x2 = min(width, cx+box_w_new/2)
            y2 = min(height, cy+box_h_new/2)
            boxes[i] = [x1, y1, x2, y2]
        return boxes

    def box_resize(self, boxes, width, height, ratio=2):
        padding = np.zeros_like(boxes, dtype=int)
        for i, box in enumerate(boxes):
            x1, y1, x2, y2 = box
            box_w = x2-x1
            box_h = y2-y1
            cx = (x1+x2) / 2
            cy = (y1+y2) / 2
            if box_h/box_w > ratio:
                box_w = box_h / ratio
            else:
                box_h = box_w * ratio
            x1 = int(max(0, cx-box_w/2))
            y1 = int(max(0, cy-box_h/2))
            x2 = int(min(width, cx+box_w/2))
            y2 = int(min(height, cy+box_h/2))
            pad_x1 = x1 - int(cx-box_w/2) 
            pad_y1 = y1 - int(cy-box_h/2) 
            pad_x2 = int(cx+box_w/2) - x2
            pad_y2 = int(cy+box_h/2) - y2
            boxes[i] = [x1, y1, x2, y2]
            #debug here
            #padding[i] = [pad_x1, pad_x2, pad_y1, pad_y2]
            padding[i] = [pad_y1, pad_y2, pad_x1, pad_x2]
        return boxes, padding

    def crop_and_padding(self, im, boxes, paddings):
        crops = []
        temp = np.zeros(4)
        for box, padding in zip(boxes, paddings):
            x1, y1, x2, y2 = box.astype(int)
            crop = im[y1:y2, x1:x2]
            if (padding - temp).sum() > 0:
                crop = np.pad(crop, ((padding[0],padding[1]),(padding[2], padding[3]), (0, 0)), 'constant')
            crops.append(crop)
        return crops

    def resize_crops(self, crops, size):
        # size = (width, height)
        scales = []
        for i, crop in enumerate(crops):
            h, w, c = crop.shape
            #print(crop.shape)
            h_scale = h / size[1]
            w_scale = w / size[0]
            scales.append((h_scale + w_scale)/2)
            crops[i] = cv2.resize(crop, size)
            #cv2.imshow('test', crops[i])
            #cv2.waitKey(1000)
        return crops, scales