import numpy as np

from pycocotools.coco import COCO
from pycocotools.cocoeval import COCOeval

def eval_result(deterctor_name, resolution):
    gt_json='/ssd/data/datasets/COCO/annotations/instances_val2014.json'
    #dt_json='human_detection_result/{}.json'.format(deterctor_name)
    dt_json='human_detection_result/COCO_human_{}_{}.json'.format(deterctor_name, resolution)
    annType = 'bbox'
    cocoGt=COCO(gt_json)
    cocoDt=cocoGt.loadRes(dt_json)

    imgIds=sorted(cocoGt.getImgIds())

    ious = [.5]

    # running evaluation
    cocoEval = COCOeval(cocoGt,cocoDt,annType)

    # reset some parameters
    cocoEval.params.catIds = [1] # only evaluate the human classes
    cocoEval.params.areaRng = [[0 ** 2, 1e5 ** 2]] # we don't need the evaluation for different size 
    cocoEval.areaRngLbl = ['all']
    cocoEval.params.imgIds = imgIds
    cocoEval.params.iouThrs = ious
    cocoEval.evaluate()
    # evaluate(): evaluates detections on every image and every category and
    # concats the results into the "evalImgs" with fields:
    #  T: the threshold of IoUs =len([0.5, 0.55, 0.6 ... 0.95]) in default
    #  D: detection number in the image for the category, =100 e.g. maxDet number in default
    #  G: the number of the ground truth in the image for the category
    #  dtIds      - [1xD] id for each of the D detections (dt)
    #  gtIds      - [1xG] id for each of the G ground truths (gt)
    #  dtMatches  - [TxD] matching gt id at each IoU or 0
    #  gtMatches  - [TxG] matching dt id at each IoU or 0
    #  dtScores   - [1xD] confidence of each dt
    #  gtIgnore   - [1xG] ignore flag for each gt
    #  dtIgnore   - [TxD] ignore flag for each dt at each IoU

    return cocoEval

# get false positive rate, iou_ind is the index of target iou in ious of iou settings
# for example, ious = [0.5, 0.55, 0.6, ... 0.95], I need the fpr of iou=0.5, iou_ind=0
# for example, ious = [0.5, 0.55, 0.6, ... 0.95], I need the fpr of iou=0.6, iou_ind=2
# in this function if at least one of the human in the image is detected, the image is TP
# cat_id = 1: human
def get_FPR(score_thre, cocoEval, iou_ind=0, cat_id=1 ):
    total_img_num = len(cocoEval.params.imgIds)

    # make surethere is only one category, only one area range, and one iou threshold was evaluated
    # len(cocoEval.evalImgs) = #image_ids * #category_ids * #areaRng
    assert total_img_num == len(cocoEval.evalImgs) 

    #score_thre = 0.5
    TP = 0
    FP = 0
    TN = 0
    FN = 0

    print('total image number is {}'.format(total_img_num))
    for evalImg in cocoEval.evalImgs:
        # if there is no detection and no ground truth for the category and the image
        if evalImg is None:
            TN += 1
            continue
        dt_scores = np.array(evalImg['dtScores'])
        dt_matches = evalImg['dtMatches'][iou_ind]
        gt_matches = evalImg['gtMatches'][iou_ind]
        #if len(gt_matches) == 0: # there is ground truth for the category
        #    print('there is no gt match')
        matched_ind = np.where(dt_matches>0)
        matched_scores = dt_scores[matched_ind]
        if len(matched_scores) == 0:
            max_score = 0
        else:
            max_score = max(matched_scores)
        if max_score > score_thre:
            if len(gt_matches) > 0: # there is ground truth for the category
                TP += 1
            else:
                FN += 1
        else:
            if len(gt_matches) > 0: # there is ground truth for the category
                FP += 1
            else:
                TN += 1
    FPR = FP / (FP+TN)
    print('TP: {}, FP: {}, TN: {}, FN: {}'.format(TP, FP, TN, FN))
    print('False Positive rate is {}'.format(FPR))
    return FPR
    
def search_thre(cocoeval):
    scores = [x/100 for x in range(1, 100)]
    FPRs= []
    have_result = False
    thre = None
    for score in scores:
        FPR = get_FPR(score, cocoeval)
        FPRs.append({'score':score, 'FPR':FPR})
        if FPR >= 0.1 and not have_result:
            have_result = True 
            thre = score
    return thre




if __name__=='__main__':
    det_name = 'centernet'
    resolution = 800

    cocoeval = eval_result(det_name, resolution)
    thre = search_thre(cocoeval)
    print('the threshould for {}_{} is {}'.format(det_name, resolution, thre))
    #get_FPR(0.5, cocoeval)
    
