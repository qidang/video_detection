python run_human_detection_dataset.py --detector centernet --dataset modanet --resolution 800
python run_human_detection_dataset.py --detector ssd --dataset modanet --resolution 800
python run_human_detection_dataset.py --detector yolo_v3 --dataset modanet --resolution 800
python run_human_detection_dataset.py --detector centernet --dataset modanet --resolution 1216
python run_human_detection_dataset.py --detector centernet --dataset COCO --resolution 800
python run_human_detection_dataset.py --detector ssd --dataset COCO --resolution 800
python run_human_detection_dataset.py --detector yolo_v3 --dataset COCO --resolution 800
python run_human_detection_dataset.py --detector centernet --dataset COCO --resolution 1216
