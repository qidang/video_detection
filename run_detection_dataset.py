import argparse
import os
import pickle
import torch
import json
import numpy as np
from dev import get_coco_dataset, get_modanet_dataset
from dev import set_device
from dev import get_human_model, get_garment_model
from dev import parse_video_list
from dev import run_detect, run_detect_on_shot
from dev import load_classification_model
from torchcore.data.datasets.coco_person_cv2 import COCOPersonCV2Dataset
from torchcore.data.datasets import ModanetCV2Dataset

def human_detection(dataset, human_model, part, device='cuda' ):
    #data_it = iter(dataloader)
    #inputs = next(data_it)
    #fourcc = 'mp4v'
    #out = cv2.VideoWriter(video_info['out_path'],cv2.VideoWriter_fourcc(*fourcc), frame_rate=video_info['fps'], video_info['size'])
    device = torch.device(device)
    result = {}

    # do human detection and tracking
    for inputs in dataset:
        inputs = set_device(inputs, device)
        image_ids = inputs['image_id']
        #print(inputs['image_sizes'])
        human_batch = human_model(inputs)
        #if torch.is_tensor(inputs['ori_im']):
        #    im = inputs['ori_im'].detach().cpu().numpy()
        #else:
        #    im = inputs['ori_im']
        for ind, human_out in enumerate(human_batch):
            image_id = int(image_ids[ind])
            # human_out is the human boxes in each frame
            boxes = human_out['boxes']
            scores = human_out['scores']
            labels = human_out['labels']

            result[image_id]= human_out
    return result

def save_result(detector_name, resolution, train_part, val_part, dataset):
    root = 'human_detection_result'
    result = {'train2014':train_part, 'val2014':val_part}
    out_path = os.path.join(root, '{}_human_{}_{}.pkl'.format(dataset, detector_name, resolution))
    with open(out_path,'wb') as f:
        pickle.dump(result, f)
    print('the human detection result is saved to {}'.format(out_path))

def save_modanet_result(detector_name, resolution, train_part, val_part, dataset):
    root = 'human_detection_result'
    result = {'train':train_part, 'val':val_part}
    out_path = os.path.join(root, '{}_human_{}_{}_new.pkl'.format(dataset, detector_name, resolution))
    with open(out_path,'wb') as f:
        pickle.dump(result, f)
    print('the human detection result is saved to {}'.format(out_path))

def save_coco_json_result(detect_type, detector_name, resolution, val_part, dataset):
    if detect_type == 'human':
        root = 'human_detection_result'
        out_path = os.path.join(root, '{}_human_{}_{}.json'.format(dataset, detector_name, resolution))
    else:
        root = 'garment_detection_result'
        out_path = os.path.join(root, '{}_garment_{}_{}.json'.format(dataset, detector_name, resolution))

    result = []
    for object_id, content in val_part.items():
        for box, score, label in zip(content['boxes'], content['scores'], content['labels']):
            # convert to xywh from xyxy
            box[2] = box[2] - box[0]
            box[3] = box[3] - box[1]
            result.append({'image_id':object_id,
                           'category_id': int(label),
                           'bbox': np.array(box).astype(int).tolist(),
                           'score': float(score)})
    with open(out_path,'w') as f:
        json.dump(result, f)
    print('the human detection result is saved to {}'.format(out_path))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--type', default='human', choices=['human', 'garment', 'garment_whole'],
                        type=str, help='Human detector')
    parser.add_argument('--detector', default='centernet', choices=['centernet', 'yolo_v3', 'frcnn', 'ssd'],
                        type=str, help='Human detector')
    parser.add_argument('--dataset', default='COCO', choices=['COCO', 'modanet'],
                        type=str, help='input dataset')
    parser.add_argument('--resolution', default=416, 
                        type=int, help='Human detector resolution')
    parser.add_argument('--device', default='cuda', 
                        type=str, help='Garment detector resolution')
    parser.add_argument('--json', action='store_true', 
                         help='Do we just want to save json result')
    args = parser.parse_args()
    print(args)

    detector = args.detector
    resolution = args.resolution
    device = args.device
    detect_type = args.type

    if not args.json:
        if detect_type == 'human':
            print('***********************')
            print('Warning: the threshold is reset here!!!!!!')
            print('***********************')
            model = get_human_model(detector, resolution, device, score_thre=0.1)
        elif detect_type == 'garment':
            model = get_garment_model(detector, resolution, garment_score_thre=None)
        elif detect_type == 'garment_whole':
            model = get_garment_model(detector, resolution, garment_score_thre=None, use_whole_image_detector=True)

    else:
        if detect_type == 'human':
            model = get_human_model(detector, resolution, device, score_thre=0.001)
        elif detect_type == 'garment':
            model = get_garment_model(detector, resolution, garment_score_thre=0.001)
        elif detect_type == 'garment_whole':
            model = get_garment_model(detector, resolution, garment_score_thre=0.001, use_whole_image_detector=True)
    print('model done')
    #classification_model = load_classification_model(device=device)
    if args.dataset == 'COCO':
        image_root = os.path.expanduser('~/data/datasets/COCO')
        anno_path = os.path.expanduser('~/data/annotations/coco2014_instances_person_all.pkl')

        part = 'val2014'
        dataset = get_coco_dataset(detector, resolution, image_root, anno_path, part)
        val_result = human_detection(dataset, model, part)

        if not args.json:
            part = 'train2014'
            dataset = get_coco_dataset(detector, resolution, image_root, anno_path, part)
            train_result = human_detection(dataset, model, part)

    elif args.dataset == 'modanet':
        image_root = os.path.expanduser('~/data/datasets/modanet/Images')
        #anno_path = os.path.expanduser('~/data/annotations/modanet2018_instances_revised.pkl')
        anno_path = os.path.expanduser('~/data/annotations/modanet2018_instances_all.pkl')

        part = 'val'
        dataset = get_modanet_dataset(detector, resolution, image_root, anno_path, part)
        val_result = human_detection(dataset, model, part)
        
        if not args.json:
            part = 'train'
            dataset = get_modanet_dataset(detector, resolution, image_root, anno_path, part)
            train_result = human_detection(dataset, model, part)


    if not args.json:
        if args.dataset == 'modanet':
            save_modanet_result(detector,resolution, train_result, val_result, args.dataset)
        else:
            save_result(detector, resolution, train_result, val_result, args.dataset)
    else:
        save_coco_json_result(detect_type, detector, resolution, val_result, args.dataset)
