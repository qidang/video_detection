import os
import cv2
import argparse
import pickle
import time
from collections import OrderedDict

from set_tracking_ground_truth import track_result, load_pickle, visulize_track
from tracker.tracker import Tracker
from dev import get_transnet, extract_shots, set_device, load_classification_model
from dev import smooth_shots
from dev import get_video_info
from dev import get_garment_result, get_garment_model, get_human_model
from dev import convert_result_to_coco_style
from dev import benchmark_coco
from dev import remove_unrelated_coco_result
from dev import get_dataset, run_whole_image_detection

from torchcore.data.datasets import PatchDataset
from torchcore.data.transforms import ToTensor, Normalize, Compose, ResizeAndPadding, RandomMirror
from benchmark_image_whole import set_no_label, filter_garments_from_tracker, load_pkl_result


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--load_shots', default=None, 
                        type=str, help='Do we want to load shots from file, it should be file_path')
    parser.add_argument('--human_detector', default='centernet', choices=['centernet', 'yolo_v3', 'frcnn', 'ssd'],
                        type=str, help='which visual assessment model we want to load')
    parser.add_argument('--human_resolution', default=416, choices=[416, 800, 1216],
                        type=int, help='Do we want to load tracker from file, it should be file_path')
    parser.add_argument('--garment_detector', default='centernet', choices=['centernet', 'yolo_v3', 'frcnn', 'ssd'],
                        type=str, help='which visual assessment model we want to load')
    parser.add_argument('--garment_resolution', default=256, choices=[128, 256, 512, 800],
                        type=int, help='Do we want to load tracker from file, it should be file_path')
    parser.add_argument('--visual_detector', default='centernet', choices=['centernet', 'yolo_v3', 'frcnn', 'ssd'],
                        type=str, help='which visual assessment model we want to load')
    parser.add_argument('--visual_resolution', default=416, choices=[416, 800, 1216],
                        type=int, help='Do we want to load tracker from file, it should be file_path')
    parser.add_argument('--no_label', action='store_true',
                        help='Do we want to benchmark without garment label')
    parser.add_argument('--garment_detection_whole', action='store_true',
                        help='Do we want to benchmark without garment label')

    args = parser.parse_args()
    print(args)
    if args.garment_detection_whole:
        assert args.garment_resolution == 800

    video_root_path = os.path.expanduser('~/data/fashion_videos/')
    video_name = 'timing_clip'
    video_path = 'timing_clip.mp4'
    #video_path = os.path.join(video_root_path, '{}.mp4'.format(video_name))
    video_info = get_video_info(video_path)
    video_info['video_name'] = video_name
    human_detector = args.human_detector
    human_resolution = args.human_resolution
    garment_detector = args.garment_detector
    garment_resolution = args.garment_resolution
    visual_detector = args.visual_detector
    visual_resolution = args.visual_resolution
    no_label = False
    garment_detection_whole = args.garment_detection_whole

    debug = False
    video_out = os.path.join('out_videos', '{}_timg_test.mp4'.format(video_name))

    # load models before timing 
    transnet_path = './transnet/model/transnet_model-F16_L3_S2_D256'
    transnet, params = get_transnet(transnet_path)

    human_model = get_human_model(human_detector, human_resolution, device='cuda')
    if garment_detection_whole:
        garment_model = get_garment_model(garment=garment_detector, garment_res=800, garment_score_thre=None, use_whole_image_detector=True)
    else:
        garment_model = get_garment_model(garment=garment_detector, garment_res=garment_resolution, garment_score_thre=None)

    times = OrderedDict()
    start = time.time()
    temp = start

    # shot split
    if args.load_shots is None:
        shots = extract_shots(transnet, params, video_path)
    #else:
    #    if not os.path.isfile(args.load_shots):
    #        raise ValueError('Invalid shot path')
    #    with open(args.load_shots, 'rb') as f:
    #        shots = pickle.load(f)

    shots = smooth_shots(shots, score_thre=0.1, frame_thre=10)
    times['shots_time'] = time.time() - temp
    temp = time.time()

    # human detection
    human_dataset, _ = get_dataset(human_detector, human_resolution, video_path)
    human_result = run_whole_image_detection(human_dataset, human_model, video_info)

    times['human_time'] = time.time() - temp
    temp = time.time()

    # tracking
    pic_width = video_info['width']
    pic_height = video_info['height']
    human_path = './human_detection_result/{}_{}_{}.pkl'.format(video_name, human_detector, human_resolution)
    #human_result = load_pickle(human_path)
    device = 'cuda'
    classification_model = load_classification_model(device=device, detector=visual_detector, resolution=visual_resolution)
    tracker = track_result(shots, human_result,video_path, classification_model, pic_width, pic_height)
    #with open('tracker_spe_all10_svd.pkl', 'wb') as f:
    #visulize_track(tracker, video_path, video_info, video_out)
    times['track_time'] = time.time() - temp
    temp = time.time()

    # garment detection
    valid_track_ids = None
    result_root = 'garment_detection_result'
    if garment_detection_whole:
        #dt_pkl_path = os.path.join(result_root, '{}_{}_{}_whole_benchmark.pkl'.format(video_name, garment_detector, 800 ))
        #result = load_pkl_result(dt_pkl_path)
        dataset, _ = get_dataset(garment_detector, garment_resolution, video_path)
        result = run_whole_image_detection(dataset, garment_model, video_info, device='cuda')
        result = filter_garments_from_tracker(tracker, result, valid_track_ids)
    else:
        result = get_garment_result(video_path, tracker, garment_model, garment_detector, valid_track_ids, garment_resolution, video_info, save_result=False, no_label=no_label )

    times['garment_time'] = time.time() - temp

    time_total = time.time() -start
    times['total'] = time_total
    frame_num = video_info['frame_number']
    time_per_frame = time_total / frame_num
    times['time_per_frame'] = time_per_frame
    if garment_detection_whole:
        out_str = 'Human detector: {}_{}, Garment whole detector: {}_{}. Using time {:.2f} seconds, {:.4f} seconds per frame'.format(human_detector, human_resolution, garment_detector, garment_resolution, time_total, time_per_frame)
        print(out_str)
    else:
        out_str = 'Human detector: {}_{}, Garment patch detector: {}_{}. Using time {:.2f} seconds, {:.4f} seconds per frame'.format(human_detector, human_resolution, garment_detector, garment_resolution, time_total, time_per_frame)
        print()
    print(times)
    with open('time_benchmark.log', 'a') as f:
        f.write(out_str+'\n')
        for key, val in times.items():
            f.write('{}:{:.2f},'.format(key, val))
        f.write('\n\n')



    if debug:
        if garment_detection_whole:
            out_pkl_path = os.path.join('benchmark_result', 'human_{}_{}_garment_whole_{}_{}_timing.pkl'.format(human_detector, human_resolution, garment_detector, garment_resolution))
        else:
            out_pkl_path = os.path.join('benchmark_result', 'human_{}_{}_garment_{}_{}_timing.pkl'.format(human_detector, human_resolution, garment_detector, garment_resolution))

        with open(out_pkl_path, 'wb') as f:
            pickle.dump(result, f)
            print('save result to ', out_pkl_path)
