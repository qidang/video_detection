import numpy as np
import os
import pickle
import cv2
import torch
import argparse
import sys

from tracker.tracker import Tracker
from dev import get_transnet, extract_shots, set_device, load_classification_model
from dev import smooth_shots
from dev import get_video_info
from torchcore.data.datasets import PatchDataset
from torchcore.data.transforms import ToTensor, Normalize, Compose, ResizeAndPadding, RandomMirror
from torchcore.tools.visulize_tools import cv_put_text_with_box, cv_put_text

sys.setrecursionlimit(1000000)

def load_pickle(path):
    with open(path, 'rb') as f:
        result = pickle.load(f)
    return result

def track_result(shots, human_result, video_path, classification_model, pic_width, pic_height):
    tracker = Tracker(IoU_thre=0.5, min_valid_length=3, pic_width=pic_width, pic_height=pic_height)
    frame_count = 1
    new_shot = False
    shot_id = 0
    shot_start_frame = 1
    shot_end_frame = 0
    images_in_shot = {}
    cap = cv2.VideoCapture(video_path)
    device = 'cuda'

    transforms_test = Compose([ResizeAndPadding(160), ToTensor(), Normalize()])
    for i, shot in enumerate(shots):
        # if there is no human detection
        if frame_count not in human_result:
            boxes = []
        else:
            boxes = human_result[frame_count]
            if boxes is None:
                boxes = []
            else:
                boxes = boxes['boxes']
        tracker.track_next(boxes)

        if shot > 0.1 or i+1==len(shots):
            new_shot = True
            tracker.stash()
            #tracker.cal_track_model_score(shot_id)
            shot_id += 1
            shot_end_frame = frame_count

        ret, frame = cap.read()
        images_in_shot[frame_count] = frame
        if new_shot:
            new_shot = False
            # do detection for the previous shot
            shot_boxes = tracker.get_shot_boxes(shot_id)
            print('shot {}, start frame: {}, end frame: {}'.format(shot_id, shot_start_frame, shot_end_frame))
            #print(shot_boxes.keys())

            for frame in range(shot_start_frame, shot_end_frame+1):
                if frame not in shot_boxes:
                    #cv_put_text_with_box(im, 'Shot'+str(shot_id), (0,0), cv2.FONT_HERSHEY_COMPLEX, 2, font_color=(0, 0, 0), box_color=(255,255,255),thickness=2)
                    continue
                boxes = shot_boxes[frame]['boxes']
                nodes = shot_boxes[frame]['nodes']
                im = images_in_shot[frame]
                if classification_model is not None:
                    patch_dataset = PatchDataset(im, boxes, transforms=transforms_test, single_mode=True)
                    dataloader = torch.utils.data.DataLoader(patch_dataset, batch_size=64, num_workers=0, shuffle=False, drop_last=False)
                    labels = []
                    for inputs_patch, _ in dataloader:
                        inputs_patch = set_device(inputs_patch, device)
                        output = classification_model(inputs_patch)
                        labels.append(output['pred'].detach().cpu().numpy())
                    labels = np.hstack(labels)

                    #classified_boxes = boxes[np.where(labels==1)]
                    #omitted_boxes = boxes[np.where(labels==0)]
                    for i, node in enumerate(nodes):
                        #print('nodes has classification score', node)
                        if labels[i] == 1:
                            node.classification_score = 1
                        else:
                            node.classification_score = 0
            
            tracker.cal_track_model_score(shot_id)
            images_in_shot = {}
            shot_start_frame = shot_end_frame + 1

        frame_count += 1
        #if shot_id == 5:
        #    break
    cap.release()
    return tracker

def visulize_nodes(im, nodes, model ):
    for node in nodes:
        box = node.box
        track_id = node.track_id
        pos = (int(box[0]), int(box[1]+20))
        cv_put_text(im, 'ID: '+str(track_id), pos, cv2.FONT_HERSHEY_COMPLEX, 1, (255, 0, 255), 2 )
        if model:
            if node.interpolate:
                cv2.rectangle(im, tuple(box[:2]), tuple(box[2:]),(55,0,0), lineType=4)
            else:
                cv2.rectangle(im, tuple(box[:2]), tuple(box[2:]),(0,255,0), lineType=4)

            #cv2.rectangle(im, tuple(box[:2]), tuple(box[2:]),(0,255,0), lineType=4)
            cv_put_text_with_box(im, 'model', tuple(box[:2].astype(int)), cv2.FONT_HERSHEY_COMPLEX, 1, font_color=(0, 0, 0), box_color=(0,255,0),thickness=2)
        else:
            cv2.rectangle(im, tuple(box[:2]), tuple(box[2:]),(0,0,255), lineType=4)
            cv_put_text_with_box(im, 'audience', tuple(box[:2].astype(int)), cv2.FONT_HERSHEY_COMPLEX, 1, font_color=(0, 0, 0), box_color=(0,0,255),thickness=2)

def visulize_track(tracker, video_path,video_info, out_path):
    model_nodes, human_nodes = tracker.convert_tracks(one_model_per_shot=False)
    shots = tracker.shots
    print(shots.keys())
    cap = cv2.VideoCapture(video_path)
    fourcc = 'mp4v'
    out = cv2.VideoWriter(out_path, cv2.VideoWriter_fourcc(*fourcc), video_info['fps'], video_info['size'])
    frame_num = 0
    shot_num = 1
    while True:
        ret, frame = cap.read()
        frame_num += 1
        if ret == False:
            break
        if frame_num >= shots[shot_num]['end_frame']:
            shot_num += 1
        cv_put_text_with_box(frame, 'Shot'+str(shot_num), (0,0), cv2.FONT_HERSHEY_COMPLEX, 2, font_color=(0, 0, 0), box_color=(255,255,255),thickness=2)
        if frame_num in model_nodes:
            nodes = model_nodes[frame_num]
            visulize_nodes(frame, nodes, model=True )
        if frame_num in human_nodes:
            nodes = human_nodes[frame_num]
            visulize_nodes(frame, nodes, model=False)
        out.write(frame)
    out.release()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--load_shots', default=None, 
                        type=str, help='Do we want to load shots from file, it should be file_path')
    parser.add_argument('--load_tracker', default=None, 
                        type=str, help='Do we want to load tracker from file, it should be file_path')
    parser.add_argument('--visual_detector', default='centernet', choices=['centernet', 'yolo_v3', 'frcnn', 'ssd'],
                        type=str, help='which visual assessment model we want to load')
    parser.add_argument('--visual_resolution', default=416, choices=[416, 800, 1216],
                        type=int, help='Do we want to load tracker from file, it should be file_path')

    args = parser.parse_args()

    video_root_path = os.path.expanduser('~/data/fashion_videos/')
    video_name = 'IDA'
    video_path = os.path.join(video_root_path, '{}.mp4'.format(video_name))
    video_info = get_video_info(video_path)
    visual_detector = args.visual_detector
    visual_resolution = args.visual_resolution

    #video_out = os.path.join('out_videos', '{}_groundtruth_one.mp4'.format(video_name))
    video_out = os.path.join('out_videos', '{}_groundtruth_spectral_clustering_all10_svd.mp4'.format(video_name))

    if args.load_shots is None:
        transnet_path = './transnet/model/transnet_model-F16_L3_S2_D256'
        transnet, params = get_transnet(transnet_path)
        shots = extract_shots(transnet, params, video_path)
    else:
        if not os.path.isfile(args.load_shots):
            raise ValueError('Invalid shot path')
        with open(args.load_shots, 'rb') as f:
            shots = pickle.load(f)

    shots = smooth_shots(shots, score_thre=0.1, frame_thre=10)
    pic_width = video_info['width']
    pic_height = video_info['height']
    human_path = './human_detection_result/{}_merged_human_all10_svd.pkl'.format(video_name)
    human_result = load_pickle(human_path)
    device = 'cuda'
    classification_model = load_classification_model(device=device, detector=visual_detector, resolution=visual_resolution)
    if args.load_tracker is not None:
        tracker = load_pickle(args.load_tracker)
    else:
        tracker = track_result(shots, human_result,video_path, classification_model, pic_width, pic_height)
        #with open('tracker_spe_all10_svd.pkl', 'wb') as f:
        with open('trackers/tracker_{}_{}.pkl'.format(visual_detector, visual_resolution), 'wb') as f:
            pickle.dump(tracker, f)
    visulize_track(tracker, video_path, video_info, video_out)