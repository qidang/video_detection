#python system_benchmark_timing.py --human_detector centernet --human_resolution 416 --garment_detector centernet --garment_resolution 800 --garment_detection_whole
#python system_benchmark_timing.py --human_detector centernet --human_resolution 800 --garment_detector centernet --garment_resolution 800 --garment_detection_whole
#python system_benchmark_timing.py --human_detector centernet --human_resolution 1216 --garment_detector centernet --garment_resolution 800 --garment_detection_whole
#python system_benchmark_timing.py --human_detector ssd --human_resolution 416 --garment_detector ssd --garment_resolution 800 --garment_detection_whole
#python system_benchmark_timing.py --human_detector ssd --human_resolution 800 --garment_detector ssd --garment_resolution 800 --garment_detection_whole
#python system_benchmark_timing.py --human_detector yolo_v3 --human_resolution 416 --garment_detector yolo_v3 --garment_resolution 800 --garment_detection_whole
#python system_benchmark_timing.py --human_detector yolo_v3 --human_resolution 800 --garment_detector yolo_v3 --garment_resolution 800 --garment_detection_whole

export PYTHONWARNINGS="ignore"
python system_benchmark_timing.py --human_detector centernet --human_resolution 416 --garment_detector centernet --garment_resolution 128
python system_benchmark_timing.py --human_detector centernet --human_resolution 416 --garment_detector centernet --garment_resolution 256
python system_benchmark_timing.py --human_detector centernet --human_resolution 416 --garment_detector centernet --garment_resolution 512
python system_benchmark_timing.py --human_detector centernet --human_resolution 416 --garment_detector centernet --garment_resolution 800 --garment_detection_whole

python system_benchmark_timing.py --human_detector centernet --human_resolution 416 --garment_detector ssd --garment_resolution 128
python system_benchmark_timing.py --human_detector centernet --human_resolution 416 --garment_detector ssd --garment_resolution 256
python system_benchmark_timing.py --human_detector centernet --human_resolution 416 --garment_detector ssd --garment_resolution 512
python system_benchmark_timing.py --human_detector centernet --human_resolution 416 --garment_detector ssd --garment_resolution 800 --garment_detection_whole

python system_benchmark_timing.py --human_detector centernet --human_resolution 416 --garment_detector yolo_v3 --garment_resolution 128
python system_benchmark_timing.py --human_detector centernet --human_resolution 416 --garment_detector yolo_v3 --garment_resolution 256
python system_benchmark_timing.py --human_detector centernet --human_resolution 416 --garment_detector yolo_v3 --garment_resolution 512
python system_benchmark_timing.py --human_detector centernet --human_resolution 416 --garment_detector yolo_v3 --garment_resolution 800 --garment_detection_whole

python system_benchmark_timing.py --human_detector centernet --human_resolution 800 --garment_detector centernet --garment_resolution 128
python system_benchmark_timing.py --human_detector centernet --human_resolution 800 --garment_detector centernet --garment_resolution 256
python system_benchmark_timing.py --human_detector centernet --human_resolution 800 --garment_detector centernet --garment_resolution 512
python system_benchmark_timing.py --human_detector centernet --human_resolution 800 --garment_detector centernet --garment_resolution 800 --garment_detection_whole

python system_benchmark_timing.py --human_detector centernet --human_resolution 800 --garment_detector ssd --garment_resolution 128
python system_benchmark_timing.py --human_detector centernet --human_resolution 800 --garment_detector ssd --garment_resolution 256
python system_benchmark_timing.py --human_detector centernet --human_resolution 800 --garment_detector ssd --garment_resolution 512
python system_benchmark_timing.py --human_detector centernet --human_resolution 800 --garment_detector ssd --garment_resolution 800 --garment_detection_whole

python system_benchmark_timing.py --human_detector centernet --human_resolution 800 --garment_detector yolo_v3 --garment_resolution 128
python system_benchmark_timing.py --human_detector centernet --human_resolution 800 --garment_detector yolo_v3 --garment_resolution 256
python system_benchmark_timing.py --human_detector centernet --human_resolution 800 --garment_detector yolo_v3 --garment_resolution 512
python system_benchmark_timing.py --human_detector centernet --human_resolution 800 --garment_detector yolo_v3 --garment_resolution 800 --garment_detection_whole

python system_benchmark_timing.py --human_detector centernet --human_resolution 1216 --garment_detector centernet --garment_resolution 128
python system_benchmark_timing.py --human_detector centernet --human_resolution 1216 --garment_detector centernet --garment_resolution 256
python system_benchmark_timing.py --human_detector centernet --human_resolution 1216 --garment_detector centernet --garment_resolution 512
python system_benchmark_timing.py --human_detector centernet --human_resolution 1216 --garment_detector centernet --garment_resolution 800 --garment_detection_whole

python system_benchmark_timing.py --human_detector centernet --human_resolution 1216 --garment_detector ssd --garment_resolution 128
python system_benchmark_timing.py --human_detector centernet --human_resolution 1216 --garment_detector ssd --garment_resolution 256
python system_benchmark_timing.py --human_detector centernet --human_resolution 1216 --garment_detector ssd --garment_resolution 512
python system_benchmark_timing.py --human_detector centernet --human_resolution 1216 --garment_detector ssd --garment_resolution 800 --garment_detection_whole

python system_benchmark_timing.py --human_detector centernet --human_resolution 1216 --garment_detector yolo_v3 --garment_resolution 128
python system_benchmark_timing.py --human_detector centernet --human_resolution 1216 --garment_detector yolo_v3 --garment_resolution 256
python system_benchmark_timing.py --human_detector centernet --human_resolution 1216 --garment_detector yolo_v3 --garment_resolution 512
python system_benchmark_timing.py --human_detector centernet --human_resolution 1216 --garment_detector yolo_v3 --garment_resolution 800 --garment_detection_whole


python system_benchmark_timing.py --human_detector ssd --human_resolution 416 --garment_detector centernet --garment_resolution 128
python system_benchmark_timing.py --human_detector ssd --human_resolution 416 --garment_detector centernet --garment_resolution 256
python system_benchmark_timing.py --human_detector ssd --human_resolution 416 --garment_detector centernet --garment_resolution 512
python system_benchmark_timing.py --human_detector ssd --human_resolution 416 --garment_detector centernet --garment_resolution 800 --garment_detection_whole

python system_benchmark_timing.py --human_detector ssd --human_resolution 416 --garment_detector ssd --garment_resolution 128
python system_benchmark_timing.py --human_detector ssd --human_resolution 416 --garment_detector ssd --garment_resolution 256
python system_benchmark_timing.py --human_detector ssd --human_resolution 416 --garment_detector ssd --garment_resolution 512
python system_benchmark_timing.py --human_detector ssd --human_resolution 416 --garment_detector ssd --garment_resolution 800 --garment_detection_whole

python system_benchmark_timing.py --human_detector ssd --human_resolution 416 --garment_detector yolo_v3 --garment_resolution 128
python system_benchmark_timing.py --human_detector ssd --human_resolution 416 --garment_detector yolo_v3 --garment_resolution 256
python system_benchmark_timing.py --human_detector ssd --human_resolution 416 --garment_detector yolo_v3 --garment_resolution 512
python system_benchmark_timing.py --human_detector ssd --human_resolution 416 --garment_detector yolo_v3 --garment_resolution 800 --garment_detection_whole

python system_benchmark_timing.py --human_detector ssd --human_resolution 800 --garment_detector centernet --garment_resolution 128
python system_benchmark_timing.py --human_detector ssd --human_resolution 800 --garment_detector centernet --garment_resolution 256
python system_benchmark_timing.py --human_detector ssd --human_resolution 800 --garment_detector centernet --garment_resolution 512
python system_benchmark_timing.py --human_detector ssd --human_resolution 800 --garment_detector centernet --garment_resolution 800 --garment_detection_whole

python system_benchmark_timing.py --human_detector ssd --human_resolution 800 --garment_detector ssd --garment_resolution 128
python system_benchmark_timing.py --human_detector ssd --human_resolution 800 --garment_detector ssd --garment_resolution 256
python system_benchmark_timing.py --human_detector ssd --human_resolution 800 --garment_detector ssd --garment_resolution 512
python system_benchmark_timing.py --human_detector ssd --human_resolution 800 --garment_detector ssd --garment_resolution 800 --garment_detection_whole

python system_benchmark_timing.py --human_detector ssd --human_resolution 800 --garment_detector yolo_v3 --garment_resolution 128
python system_benchmark_timing.py --human_detector ssd --human_resolution 800 --garment_detector yolo_v3 --garment_resolution 256
python system_benchmark_timing.py --human_detector ssd --human_resolution 800 --garment_detector yolo_v3 --garment_resolution 512
python system_benchmark_timing.py --human_detector ssd --human_resolution 800 --garment_detector yolo_v3 --garment_resolution 800 --garment_detection_whole


python system_benchmark_timing.py --human_detector yolo_v3 --human_resolution 416 --garment_detector centernet --garment_resolution 128
python system_benchmark_timing.py --human_detector yolo_v3 --human_resolution 416 --garment_detector centernet --garment_resolution 256
python system_benchmark_timing.py --human_detector yolo_v3 --human_resolution 416 --garment_detector centernet --garment_resolution 512
python system_benchmark_timing.py --human_detector yolo_v3 --human_resolution 416 --garment_detector centernet --garment_resolution 800 --garment_detection_whole

python system_benchmark_timing.py --human_detector yolo_v3 --human_resolution 416 --garment_detector ssd --garment_resolution 128
python system_benchmark_timing.py --human_detector yolo_v3 --human_resolution 416 --garment_detector ssd --garment_resolution 256
python system_benchmark_timing.py --human_detector yolo_v3 --human_resolution 416 --garment_detector ssd --garment_resolution 512
python system_benchmark_timing.py --human_detector yolo_v3 --human_resolution 416 --garment_detector ssd --garment_resolution 800 --garment_detection_whole

python system_benchmark_timing.py --human_detector yolo_v3 --human_resolution 416 --garment_detector yolo_v3 --garment_resolution 128
python system_benchmark_timing.py --human_detector yolo_v3 --human_resolution 416 --garment_detector yolo_v3 --garment_resolution 256
python system_benchmark_timing.py --human_detector yolo_v3 --human_resolution 416 --garment_detector yolo_v3 --garment_resolution 512
python system_benchmark_timing.py --human_detector yolo_v3 --human_resolution 416 --garment_detector yolo_v3 --garment_resolution 800 --garment_detection_whole

python system_benchmark_timing.py --human_detector yolo_v3 --human_resolution 800 --garment_detector centernet --garment_resolution 128
python system_benchmark_timing.py --human_detector yolo_v3 --human_resolution 800 --garment_detector centernet --garment_resolution 256
python system_benchmark_timing.py --human_detector yolo_v3 --human_resolution 800 --garment_detector centernet --garment_resolution 512
python system_benchmark_timing.py --human_detector yolo_v3 --human_resolution 800 --garment_detector centernet --garment_resolution 800 --garment_detection_whole

python system_benchmark_timing.py --human_detector yolo_v3 --human_resolution 800 --garment_detector ssd --garment_resolution 128
python system_benchmark_timing.py --human_detector yolo_v3 --human_resolution 800 --garment_detector ssd --garment_resolution 256
python system_benchmark_timing.py --human_detector yolo_v3 --human_resolution 800 --garment_detector ssd --garment_resolution 512
python system_benchmark_timing.py --human_detector yolo_v3 --human_resolution 800 --garment_detector ssd --garment_resolution 800 --garment_detection_whole

python system_benchmark_timing.py --human_detector yolo_v3 --human_resolution 800 --garment_detector yolo_v3 --garment_resolution 128
python system_benchmark_timing.py --human_detector yolo_v3 --human_resolution 800 --garment_detector yolo_v3 --garment_resolution 256
python system_benchmark_timing.py --human_detector yolo_v3 --human_resolution 800 --garment_detector yolo_v3 --garment_resolution 512
python system_benchmark_timing.py --human_detector yolo_v3 --human_resolution 800 --garment_detector yolo_v3 --garment_resolution 800 --garment_detection_whole
#python system_benchmark_timing.py --human_detector centernet --human_resolution 800 --garment_detector centernet --garment_resolution 512
#python system_benchmark_timing.py --human_detector centernet --human_resolution 1216 --garment_detector centernet --garment_resolution 512
#python system_benchmark_timing.py --human_detector ssd --human_resolution 416 --garment_detector ssd --garment_resolution 512
#python system_benchmark_timing.py --human_detector ssd --human_resolution 800 --garment_detector ssd --garment_resolution 512
#python system_benchmark_timing.py --human_detector yolo_v3 --human_resolution 416 --garment_detector yolo_v3 --garment_resolution 512
#python system_benchmark_timing.py --human_detector yolo_v3 --human_resolution 800 --garment_detector yolo_v3 --garment_resolution 512

#python system_benchmark_timing.py --human_detector frcnn --human_resolution 416 --garment_detector frcnn --garment_resolution 512
#python system_benchmark_timing.py --human_detector frcnn --human_resolution 800 --garment_detector frcnn --garment_resolution 512
#python system_benchmark_timing.py --human_detector frcnn --human_resolution 1216 --garment_detector frcnn --garment_resolution 512
#python system_benchmark_timing.py --human_detector frcnn --human_resolution 416 --garment_detector frcnn --garment_resolution 800 --garment_detection_whole
#python system_benchmark_timing.py --human_detector frcnn --human_resolution 800 --garment_detector frcnn --garment_resolution 800 --garment_detection_whole
#python system_benchmark_timing.py --human_detector frcnn --human_resolution 1216 --garment_detector frcnn --garment_resolution 800 --garment_detection_whole
