import torch
import cv2

class VideoDataset(torch.utils.data.IterableDataset):
    def __init__(self, video_path, transforms=None):
        super(VideoDataset).__init__()
        self.video_path = video_path
        self.cap = cv2.VideoCapture(video_path)
        self.transforms = transforms
        self.width  = self.cap.get(cv2.CAP_PROP_FRAME_WIDTH)   # float
        self.height = self.cap.get(cv2.CAP_PROP_FRAME_HEIGHT)  # float
        self.fps = self.cap.get(cv2.CAP_PROP_FPS)
        self.size = (int(self.width), int(self.height))
        codec = int(self.cap.get(cv2.CAP_PROP_FOURCC))
        self.fourcc = chr(codec&0xFF) + chr((codec>>8)&0xFF) + chr((codec>>16)&0xFF) + chr((codec>>24)&0xFF)

    def __iter__(self):
        while(True):
            ret, frame = self.cap.read()
            if not ret:
                return

            #im = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            im = frame
            inputs = {}
            inputs['data'] = im
            inputs['ori_im'] = im
            if self.transforms is not None:
                inputs = self.transforms(inputs)
            yield inputs

    def get_info(self):
        info = {}
        info['width'] = self.width
        info['height'] = self.height
        info['fps'] = self.fps
        info['size'] = self.size
        info['fourcc'] = self.fourcc

        return info



#class VideoDatasetIterator():
#    def __init__(self, video_dataset):
#        self.cap = cv2.VideoCapture(video_dataset.video_path)
#        self.transforms = video_dataset.transforms
#
#    def __next__(self):
#        ret, frame = cap.read()
#        if not ret:
#            self.cap.release()
#            raise StopIteration()