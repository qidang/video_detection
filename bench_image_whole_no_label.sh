python benchmark_image_whole.py --tracker tracker_spe_all10_svd.pkl --garment_detector centernet --no_label
python benchmark_image_whole.py --tracker tracker_spe_all10_svd.pkl --garment_detector frcnn --no_label
python benchmark_image_whole.py --tracker tracker_spe_all10_svd.pkl --garment_detector ssd --no_label
python benchmark_image_whole.py --tracker tracker_spe_all10_svd.pkl --garment_detector yolo_v3 --no_label
