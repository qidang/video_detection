import os
import pickle
import cv2

from dev import merge_human_result
from torchcore.tools.visulize_tools import cv_put_text_with_box, cv_put_text

def visulize_human(im, boxes, labels, scores):
    color = (255, 0, 255)
    for box, label, score in zip(boxes, labels, scores):
        cv2.rectangle(im, tuple(box[:2]), tuple(box[2:]),color, lineType=4)
        pos = (int(box[0]), int(box[1]+20))
        cv_put_text(im, 'label: '+str(label), pos, cv2.FONT_HERSHEY_COMPLEX, 1, (255, 0, 255), 2 )
        pos = (int(box[0]), int(box[1]+40))
        cv_put_text(im, 'score: {:.3f}'.format(score), pos, cv2.FONT_HERSHEY_COMPLEX, 1, (255, 0, 255), 2 )


if __name__ == '__main__':
    result_root = 'human_detection_result'
    results_name = ['IDA_frcnn_416', 'IDA_yolo_v3_416', 'IDA_centernet_416', 'IDA_ssd_416']
    #results_name = ['IDA_frcnn_416', 'IDA_centernet_416', 'IDA_ssd_416']
    #results_name = ['IDA_yolo_v3_416']
    video_root_path = os.path.expanduser('~/data/fashion_videos/')
    video_name = 'IDA'
    video_path = os.path.join(video_root_path, '{}.mp4'.format(video_name))
    out_path = os.path.join('out_videos', '{}_merge.mp4'.format(video_name))
    out_pkl_path = os.path.join(result_root, '{}_merged_human.pkl'.format(video_name))

    results = []
    for name in results_name:
        path = os.path.join(result_root, '{}.pkl'.format(name))
        with open(path, 'rb') as f:
            result = pickle.load(f)
            results.append(result)

    merge_result = merge_human_result(results)
    with open(out_pkl_path, 'wb') as f:
        pickle.dump(merge_result, f)

    cap = cv2.VideoCapture(video_path)
    width  = cap.get(cv2.CAP_PROP_FRAME_WIDTH)   # float
    height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)  # float
    fps = cap.get(cv2.CAP_PROP_FPS)
    size = (int(width), int(height))
    fourcc = 'mp4v'
    out = cv2.VideoWriter(out_path, cv2.VideoWriter_fourcc(*fourcc), fps, size)
    
    frame_num = 1
    while(True):
        ret, frame = cap.read()
        if not ret:
            break

        #im = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        im = frame
        
        result = merge_result[frame_num]
        if result is not None:
            scores = result['scores']
            labels = result['labels']
            boxes = result['boxes']

            #print('boxes', boxes)
            #print('scores', scores)
            #print('labels', labels)
            visulize_human(im, boxes, labels, scores)
        out.write(im)
        frame_num += 1
        #if frame_num > 300:
        #    break
    out.release()
    
    


