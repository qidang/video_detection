import pickle
import os
import numpy as np
import argparse

def load_pickle(path, part):
    print(path)
    with open(path, 'rb') as f:
        anno = pickle.load(f)
        anno = anno[part]
    return anno

def load_detection(detection_anno):
    anno = {}
    for key, val in detection_anno.items():
        anno[int(key)] = val
    return anno

def transfer_groundtruth(ground_anno):
    new_anno = {}
    for anno in ground_anno:
        anno_id = anno['id']
        boxes = []
        labels = []
        for obj in anno['objects']:
            box = obj['bbox']
            # convert to xyxy
            box[2] += box[0]
            box[3] += box[1]
            boxes.append(box)
            labels.append(obj['category_id'])
        new_anno[anno_id] = {}
        new_anno[anno_id]['boxes'] = np.array(boxes)
        new_anno[anno_id]['labels'] = np.array(labels)
        new_anno[anno_id]['file_name'] = anno['file_name']
    return new_anno

def transfer_detection(detections, ground_anno):
    non_valid = []
    for anno in ground_anno:
        anno_id = anno['id']
        anno['objects'] = []
        if anno_id in detections:
            detects = detections[anno_id]
            for box, label in zip(detects['boxes'], detects['labels']):
                obj = {}
                obj['bbox'] = box
                obj['category_id'] = label
                anno['objects'].append(obj)
        else:
            non_valid.append(anno_id)

    for anno_id in non_valid:
        del ground_anno[anno_id]

    return ground_anno


def cal_iou(boxa, boxb):
    x1 = max(boxa[0], boxb[0])
    y1 = max(boxa[1], boxb[1])
    x2 = min(boxa[2], boxb[2])
    y2 = min(boxa[3], boxb[3])
    if x2<= x1 or y2 <= y1:
        return 0
    inter = (x2-x1)*(y2-y1)
    union = (boxa[2]-boxa[0])*(boxa[3]-boxa[1]) + (boxb[2]-boxb[0])*(boxb[3]-boxb[1]) - inter
    iou = inter/union
    return iou

def gen_IoU_matrix(gt_boxes, dt_boxes):
    gt_len = len(gt_boxes)
    dt_len = len(dt_boxes)
    mat = np.zeros((dt_len, gt_len))
    for i, dt_box in enumerate(dt_boxes):
        for j, gt_box in enumerate(gt_boxes):
            iou = cal_iou(dt_box, gt_box)
            mat[i][j] = iou
    return mat

def filter_boxes(ground_anno, detection_anno, class_num, iou_thre=0.8 ):
    result = {}
    for im_id in detection_anno:
        boxes = []
        labels = []

        for cla in range(1, class_num+1):
            gt_ind = np.where(ground_anno[im_id]['labels'] == cla)
            dt_ind = np.where(detection_anno[im_id]['labels'] == cla)
            if len(gt_ind[0])==0 or len(dt_ind[0])==0:
                continue
            gt_boxes = ground_anno[im_id]['boxes'][gt_ind]
            dt_boxes = detection_anno[im_id]['boxes'][dt_ind]
            if len(gt_boxes) ==0 or len(dt_boxes) == 0:
                continue
            iou_mat = gen_IoU_matrix(gt_boxes, dt_boxes)
            cla_boxes = []
            cla_labels = []
            for i, iou in enumerate(iou_mat):
                if iou.max() >= iou_thre:
                    cla_boxes.append(dt_boxes[i])
                    cla_labels.append(cla)
            boxes.extend(cla_boxes)
            labels.extend(cla_labels)

        result[im_id] = {}
        result[im_id]['boxes'] = boxes
        result[im_id]['labels'] = labels
    return result


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--detector', default='centernet', choices=['centernet', 'yolo_v3', 'frcnn', 'ssd'],
                        type=str, help='Human detector')
    parser.add_argument('--resolution', default=416, 
                        type=int, help='Human detector resolution')
    args= parser.parse_args()

    detector = args.detector
    resolution = args.resolution

    ground_path = os.path.expanduser('~/data/annotations/coco2014_instances_person_all.pkl')
    detection_path = os.path.join('human_detection_result', 'COCO_human_{}_{}.pkl'.format(detector, resolution))
    out_path = os.path.join('human_detection_result', 'coco_human_{}_{}_transfered.pkl'.format(detector, resolution))
    class_num = 1
    result = {}

    part = 'train2014'
    ground_anno = load_pickle(ground_path, part)
    ground_anno_out = transfer_groundtruth(ground_anno)

    detection_anno = load_pickle(detection_path, part)
    detection_anno_out = load_detection(detection_anno)

    detect_out = filter_boxes(ground_anno_out, detection_anno_out, class_num)
    detection_transferred_train = transfer_detection(detect_out, ground_anno)

    part = 'val2014'
    ground_anno = load_pickle(ground_path, part)
    ground_anno_out = transfer_groundtruth(ground_anno)

    detection_anno = load_pickle(detection_path, part)
    detection_anno_out = load_detection(detection_anno)

    detect_out = filter_boxes(ground_anno_out, detection_anno_out, class_num)
    detection_transferred_test = transfer_detection(detect_out, ground_anno)

    result['train2014'] = detection_transferred_train
    result['val2014'] = detection_transferred_test
    
    with open(out_path, 'wb') as f:
        pickle.dump(result, f)
