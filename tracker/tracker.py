import numpy as np
import time

class Node:
    def __init__(self, index, box, frame_num):
        self._parents = None
        self._child = None
        self._index = index
        self._frame_num = frame_num
        self._connected = False
        self._box = box

    @property
    def connected(self):
        return self._connected

    @connected.setter
    def connected(self, value):
        self._connected = value

    @property
    def box(self):
        return self._box

    @property
    def box_area(self):
        if hasattr(self, '_box_area'):
            return self._box_area
        else:
            box = self._box
            w = box[2] - box[0]
            h = box[3] - box[1]
            self._box_area = w*h
            return self._box_area

    @property
    def box_center(self):
        if hasattr(self, '_box_center'):
            return self._box_center
        else:
            xc = (self._box[0] + self._box[2]) / 2
            yc = (self._box[1] + self._box[3]) / 2
            self._box_center = (xc, yc)
            return self._box_center

    @property
    def frame_num(self):
        return self._frame_num

    @property
    def parents(self):
        return self._parents

    @parents.setter
    def parents(self, value):
        self._parents = value

    @property
    def child(self):
        return self._child

    @child.setter
    def child(self, value):
        self._child = value

class Track:
    def __init__(self, head, track_id):
        if isinstance(head, Node):
            head.interpolate = False
            self._head = head
            self._end = head
        else:
            raise ValueError('track head should be instance of node')
        self._track_id = track_id

    def __len__(self):
        return self._end.frame_num - self._head.frame_num +1

    @property
    def end(self):
        return self._end

    @property
    def head(self):
        return self._head

    @property
    def track_id(self):
        return self._track_id

    # add prediction means: if the added node is not right after
    # the previous node, we insert more nodes by interpolation
    def add_node(self, node, iou=0, interpolate=False):
        if self._end._frame_num != node._frame_num + 1 and interpolate:
            # interpolate the node
            #print('start to interpolate node')
            #print('current node is frame: {}'.format(node.frame_num))
            #print('end node is frame: {}'.format(self.end.frame_num))
            self.interpolate_nodes(node)
            anode = self.end
            #while anode is not None:
            #    print( anode.frame_num)
            #    anode = anode.parents

        self._end.iou = iou
        node._parents = self._end
        node.connected = True
        node.interpolate = False
        self._end.child = node
        self._end = node

    def interpolate_nodes(self, node):
        pre_box = self._end.box
        cur_box = node.box
        frame = self._end.frame_num
        frame_diff = node.frame_num - self.end.frame_num
        for i in range(1, frame_diff):
            box = i/frame_diff*pre_box+(frame_diff-i)/frame_diff*cur_box
            node = Node(-1, box, frame+i)
            node.parents = self._end
            node.interpolate = True
            node.connected = True
            self._end.child = node
            self._end = node

    def interpolate_num(self):
        node = self.head
        count = 0
        while node is not None:
            if node.interpolate:
                count += 1
            node = node.child
        return count

    def non_interpolate_num(self):
        node = self.head
        count = 0
        while node is not None:
            if not node.interpolate:
                count += 1
            node = node.child
        return count

class Tracker:
    def __init__(self, IoU_thre=0.5, min_valid_length=3, pic_width=-1, pic_height=-1):
        self._tracks = []
        self._cur_frame = 0
        self._current_tracks = []
        self.IoU_thre = IoU_thre
        self.min_valid_length = min_valid_length
        self.track_count = 0
        self.width = pic_width
        self.height = pic_height
        self.min_width_c = pic_width / 4
        self.max_width_c = pic_width *3 / 4
        self.shot_num = 1
        self.shot_len = {}
        self.shot_start_frame = 1
        self.shots = {}

    @property
    def tracks(self):
        return self._tracks

    def init_nodes(self, boxes, frame, labels=None, scores=None):
        nodes = []
        for index, box in enumerate(boxes):
            new_node = Node(index, box, frame)
            if labels is not None:
                new_node.label = labels[index]
            if scores is not None:
                new_node.score = scores[index]
            nodes.append(new_node)
        return nodes

    def add_iou_to_nodes(self, nodes):
        nodes_num = len(nodes)
        iou_mat = np.zeros((nodes_num, nodes_num))
        for i in range(nodes_num):
            for j in range(i+1, nodes_num):
                iou_mat[i][j] = cal_IoU(nodes[i].box, nodes[j].box)
                iou_mat[j][i] = iou_mat[i][j]
        # calculate average IoU for Non-zero numbers
        for i in range(nodes_num):
            non_zero_num = (iou_mat[i,:]!=0).sum()
            if non_zero_num != 0:
                iou_sum = iou_mat[i,:].sum()
                iou = iou_sum / non_zero_num
            else:
                iou = 0
            nodes[i].inner_iou = iou

    def cal_track_inner_iou(self):
        for track in self._tracks:
            cur_node = track.head
            inner_iou = 0
            while cur_node != None:
                inner_iou += cur_node.inner_iou
                cur_node = cur_node._child
            track.inner_iou = inner_iou / len(track)

    def connect_box(self, sorted_ious, nodes):
        for pair in sorted_ious:
            j = pair[0]
            k = pair[1]
            iou = pair[2]
            if self._current_tracks[j].end.frame_num != nodes[k].frame_num and not nodes[k].connected:
                #connect the node to current_tracks
                self._current_tracks[j].add_node(nodes[k], iou=iou, interpolate=True)

    def is_complete_track(self, track, threshold=0):
        #if track.end.frame_num != self._cur_frame:
        if self._cur_frame - track.end.frame_num > threshold:
            return True
        else:
            return False

    def add_tracks(self):
        i = 0
        while i < len(self._current_tracks):
            if self.is_complete_track(self._current_tracks[i], threshold=25):
                self._tracks.append(self._current_tracks[i])
                del self._current_tracks[i]
            else:
                i = i+1

    def stash(self):
        # save the shots in the tracker
        self.shots[self.shot_num] = {}
        self.shots[self.shot_num]['start_frame'] = self.shot_start_frame
        self.shots[self.shot_num]['end_frame'] = self._cur_frame

        self.shot_len[self.shot_num] = self._cur_frame - self.shot_start_frame + 1
        self.shot_start_frame = self._cur_frame + 1
        self.shot_num += 1
        i = 0
        while i < len(self._current_tracks):
            self._tracks.append(self._current_tracks[i])
            del self._current_tracks[i]

    def add_nodes_to_cur_track(self, nodes):
        for node in nodes:
            if not node.connected:
                track_id = self.track_count
                self.track_count += 1
                new_track = Track(node, track_id)
                new_track.shot_id = self.shot_num
                self._current_tracks.append(new_track)

    def set_shot_num(self, shot_num):
        self.shot_num = shot_num

    def track_garments_in_track(self, track):
        node = track.head
        while node is not None:
            boxes = node.garment_boxes
            labels = node.garment_labels
            scores = node.garment_scores
            current_frame = node.frame_num
            self.track_next_garment(boxes, current_frame, labels=labels, scores=scores)
            node = node.child
        i = 0
        while i < len(self._current_tracks):
            self._tracks.append(self._current_tracks[i])
            del self._current_tracks[i]

    def track_next_garment(self, boxes, current_frame, labels=None, scores=None):
        track_len = len(self._current_tracks)
        cur_num = len(boxes)

        # calculate IoUs between current track and current boxes
        IoUs = []
        for j in range(track_len):
            for k in range(cur_num):
                iou_temp = cal_IoU(self._current_tracks[j].end.box, boxes[k])
                #only consider the boxes with enough IoU
                if iou_temp >= self.IoU_thre:
                    IoUs.append([j, k, iou_temp])
        sorted_ious = sorted(IoUs, key=lambda x:x[2], reverse=True)

        # calculate boxes IoUs within a frame
        nodes = self.init_nodes(boxes, current_frame, labels=labels, scores=scores)
        self.add_iou_to_nodes(nodes)

        # connect the ones that can be connected
        self.connect_box(sorted_ious, nodes)

        # put completed tracks to tracks
        self.add_tracks()
        # add unconnected node to tracks
        self.add_nodes_to_cur_track(nodes)

    def track_next(self, boxes):
        self._cur_frame += 1
        #print('current frame is {}'.format(self._cur_frame))

        track_len = len(self._current_tracks)
        cur_num = len(boxes)

        # calculate IoUs between current track and current boxes
        IoUs = []
        for j in range(track_len):
            for k in range(cur_num):
                iou_temp = cal_IoU(self._current_tracks[j].end.box, boxes[k])
                #only consider the boxes with enough IoU
                if iou_temp >= self.IoU_thre:
                    IoUs.append([j, k, iou_temp])
        sorted_ious = sorted(IoUs, key=lambda x:x[2], reverse=True)

        # calculate boxes IoUs within a frame
        nodes = self.init_nodes(boxes, self._cur_frame)
        self.add_iou_to_nodes(nodes)

        # connect the ones that can be connected
        self.connect_box(sorted_ious, nodes)

        # put completed tracks to tracks
        self.add_tracks()
        # add unconnected node to tracks
        self.add_nodes_to_cur_track(nodes)

        #self._tracks.extend(self._current_tracks)
    def get_current_boxes(self):
        current_boxes = []
        box_ids = []
        for track in self._current_tracks:
            # the length of the track shold be bigger than min_vid_length
            if len(track) > self.min_valid_length and track.end.frame_num == self._cur_frame:
                # the person should be in the center in recent frames
                box_center = self.get_track_center(track, max_length=50)
                if box_center[0] > self.min_width_c and box_center[0]< self.max_width_c:
                    current_boxes.append(track.end.box)
                    box_ids.append(track.track_id)
        if len(current_boxes) != 0:
            current_boxes = np.row_stack(current_boxes)
        return current_boxes, box_ids

    def get_shot_boxes(self, shot_id):
        shot_boxes = {}
        for track in reversed(self.tracks):
            #print('shot boxes', track.shot_id)
            if track.shot_id!= shot_id:
                break
            node = track.head
            while node is not None:
                frame_num = node.frame_num
                box = node.box
                if frame_num in shot_boxes:
                    shot_boxes[frame_num]['boxes'].append(box)
                    shot_boxes[frame_num]['nodes'].append(node)
                else:
                    shot_boxes[frame_num] = {'boxes':[box], 'nodes':[node]}
                node = node.child
        for k, v in shot_boxes.items():
            shot_boxes[k]['boxes'] = np.array(v['boxes'])
        return shot_boxes

    def get_shot_interpolate_and_detection(self, shot_id, model_score_threshold):
        inter_count = 0
        total_len = 0
        for track in reversed(self.tracks):
            #print('shot boxes', track.shot_id)
            if track.shot_id!= shot_id:
                break
            if track.model_score < model_score_threshold:
                continue
            inter_count += track.interpolate_num()
            total_len += len(track)
        return inter_count, total_len

    # get the max detection number among all the tracks
    def get_shot_max_detection(self, shot_id, model_score_threshold):
        max_len = 0
        # in case there is no model detection in some shots due to wrong classification
        max_len_all = 0
        for track in reversed(self.tracks):
            #print('shot boxes', track.shot_id)
            if track.shot_id!= shot_id:
                break
            det_num = track.non_interpolate_num()
            if track.model_score > model_score_threshold:
                max_len = max(max_len, det_num)
            max_len_all = max(max_len_all, det_num)
        if max_len == 0:
            return max_len_all
        return max_len

    # get the max detection number among all the tracks and total detection num
    def get_shot_max_people_detection(self, shot_id, model_score_threshold):
        max_len = 0
        total_detection = 0
        # in case there is no model detection in some shots due to wrong classification
        max_len_all = 0
        for track in reversed(self.tracks):
            #print('shot boxes', track.shot_id)
            if track.shot_id!= shot_id:
                break
            det_num = track.non_interpolate_num()
            total_detection += det_num
            if track.model_score > model_score_threshold:
                max_len = max(max_len, det_num)
            max_len_all = max(max_len_all, det_num)
        if max_len == 0:
            return max_len_all, total_detection
        return max_len, total_detection

    def get_garment_detection_num(self, shot_id, model_score_threshold):
        total_detection = 0
        track_len_total = 0
        for track in reversed(self.tracks):
            #print('shot boxes', track.shot_id)
            if track.shot_id!= shot_id:
                break
            if track.model_score < model_score_threshold:
                continue
            det_num = track.non_interpolate_num()
            total_detection += det_num
            track_len_total += len(track)
        return total_detection, track_len_total


    def get_shot_model_boxes(self, shot_id, model_score_threshold):
        shot_boxes = {}
        for track in reversed(self.tracks):
            #print('shot boxes', track.shot_id)
            if track.shot_id!= shot_id:
                break
            if track.model_score < model_score_threshold:
                continue
            node = track.head
            while node is not None:
                frame_num = node.frame_num
                box = node.box
                if frame_num in shot_boxes:
                    shot_boxes[frame_num]['boxes'].append(box)
                    shot_boxes[frame_num]['nodes'].append(node)
                else:
                    shot_boxes[frame_num] = {'boxes':[box], 'nodes':[node]}
                node = node.child
        for k, v in shot_boxes.items():
            shot_boxes[k]['boxes'] = np.array(v['boxes'])
        return shot_boxes

    def get_shot_model_tracks(self, shot_id, model_score_threshold):
        shot_tracks = []
        longest_track = None
        max_len_all = 0
        for track in reversed(self.tracks):
            #print('shot boxes', track.shot_id)
            if track.shot_id!= shot_id:
                break
            det_num = track.non_interpolate_num()
            max_len_all = max(max_len_all, det_num)
            if max_len_all == det_num:
                longest_track = track
            if track.model_score < model_score_threshold:
                continue
            shot_tracks.append(track)
        if len(shot_tracks) == 0:
            shot_tracks.append(None)
        return shot_tracks

    def get_model_track_ids(self, model_score_threshold=0.1):
        model_ids = []
        for track in self.tracks:
            if track.model_score >= model_score_threshold:
                model_ids.append(track.track_id)
        return model_ids
    
    def get_tracks_less_than_id(self, max_id):
        track_ids=[]
        for track in self.tracks:
            if track.track_id <= max_id:
                track_ids.append(track.track_id)
        return track_ids

    def cal_track_model_score(self, shot_id):
        for track in reversed(self.tracks):
            #print(track.shot_id, shot_id)
            if track.shot_id!= shot_id:
                break
            #print('calculate the score of ',track.track_id)
            node = track.head
            cla_scores = []
            box_centers = []
            box_areas = []
            #track_len_score = len(track) / self.cur_shot_len
            track_len_score = len(track) / self.shot_len[shot_id]
            while node is not None:
                #print('node needed:', node)
                #calculate average classification score
                if not hasattr(node, 'classification_score'):
                    print(track.track_id)
                    print(track.head)
                    print(track.shot_id)
                    print(node.frame_num)

                cla_scores.append(node.classification_score)
                box_centers.append(node.box_center[0]/ self.width)
                box_areas.append(node.box_area/ self.width/ self.height)
                node = node.child
            center_score = self.cal_center_score(box_centers)
            cla_score = np.array(cla_scores).mean()
            track.center_score = center_score
            track.len_score = track_len_score
            track.cla_score = cla_score
            
            #size_score = self.cal_size_score(box_areas)
            track.model_score = cla_score * track_len_score * center_score

    def cal_size_score(self, sizes):
        pass

    def cal_center_score(self, centers):
        scores= np.zeros(len(centers))
        for i, center in enumerate(centers):
            score = np.exp(-1*(center-0.5)**2/0.09)
            scores[i] = score
        return scores.mean()

    def get_track_center(self, track, max_length):
        cal_len = min(max_length, len(track))
        cur_node = track.end
        box_center_x = 0
        box_center_y = 0

        for _ in range(cal_len):
            if cur_node is None:
                node = track.end
                while node is not None:
                    print(node.frame_num)
                    node = node.parents
            box_center = cur_node.box_center
            box_center_x += box_center[0]
            box_center_y += box_center[1]
            cur_node = cur_node.parents

        box_center_x = box_center_x / cal_len
        box_center_y = box_center_y / cal_len
        return (box_center_x, box_center_y)


    def filter_tracks(self, min_length, max_inner_iou):
        # if the tracklet is smaller than the min_length, it would be filtered out
        # inner iou means the overlape for one box with other boxes in the same frame
        # we think that the audience can easily overlap each other because they are 
        # more dense while the models should have less overlap
        i = 0
        while i < len(self._tracks):
            if len(self._tracks[i]) < min_length or self._tracks[i].inner_iou >= max_inner_iou:
                del self._tracks[i]
            else:
                i = i+1
    
    def get_tracks_by_id(self, valid_tracks_id):
        valid_tracks = []
        invalid_tracks = []
        for track in self.tracks:
            if track.track_id in valid_tracks_id:
                valid_tracks.append(track)
            else:
                invalid_tracks.append(track)
        return valid_tracks, invalid_tracks


    def convert_tracks(self, model_score_threshold=0.1, one_model_per_shot=False, valid_track_ids=None):
        model_nodes = {}
        human_nodes = {}
        if one_model_per_shot:
            shot_result = self.filter_tracks_each_shot(model_score_threshold=model_score_threshold)
            for shot_id, result in shot_result.items():
                model_track = result['model']
                self.add_node_to_list(model_track, model_nodes)
                for a_track in result['audience']:
                    self.add_node_to_list(a_track, human_nodes)
        elif valid_track_ids is not None:
            valid_tracks, invalid_tracks = self.get_tracks_by_id(valid_track_ids)
            for a_track in valid_tracks:
                self.add_node_to_list(a_track, model_nodes)
            for a_track in invalid_tracks:
                self.add_node_to_list(a_track, human_nodes)
        else:
            for track in self.tracks:
                if not hasattr(track, 'model_score'):
                    print(track.track_id)
                    continue
                cur_node = track.head
                if track.model_score > model_score_threshold:
                    nodes = model_nodes
                else:
                    nodes = human_nodes
                self.add_node_to_list(track, nodes)
        return model_nodes, human_nodes

    def add_node_to_list(self, track, nodes):
        cur_node = track.head
        while cur_node is not None:
            cur_node.track_id = track.track_id
            frame_num = cur_node.frame_num
            if frame_num not in nodes:
                nodes[frame_num] = []
            nodes[frame_num].append(cur_node)
            cur_node = cur_node.child

    def filter_tracks_each_shot(self, model_score_threshold=0.1):
        shot_result = {}
        for track in self.tracks:
            shot_id = track.shot_id
            track_len = len(track)
            model_score = track.model_score
            if shot_id not in shot_result:
                shot_result[shot_id] = {'audience':[], 'model':None, 'max_len':track_len, 'max_len_track':track}
            if model_score>model_score_threshold:
                if shot_result[shot_id]['model'] is None:
                    shot_result[shot_id]['model'] = track
                elif len(shot_result[shot_id]['model']) < track_len:
                    shot_result[shot_id]['audience'].append(shot_result[shot_id]['model'])
                    shot_result[shot_id]['model'] = track
                else:
                    shot_result[shot_id]['audience'].append(track)
            else:
                shot_result[shot_id]['audience'].append(track)
                if len(track)>shot_result[shot_id]['max_len']:
                    shot_result[shot_id]['max_len'] = len(track)
                    shot_result[shot_id]['max_len_track'] = track
        for shot_id in shot_result:
            cur_result = shot_result[shot_id]
            if cur_result['model'] is None:
                cur_result['model'] = cur_result['max_len_track']
            if cur_result['model'] in cur_result['audience']:
                cur_result['audience'].remove(cur_result['model'])
        return shot_result




def cal_IoU(roi1, roi2):
    x1 = max(roi1[0], roi2[0])
    y1 = max(roi1[1], roi2[1])
    x2 = min(roi1[2], roi2[2])
    y2 = min(roi1[3], roi2[3])
    if x1>=x2 or y1>=y2:
        return 0
    inter = (x2 - x1) * (y2 - y1)
    union = (roi1[2]-roi1[0])*(roi1[3]-roi1[1]) + (roi2[2]-roi2[0])*(roi2[3]-roi2[1]) - inter
    IoU= inter/union
    return IoU