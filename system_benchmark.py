import os
import cv2
import argparse
import pickle

from set_tracking_ground_truth import track_result, load_pickle, visulize_track
from tracker.tracker import Tracker
from dev import get_transnet, extract_shots, set_device, load_classification_model
from dev import smooth_shots
from dev import get_video_info
from dev import get_garment_result, get_garment_model
from dev import convert_result_to_coco_style
from dev import benchmark_coco
from dev import remove_unrelated_coco_result
from torchcore.data.datasets import PatchDataset
from torchcore.data.transforms import ToTensor, Normalize, Compose, ResizeAndPadding, RandomMirror
from benchmark_image_whole import set_no_label, filter_garments_from_tracker, load_pkl_result


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--load_shots', default='shots.pkl', 
                        type=str, help='Do we want to load shots from file, it should be file_path')
    parser.add_argument('--human_detector', default='centernet', choices=['centernet', 'yolo_v3', 'frcnn', 'ssd'],
                        type=str, help='which visual assessment model we want to load')
    parser.add_argument('--human_resolution', default=416, choices=[416, 800, 1216],
                        type=int, help='Do we want to load tracker from file, it should be file_path')
    parser.add_argument('--garment_detector', default='centernet', choices=['centernet', 'yolo_v3', 'frcnn', 'ssd'],
                        type=str, help='which visual assessment model we want to load')
    parser.add_argument('--garment_resolution', default=256, choices=[128, 256, 512, 800],
                        type=int, help='Do we want to load tracker from file, it should be file_path')
    parser.add_argument('--visual_detector', default='centernet', choices=['centernet', 'yolo_v3', 'frcnn', 'ssd'],
                        type=str, help='which visual assessment model we want to load')
    parser.add_argument('--visual_resolution', default=416, choices=[416, 800, 1216],
                        type=int, help='Do we want to load tracker from file, it should be file_path')
    parser.add_argument('--no_label', action='store_true',
                        help='Do we want to benchmark without garment label')
    parser.add_argument('--garment_detection_whole', action='store_true',
                        help='Do we want to benchmark without garment label')

    args = parser.parse_args()
    print(args)

    debug = False

    video_root_path = os.path.expanduser('~/data/fashion_videos/')
    video_name = 'IDA'
    video_path = os.path.join(video_root_path, '{}.mp4'.format(video_name))
    video_info = get_video_info(video_path)
    human_detector = args.human_detector
    human_resolution = args.human_resolution
    garment_detector = args.garment_detector
    garment_resolution = args.garment_resolution
    visual_detector = args.visual_detector
    visual_resolution = args.visual_resolution
    no_label = args.no_label
    garment_detection_whole = args.garment_detection_whole


    #video_out = os.path.join('out_videos', '{}_groundtruth_one.mp4'.format(video_name))
    video_out = os.path.join('out_videos', '{}_test.mp4'.format(video_name))

    # shot split
    if args.load_shots is None:
        transnet_path = './transnet/model/transnet_model-F16_L3_S2_D256'
        transnet, params = get_transnet(transnet_path)
        shots = extract_shots(transnet, params, video_path)
    else:
        if not os.path.isfile(args.load_shots):
            raise ValueError('Invalid shot path')
        with open(args.load_shots, 'rb') as f:
            shots = pickle.load(f)

    shots = smooth_shots(shots, score_thre=0.1, frame_thre=10)


    # tracking
    tracker_path = 'trackers/tracker_sys_{}_{}.pkl'.format(human_detector, human_resolution)
    if os.path.exists(tracker_path):
        tracker = load_pickle(tracker_path)
    else:
        pic_width = video_info['width']
        pic_height = video_info['height']
        human_path = './human_detection_result/{}_{}_{}.pkl'.format(video_name, human_detector, human_resolution)
        human_result = load_pickle(human_path)
        device = 'cuda'
        classification_model = load_classification_model(device=device, detector=visual_detector, resolution=visual_resolution)
        tracker = track_result(shots, human_result,video_path, classification_model, pic_width, pic_height)
        #with open('tracker_spe_all10_svd.pkl', 'wb') as f:
        with open(tracker_path, 'wb') as f:
            pickle.dump(tracker, f)
    #visulize_track(tracker, video_path, video_info, video_out)

    # garment detection
    valid_track_ids = None
    result_root = 'garment_detection_result'
    if garment_detection_whole:
        dt_pkl_path = os.path.join(result_root, '{}_{}_{}_whole_benchmark.pkl'.format(video_name, garment_detector, 800 ))
        result = load_pkl_result(dt_pkl_path)
        if no_label:
            result = set_no_label(result)
        result = filter_garments_from_tracker(tracker, result, valid_track_ids)
    else:
        garment_model = get_garment_model(garment=garment_detector, garment_res=garment_resolution, garment_score_thre=0.001)
        result = get_garment_result(video_path, tracker, garment_model, garment_detector, valid_track_ids, garment_resolution, video_info, save_result=False, no_label=no_label )

    # save garment result
    if no_label:
        if garment_detection_whole:
            out_json_path = os.path.join('benchmark_result', 'human_{}_{}_garment_whole_{}_{}_no_label.json'.format(human_detector, human_resolution, garment_detector, garment_resolution))
        else:
            out_json_path = os.path.join('benchmark_result', 'human_{}_{}_garment_{}_{}_no_label.json'.format(human_detector, human_resolution, garment_detector, garment_resolution))
        gt_path = os.path.join(result_root, '{}_merged_garment_spe_all_svd_no_label.json'.format(video_name))
    else:
        if garment_detection_whole:
            out_json_path = os.path.join('benchmark_result', 'human_{}_{}_garment_whole_{}_{}.json'.format(human_detector, human_resolution, garment_detector, garment_resolution))
        else:
            out_json_path = os.path.join('benchmark_result', 'human_{}_{}_garment_{}_{}.json'.format(human_detector, human_resolution, garment_detector, garment_resolution))
        gt_path = os.path.join(result_root, '{}_merged_garment_spe_all_svd.json'.format(video_name))


    result = remove_unrelated_coco_result(gt_path, result)

    if debug:
        out_pkl_path = str.replace(out_json_path, 'json', 'pkl')
        with open(out_pkl_path, 'wb') as f:
            pickle.dump(result, f)
            print('save result to ', out_pkl_path)

    convert_result_to_coco_style(result, out_json_path)
    dt_path = out_json_path
    benchmark_coco(gt_path, dt_path)
