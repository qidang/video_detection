import numpy as np
import tqdm
import torch
import os
import math
import datetime
import platform
import argparse

from rcnn_config_new import config

from torchcore.dnn.networks.tools.data_parallel import DataParallel
from torchcore.tools import Logger
from torchcore.dnn import trainer

from torchcore.data.datasets import COCOPersonPatchDataset, MixDataset, ModanetHumanPatchDataset
from torchcore.data.transforms import ToTensor, Normalize, Compose, ResizeAndPadding, RandomMirror
from torchcore.dnn.networks.classification_net import ClassificationNet
from torchcore.dnn.networks.feature import resnet50

class my_trainer(trainer):
    def __init__( self, cfg, model, device, trainset, testset=None, benchmark=None, criterias=None, val_dataset=None ):
        self._cfg = cfg
        self._device = device
        self._optimizer = None
        self._model = model

        self._trainset = trainset
        self._testset = testset
        self._benchmark = benchmark
        self._criterias = criterias
        self._epoch = 0
        self._val_dataset = val_dataset
        #self._trainset_feeder = data_feeder( trainset )

        #if testset is not None :
        #    self._testset_feeder = data_feeder( testset )

        self._set_optimizer()
        if cfg.resume:
            path = os.path.join(os.path.dirname(cfg.path.CHECKPOINT), 'last.pth') 
            self.resume_training(path, device)

        model = DataParallel(model,
            device_ids=None, 
            chunk_sizes=None)
        model.to(device)
        self._model = model

        self.init_logger()

    def train( self, resume=False ):
        if self._testset is not None :
            self._validate()

        for i in range( self._epoch+1, self._niter+1 ):
            print("Epoch %d/%d" % (i,self._niter))
            self._logger.info('epoch {}\n'.format(i))
            self._train()

            #if self._testset is not None and i%15 == 0 :
            if self._testset is not None :
                self._validate()
            self._epoch = i
            self.save_training(self._cfg.path.CHECKPOINT.format(self._epoch))
            self._scheduler.step()

    def train_one_epoch(self):
        self._train()

    def validate_onece(self):
        self._validate()

    def _validate( self ):
        print('start to validate')
        self._model.eval()

        results = []
        total_num = 0
        correct = 0
        with torch.no_grad() :
            for idx,(inputs, targets) in enumerate(tqdm.tqdm(self._testset, 'evaluating')):
                inputs = self._set_device( inputs )
                output = self._model( inputs)
                total_num += len(inputs['data'])
                pred = output['pred'].detach().cpu().numpy()
                target = targets['labels'].detach().cpu().numpy()
                correct += np.sum(pred==target)
        print('accuracy is {}'.format(correct/ total_num))
    

    def _train( self ):
        self._model.train()

        loss_values = []
        average_losses = {}

        for idx, (inputs, targets) in enumerate(tqdm.tqdm(self._trainset, desc='Training')):
            #print('inputs:', inputs)
            #print('targets:', targets)

            inputs = self._set_device( inputs )
            targets = self._set_device( targets )
            #print('input device:', inputs[0].device)

            loss_dict = self._model(inputs, targets)

            # add the losses for each part
            #loss_sum = sum(loss for loss in loss_dict.values())
            loss_sum=0
            for single_loss in loss_dict:
                loss_sum = loss_sum + loss_dict[single_loss]
                if single_loss in average_losses:
                    average_losses[single_loss]+= loss_dict[single_loss].mean()
                else:
                    average_losses[single_loss] = loss_dict[single_loss].mean()

            loss_sum = loss_sum.mean()
            if not math.isfinite(loss_sum):
                #print("Loss is {}, stopping training".format(loss_sum))
                print("Loss is {}, skip this batch".format(loss_sum))
                print(loss_dict)
                continue
                #sys.exit(1)

            # Computing gradient and do SGD step
            self._optimizer.zero_grad()
            loss_sum.backward()
            self._optimizer.step()
            loss_values.append( loss_sum.cpu().detach().numpy() )

            if idx%100 == 0:
                self._logger.info('{} '.format(idx+1))
                loss_str = ''
                for loss in average_losses:
                    if idx==0:
                        loss_num = average_losses[loss]
                    else:
                        loss_num = average_losses[loss] / 100
                    self._logger.info('{} '.format(loss_num))
                    loss_str += (' {} loss:{}, '.format(loss, loss_num))
                print(loss_str[:-2])
                average_losses = {}
                self._logger.info('\n')

            #bar.update(idx+1,loss=loss_sum.item())
        #bar.finish()

        print('Average loss : ', np.mean(loss_values))

    def _set_device( self, blobs ):
        if type(blobs) == list:
            for i in range(len(blobs)):
                blobs[i] = self._set_device(blobs[i])
        elif type(blobs) == dict:
            for key, data in blobs.items():
                blobs[key] = self._set_device(data)
        elif torch.is_tensor(blobs):
            blobs = blobs.to(self._device)
        return blobs

    #def _set_optimizer( self ):
    #    params = self._cfg.dnn.OPTIMIZER
    #    if params['type'] == 'GD':
    #        self._optimizer = optim.SGD( self._model.parameters(),
    #                                    lr=params['lr'],
    #                                    momentum=params.get('momentum',0.9),
    #                                    weight_decay=params.get('weight_decay',0))
    #    elif params['type'] == 'Adam':
    #        self._optimizer = optim.Adam(self._model.parameters(),
    #                                     lr = params['lr'],
    #                                     betas=params.get('betas',(0.9, 0.999)),
    #                                     eps = params.get('eps', 1e-8)
    #                                     )
    #    else:
    #        raise ValueError('Optimiser type wrong, {} is not a valid optimizer type!')

    #    self._scheduler = optim.lr_scheduler.StepLR( self._optimizer,
    #                                                step_size=params['decay_steps'],
    #                                                gamma=params['decay_rate'] )
    #    self._niter = self._cfg.dnn.NITER

    def init_logger(self):
        train_path = self._cfg.path['LOG']

        console_formatter = '{} {{}}'.format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        self._logger = Logger(level='info', file=train_path, console=False, console_formatter=console_formatter)

        print('Loss log path is: {}'.format(train_path))

    def save_training(self, path, to_print=True):
        if isinstance(self._model, torch.nn.DataParallel):
            state_dict = self._model.module.state_dict()
        else:
            state_dict =self._model.state_dict()
        torch.save({
            'epoch': self._epoch,
            'model_state_dict': state_dict,
            'optimizer_state_dict': self._optimizer.state_dict(),
            'scheduler':self._scheduler.state_dict(),
            'niter':self._niter
        }, path)
        folder = os.path.dirname(path)
        torch.save({
            'epoch': self._epoch,
            'model_state_dict': state_dict,
            'optimizer_state_dict': self._optimizer.state_dict(),
            'scheduler':self._scheduler.state_dict(),
            'niter':self._niter
        }, os.path.join(folder, 'last.pth'))
        if to_print:
            print('The checkpoint has been saved to {}'.format(path))

    def resume_training(self, path, device, to_print=True):
        checkpoint = torch.load(path)
        self._epoch = checkpoint['epoch']
        self._model.load_state_dict(checkpoint['model_state_dict'])
        self._optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        self._niter = self._niter
        #if 'scheduler' in checkpoint:
        self._scheduler.load_state_dict(checkpoint['scheduler'])
        if to_print:
            print('Chekpoint has been loaded from {}'.format(path))


if __name__=="__main__" :
    parser = argparse.ArgumentParser()
    parser.add_argument('--detector', default='centernet', choices=['centernet', 'yolo_v3', 'frcnn', 'ssd'],
                        type=str, help='Human detector')
    parser.add_argument('--resolution', default=416, 
                        type=int, help='Human detector resolution')
    args= parser.parse_args()

    detector=args.detector
    resolution = args.resolution

    image_size = 160
    params = {}
    params['config'] = 'classification'
    params['gpu'] = '0'
    params['tag'] = '{}_{}'.format(detector, resolution)
    params['path'] = {}

    params['path']['project'] = os.path.expanduser('~/Vision/data')
    #path to load data
    if platform.system() == 'Linux':
        params['path']['dataset'] = '/ssd/data'
    else:
        params['path']['dataset'] = '/Users/qida0163/Vision/data'

    #device = torch_tools.get_device( params['gpu'] )

    cfg = config( params['config'], 'train', params['path']['dataset'], params['path']['project'] )
    cfg.update( params )
    cfg.resume = False

    #set the paths to save all the results (model, val result)
    cfg.build_path( params['tag'], 'restnet', model_hash='classification' )

    #benchmark = Benchmark(cfg, criterias=criterias)
    #benchmark = None

    transforms = Compose([ResizeAndPadding(image_size), RandomMirror(), ToTensor(), Normalize()])
    transforms_test = Compose([ResizeAndPadding(image_size), ToTensor(), Normalize()])
    anno_path = os.path.join('human_detection_result', 'coco_human_{}_{}_transfered.pkl'.format(detector, resolution))
    #anno_path = '/ssd/data/annotations/coco2014_instances_person.pkl'
    root = '/ssd/data/datasets/COCO'
    part = 'train2014' # or val2014
    #dataset_a = COCOPersonPatchDataset(root, anno_path, part=part, transforms=transforms)
    dataset_a = COCOPersonPatchDataset(root, anno_path, part=part, transforms=transforms,xywh=False)
    part = 'val2014' # or val2014
    #dataset_a_test = COCOPersonPatchDataset(root, anno_path, part=part, transforms=transforms_test)
    dataset_a_test = COCOPersonPatchDataset(root, anno_path, part=part, transforms=transforms_test, xywh=False)

    #anno_root = '/ssd/data/annotations/modanet_with_human.pkl'
    anno_root = '/ssd/data/annotations/modanet_with_human_{}_{}.pkl'.format(detector, resolution)
    image_root = '/ssd/data/datasets/modanet/Images'
    part = 'train' # or val
    dataset_b = ModanetHumanPatchDataset(anno_root, image_root, part=part, transforms=transforms)
    part = 'val' 
    dataset_b_test = ModanetHumanPatchDataset(anno_root, image_root, part=part, transforms=transforms_test)

    #mix_dataset = MixDataset(dataset_a, dataset_b, shuffle=True, sample='duplicate')
    #mix_dataset_test = MixDataset(dataset_a_test, dataset_b_test, shuffle=False, sample='subsample')
    mix_dataset = MixDataset(dataset_a, dataset_b, shuffle=True, sample=None)
    mix_dataset_test = MixDataset(dataset_a_test, dataset_b_test, shuffle=False, sample=None)

    dataloader = torch.utils.data.DataLoader(mix_dataset, batch_size=128, num_workers=8, shuffle=True, drop_last=False)
    dataloader_test = torch.utils.data.DataLoader(mix_dataset_test, batch_size=128, num_workers=8, shuffle=False, drop_last=False)

    print('Dataset A has {} samples'.format(len(dataset_a)))
    print('Dataset B has {} samples'.format(len(dataset_b)))

    print('Dataset A val has {} samples'.format(len(dataset_a_test)))
    print('Dataset B val has {} samples'.format(len(dataset_b_test)))

    use_cuda = torch.cuda.is_available()
    device_name = "cuda" if use_cuda else "cpu"
    device = torch.device( device_name )

    backbone = resnet50(pretrained=True)
    model = ClassificationNet(backbone, input_size=5, class_num=2)


    #load_checkpoint(model, cfg.path.CHECKPOINT.format(150), device)

    t = my_trainer( cfg, model, device, dataloader, testset=dataloader_test, benchmark=None, val_dataset=None )
    #loss = t.trainstep()
    #print('loss is {}'.format(loss))
    #t.train_one_epoch()
    #t.validate_onece()
    #t.load_training(cfg.path.CHECKPOINT.format(150), device)
    #t.validate_onece()
    t.train()
    print('Training for detector {} and resolution {} is done.'.format(detector, resolution))
