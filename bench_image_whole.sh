python benchmark_image_whole.py --tracker tracker_spe_all10_svd.pkl --garment_detector centernet
python benchmark_image_whole.py --tracker tracker_spe_all10_svd.pkl --garment_detector frcnn
python benchmark_image_whole.py --tracker tracker_spe_all10_svd.pkl --garment_detector ssd
python benchmark_image_whole.py --tracker tracker_spe_all10_svd.pkl --garment_detector yolo_v3
