import os
import pickle
import numpy as np
import argparse
from tracker.tracker import cal_IoU
from sklearn.metrics import average_precision_score

def load_pkl_result(result_path):
    with open(result_path, 'rb') as f:
        result = pickle.load(f)
    return result

def benchmark_tracks(tracker_dt, valid_track_ids):
    max_valid_id = max(valid_track_ids)
    ids = tracker_dt.get_model_track_ids()
    ids = [aid for aid in ids if aid<=max_valid_id]

    difference = set(valid_track_ids)^set(ids)

    ids_all = tracker_dt.get_tracks_less_than_id(max_valid_id)

    accuray = 1-len(difference)/len(ids_all)
    print('Accuracy is {}'.format(accuray))
    return accuray

def benchmark_track_ap(tracker_dt, valid_track_ids):
    scores = []
    labels = []
    max_track_id = max(valid_track_ids)
    for track in tracker_dt.tracks:
        track_id = track.track_id
        if track_id > max_track_id:
            continue
        score = track.model_score
        scores.append(score)
        if track_id in valid_track_ids:
            labels.append(1)
        else:
            labels.append(0)
    scores = np.array(scores)
    labels = np.array(labels)
    print(len(scores))
    ap = average_precision_score(labels, scores)
    print('AP is {}'.format(ap))
    return ap

    
                

if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--v_detector', default='centernet', choices=['centernet', 'yolo_v3', 'frcnn', 'ssd'],
                        type=str, help='visual assessment model detector')
    parser.add_argument('--v_resolution', default=256, 
                        type=int, help='visual assessment model resolution')

    args = parser.parse_args()
    print(args)
    #ground_truth_path = 'tracker.pkl'
    ground_truth_path = 'tracker_spe_all10_svd.pkl'
    #detection_names = ['frcnn_416', 'ssd_416', 'centernet_416', 'yolo_v3_416']
    detection_names = ['frcnn_416', 'ssd_416', 'centernet_416', 'yolo_v3_416','frcnn_800', 'ssd_800', 'centernet_800', 'yolo_v3_800', 'centernet_1216', 'frcnn_1216']
    detection_folder = 'human_detection_result'
    detection_paths = [os.path.join(detection_folder, 'IDA_{}.pkl'.format(detection_name)) for detection_name in detection_names]
    #valid_shots = [2,3,4,5,8,9,10,12,13,14,15,18,19,21,23,24,25,27,28,29,30,32,33,34,37,38,39,40,41,42,43,45,46,47,48,50,51,52,55,56,57,60,61,62,63,66,67,68,69,71,72,73,74,76,77,78,81,82,83,84,86,87,88,89,90,93,94,95,96,99,100,101,102,104,105,106,109,110,111,113,114,115,116,117]
    #valid_track_ids = [41,143,209,245,331,389,413,447,513,593,740,823,828,832,866,952,1026,1088,1207,1240,1241,1310,1329,1412,1504,1622,1641,1726,1729,1712,1766,1775,1838,1906,2022,2096,2139,2157,2230,2350,2518,2594,2641,2739,2828,2951,2995,3020,3043,3056,3140,3182,3238,3278,3357,3382,3390,3412,3448,3520,3542,3573,3626,3720,3805,3815,3831,3900,3954,4138,4172,4187,4212,4227,4289,4364,4512,4559,4573,4583,4614,4628,4735,4772,4824,4957,5003,5031,5063,5097,5100,5170,5213,5292,5443,5504,5515,5531,5558,5618,5668,5736,5823,5902,5942,5960,6012,6089,6210,6252,6284,6332,6335,6436,6473,6541,6657,6728,6746,6778,6843,6897,6972,7022,7067,7110,7111,7164,7154,7159,7210,7301,7457,7535,7565,7570,7585,7641,7723,7764,7840,7953,8007,8021,8047,8094,8170,8348,8401,8424,8468,8517,8580,8651,8764,8834,8816,8849,8859,8921,8961,9027,9176,9281,9412,9414,9413,9415,9681]
    #valid_track_ids = [17,79,141,169,213,235,262,277,301,348,360,403,489,518,545,554,573,636,668,752,771,767,807,855,894,932,1025,1057,1065,1067,1076,1130,1166,1212,1256,1283,1274,1323,1346,1381,1429,1527,1560,1565,1588,1591,1592,1596,1637,1720,1796,1815,1816,1831,1838,1848,1857,1939,1956,1999,2006,2012,2056,2074,2092,2107,2126,2130,2157,2177,2237,2267,2330,2385,2395,2408,2490,2510,2611,2633,2639,2660,2669,2722,2763,2851,2863,2876,2895,2905,2961,2994,3007,3109,3114,3136,3145,3181,3188,3210,3267,3301,3390,3425,3432,3440,3463,3490,3529,3579,3589,3615,3627,3656,3692,3695,3711,3767,3802,3899,3914, 3942, 3961,3991, 4021, 4059,4086,4158,4209,4221,4238,4267,4331,4355,4389, 4412, 4460,4457,4473,4481,4487,4537,4583,4666,4718,4730,4733,4769,4801,4839,4884,4983,5008,5055,5156,5204,5300,5349,5392,5453,5488,5556,5594,5615,5641,5657,5671,5711,5753,5820,5903]
    #valid_track_ids = [17,111,152,184,234,269,284,301,313,329,338,402,482,528,535,548,553,566,612,655,705,763,782,786,820,831,879,948,962]
    valid_track_ids = [17,111,152,184,234,269,284,301,313,329,338,402,482,528,535,548,553,566,612,655,705,763,782,786,820,831,879,948,962,1021,1043,1062,1072,1087,1119,1172,1219,1263,1265,1296,1307,1339,1415,1528,1554,1575,1582,1599,1611,1660,1719,1814,1846,1848,1872,1879,1883,1935,1966,1998,2035,2075,2087,2102,2106,2110,2112,2142,2161,2199,2233,2271,2315,2367,2373,2387,2423,2468,2575,2600,2604,2623,2640,2678,2733,2806,2826,2841,2876,2929,2951,2994,3055,3088,3111,3122,3150,3160,3186,3209,3269,3347,3396,3410,3434,3463,3486,3533,3580,3649,3665,3676,3695,3746,3809,3842,3856,3884,3892,3937,3962,4027,4079,4113,4129,4145,4167,4209,4242,4289,4325,4360,4378,4379,4395,4412,4431,4504,4609,4630,4651,4673,4676,4687,4693,4716,4757,4776,4830,4875,4895,4939,4965,4993,5053,5166,5209,5218,5242,5270,5303,5343,5404,5438,5457,5466,5492,5517,5563,5662,]
    #tracker_gt = load_pkl_result(ground_truth_path)
    
    tracker_dt_path = 'trackers/tracker_{}_{}.pkl'.format(args.v_detector, args.v_resolution)
    tracker_dt = load_pkl_result(tracker_dt_path)

    #benchmark_tracks(tracker_dt, valid_track_ids)
    benchmark_track_ap(tracker_dt, valid_track_ids)
