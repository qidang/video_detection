import argparse
import os
import pickle
from dev import get_dataset
from dev import get_models
from dev import parse_video_list
from dev import run_detect, run_detect_on_shot, human_evaluation, evaluation_garments_on_shot
from dev import load_classification_model
from dev import get_transnet, extract_shots

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--dataset_res', default=416, 
                        type=int, help='Human dataset resolution')
    parser.add_argument('--human_detector', default='centernet', choices=['centernet', 'yolo_v3', 'frcnn', 'ssd'],
                        type=str, help='Human detector')
    parser.add_argument('--human_res', default=416, 
                        type=int, help='Human detector resolution')
    parser.add_argument('--garment_detector', default='centernet', choices=['centernet', 'yolo_v3', 'frcnn', 'ssd'],
                        type=str, help='Garment detector')
    parser.add_argument('--garment_res', default=256, 
                        type=int, help='Garment detector resolution')
    parser.add_argument('--device', default='cuda', 
                        type=str, help='Garment detector resolution')
    parser.add_argument('--load_shots', default=None, 
                        type=str, help='Do we want to load shots from file, it should be file_path')
    parser.add_argument('--video_list', default='video_names.txt', 
                        type=str, help='file path for video names')
    parser.add_argument('--video_shots', default='video_shots.txt', 
                        type=str, help='file path for valid video shots')
    args = parser.parse_args()
    print(args)

    human_detector = args.human_detector
    human_resolution = args.human_res
    garment_detector = args.garment_detector
    garment_resolution = args.garment_res
    dataset_res = args.dataset_res
    device = args.device

    #score_thres = [i/10 for i in range(1,10)]
    score_thres = [i/10 for i in range(1,10)]

    #video_paths = [os.path.expanduser('~/data/fashion_clips/videos/example_video2.mp4')]
    video_root_path = os.path.expanduser('~/data/fashion_videos/')
    video_names, video_paths, valid_shots = parse_video_list(args.video_list, video_root_path, video_shots=args.video_shots)
    print(video_names)
    print(video_paths)
    print(valid_shots)
    transnet_path = './transnet/model/transnet_model-F16_L3_S2_D256'
    if args.load_shots is None:
        transnet, params = get_transnet(transnet_path)

    #shot_num = 1
    #for score in shots:
    #    if score>0.1:
    #        shot_num += 1
    #print('There are {} shots in the video'.format(shot_num))

    #print('dataset done')
    #human_model, garment_model = get_models(human=human_detector, human_res=human_resolution, 
    #                                garment=garment_detector, garment_res=garment_resolution, human_score_thre=0.3)
    #print('model done')
    
    classification_model = load_classification_model(device=device)
    for i, score_thre in enumerate(score_thres):
        print('score thre: {}'.format(score_thre))
        human_model, garment_model = get_models(human=human_detector, human_res=human_resolution, 
                                        garment=garment_detector, garment_res=garment_resolution, human_score_thre=score_thre)
        for video_path, video_name in zip(video_paths, video_names):
            if args.load_shots is None:
                shots = extract_shots(transnet, params, video_path)
                if i == 0:
                    print(shots)
                    print(len(shots))
                #with open('shots.pkl', 'wb') as f:
                #    pickle.dump(shots, f)
            else:
                if not os.path.isfile(args.load_shots):
                    raise ValueError('Invalid shot path')
                with open(args.load_shots, 'rb') as f:
                    shots = pickle.load(f)
            dataset, video_info = get_dataset(human_detector, dataset_res, video_path)
            video_info['out_path'] = 'out_videos/{}_human_{}_{}_garment_{}_{}_all.mp4'.format(video_name, human_detector, human_resolution, garment_detector, garment_resolution)


            human_evaluation(dataset, human_model, garment_model, garment_detector, video_info, shots, classification_model=classification_model, garment_res=garment_resolution, valid_shots=valid_shots[video_name] )
            #run_detect_on_shot(dataset, human_model, garment_model, garment_detector, video_info, shots, classification_model=classification_model, garment_res=garment_resolution, valid_shots=None )