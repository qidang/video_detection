import sys
import argparse
import easydict
import os
sys.path.append('..')
from centernet.src.lib.opts import opts

class CenterNetOpts():
    def __init__(self):
        args = easydict.EasyDict()
        args.task = 'ctdet'
        args.arch = 'dla_34'
        args.head_conv = 256
        args.down_ratio=4

        args.K = 100

        args.cat_spec_wh = False
        self.args = args

    def set_heads(self, opt, num_classes):
        #opt = self.args
        opt.num_classes = num_classes
        opt.heads = {'hm': opt.num_classes,
                    'wh': 2 if not opt.cat_spec_wh else 2 * opt.num_classes}
        if opt.reg_offset:
            opt.heads.update({'reg': 2})
        return opt

    def parse(self, args=''):
        opt = self.args

        opt.reg_offset = True
        opt.reg_bbox = True

        #if opt.head_conv == -1: # init default head_conv
        #    opt.head_conv = 256 if 'dla' in opt.arch else 64
        #    opt.pad = 127 if 'hourglass' in opt.arch else 31
        #    opt.num_stacks = 2 if opt.arch == 'hourglass' else 1


        #opt.root_dir = os.path.join(os.path.dirname(__file__), '..', '..')
        #opt.data_dir = os.path.join(opt.root_dir, 'data')
        #opt.exp_dir = os.path.join(opt.root_dir, 'exp', opt.task)
        
        return opt