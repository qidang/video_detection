export PYTHONWARNINGS="ignore"
python system_benchmark.py --human_detector ssd --human_resolution 416 --garment_detector centernet --garment_resolution 800 --no_label --garment_detection_whole
python system_benchmark.py --human_detector ssd --human_resolution 416 --garment_detector ssd --garment_resolution 800 --no_label --garment_detection_whole
python system_benchmark.py --human_detector ssd --human_resolution 416 --garment_detector yolo_v3 --garment_resolution 800 --no_label --garment_detection_whole
python system_benchmark.py --human_detector ssd --human_resolution 416 --garment_detector frcnn --garment_resolution 800 --no_label --garment_detection_whole

python system_benchmark.py --human_detector ssd --human_resolution 800 --garment_detector centernet --garment_resolution 800 --no_label --garment_detection_whole
python system_benchmark.py --human_detector ssd --human_resolution 800 --garment_detector ssd --garment_resolution 800 --no_label --garment_detection_whole
python system_benchmark.py --human_detector ssd --human_resolution 800 --garment_detector yolo_v3 --garment_resolution 800 --no_label --garment_detection_whole
python system_benchmark.py --human_detector ssd --human_resolution 800 --garment_detector frcnn --garment_resolution 800 --no_label --garment_detection_whole

python system_benchmark.py --human_detector ssd --human_resolution 416 --garment_detector centernet --garment_resolution 800 --garment_detection_whole
python system_benchmark.py --human_detector ssd --human_resolution 416 --garment_detector ssd --garment_resolution 800 --garment_detection_whole
python system_benchmark.py --human_detector ssd --human_resolution 416 --garment_detector yolo_v3 --garment_resolution 800 --garment_detection_whole
python system_benchmark.py --human_detector ssd --human_resolution 416 --garment_detector frcnn --garment_resolution 800 --garment_detection_whole

python system_benchmark.py --human_detector ssd --human_resolution 800 --garment_detector centernet --garment_resolution 800 --garment_detection_whole
python system_benchmark.py --human_detector ssd --human_resolution 800 --garment_detector ssd --garment_resolution 800 --garment_detection_whole
python system_benchmark.py --human_detector ssd --human_resolution 800 --garment_detector yolo_v3 --garment_resolution 800 --garment_detection_whole
python system_benchmark.py --human_detector ssd --human_resolution 800 --garment_detector frcnn --garment_resolution 800 --garment_detection_whole


