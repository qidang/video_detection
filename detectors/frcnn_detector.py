import torch
import torchvision
from torch import nn
import sys
sys.path.append('..')

from torchcore.dnn.networks.faster_rcnn import FasterRCNN


class FrcnnDetecter(nn.Module):
    def __init__(self, cfg):
        super().__init__()
        self.cfg = cfg

        backbone = torchvision.models.detection.backbone_utils.resnet_fpn_backbone('resnet101', pretrained=False)
        model = FasterRCNN(backbone, num_classes=cfg.num_classes, cfg=cfg.dnn)
        state_dict = torch.load(cfg.model_path, map_location=cfg.device)['model_state_dict']
        model.load_state_dict(state_dict, strict=True)
        self.model = model
        self.model.eval()

    def forward(self, inputs):
        outputs = self.model(inputs)
        for output in outputs:
            for k, v in output.items():
                output[k] = v.detach().cpu().numpy()

        return outputs

