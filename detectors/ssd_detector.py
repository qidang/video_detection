import torch
import numpy as np
from ssd_pytorch.ssd import build_ssd

class SSDDetector(torch.nn.Module):
    def __init__(self, cfg):
        super().__init__()
        self.model = build_ssd(cfg, cfg['min_dim'], cfg['num_classes'])
        self.model.load_weights(cfg['model_path'])
        self.model.eval()
        self.thre = cfg['thre']

    def forward(self, inputs):
        out = self.model(inputs['data']).data
        dets = self.post_process(out, inputs)
        return dets

    def post_process(self, outputs, inputs):
        all_detections = []

        for detection, h, w in zip(outputs, inputs['height'], inputs['width']):
            boxes_temp = []
            score_temp = []
            labels_temp = []
            for j, dets in enumerate(detection): 
                # skip j = 0, because it's the background class
                if j==0:
                    continue

                # TODO check here
                #mask = dets[:, 0].gt(0.).expand(5, dets.size(0)).t()
                mask = dets[:, 0].gt(self.thre).expand(5, dets.size(0)).t()
                dets = torch.masked_select(dets, mask).view(-1, 5)
                if dets.size(0) == 0:
                    continue
                boxes = dets[:, 1:]
                boxes[:, 0] *= w
                boxes[:, 2] *= w
                boxes[:, 1] *= h
                boxes[:, 3] *= h
                scores = dets[:, 0].cpu().numpy()
                labels = j * np.ones_like(scores)
                boxes_temp.append(boxes)
                score_temp.append(scores)
                labels_temp.append(labels)
            det = {}
            if len(boxes_temp)>0:
                det['boxes'] = np.concatenate(boxes_temp, axis=0)
                det['scores'] = np.concatenate(score_temp, axis=0)
                det['labels'] = np.concatenate(labels_temp, axis=0)
            else:
                det['boxes'] = boxes_temp
                det['scores'] = score_temp
                det['labels'] = labels_temp

            all_detections.append(det)
        return all_detections
