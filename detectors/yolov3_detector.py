import torch
from pytorch_yolov3.models import Darknet
from pytorch_yolov3.utils.utils import nms_split

class YoloV3Detector(torch.nn.Module):
    def __init__(self, opt):
        super().__init__()
        self.opt = opt
        self.model = Darknet(opt.cfg)
        self.model.load_state_dict(torch.load(opt.model_path, map_location=opt.device))

    def forward(self, inputs):
        out = self.model(inputs['data'])
        dets = self.post_process(out, inputs)
        return dets

    def post_process(self, outputs, inputs):
        boxes, pred_scores, class_scores, pred_class = nms_split(outputs, conf_thres=self.opt.score_thre, nms_thres=0.5)
        scales = inputs['scale'].cpu().numpy()
        all_detections = []

        for box, score, label, scale in zip(boxes, pred_scores, pred_class, scales):
            det = {}
            if len(box) == 0:
                det['boxes'] = box
            else:
                det['boxes'] = (box * scale).detach().cpu().numpy()
            det['scores'] = score
            if len(label) > 0:
                det['labels'] = label + 1
            else:
                det['labels'] = label 
            all_detections.append(det)
        return all_detections

