import torch
from torch import nn
import sys
sys.path.append('..')

from centernet.src.lib.models.model import create_model, load_model
from opts.centernet_opt import CenterNetOpts
from centernet.src.lib.models.decode import ctdet_decode
class CenterNetDetector(nn.Module):
    def __init__(self, opt):
        super().__init__()
        self.opt = opt
        #model_path = 'weights/centernet_coco_person_defalut.pth'
        model = create_model(opt.arch, opt.heads, opt.head_conv)
        self.model = load_model(
            model, opt.model_path )
    
    def forward(self, inputs):
        out = self.model(inputs['data'])
        outputs = self.post_process(out, inputs)
        return outputs

    def post_process(self, outputs, inputs):
        all_detection = []
        output = outputs[0]
        hm = output['hm'].sigmoid_()
        wh = output['wh']
        reg = output['reg'] if self.opt.reg_offset else None
        scale = inputs['scale'].cpu().numpy()
        dets = ctdet_decode(
            hm, wh, reg=reg,
            cat_spec_wh=self.opt.cat_spec_wh, K=self.opt.K)
        dets = dets.detach().cpu().numpy()
        for i, det in enumerate(dets):
            #im_id = int(batch['meta']['img_id'].cpu().numpy())
            #crop_box = batch['meta']['crop_box'][0].cpu().numpy()
            boxes = det[:,:4] * self.opt.down_ratio
            #boxes = recover_xywh_boxes(boxes, scale, crop_box)
            scores = det[:,4]
            labels = det[:,5]+1
            valid_ind = scores>self.opt.score_thre
            boxes = boxes[valid_ind] * scale[i].mean() # might need to be revised
            scores = scores[valid_ind]
            labels = labels[valid_ind]
            single_det = {}
            single_det['boxes'] = boxes
            single_det['labels'] = labels
            single_det['scores'] = scores
            all_detection.append(single_det)
        return all_detection
